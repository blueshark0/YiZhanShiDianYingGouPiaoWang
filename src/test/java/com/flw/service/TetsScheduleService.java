package com.flw.service;


import com.flw.entity.Schedule;
import com.flw.entity.Seat;
import com.hbm.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * 本类用于：测试排期服务实现类
 *
 * @author Bruce
 * @date 2017/7/23 21:38
 */
@ContextConfiguration(locations = "classpath:spring/*.xml")
@WebAppConfiguration
public class TetsScheduleService extends AbstractTestNGSpringContextTests{

    @Autowired
    private IScheduleService scheduleService;

    @Autowired
    private IMovieService movieService;

    @Autowired
    private ICinemaService cinemaService;

    @Test
    public void testSave(){
        Schedule s1 = new Schedule();
        s1.setMovie(movieService.findById(1L));
        s1.setCinema(cinemaService.findById(1L));
        s1.setOldPrice(88.5);
        s1.setNewPrice(30.5);
        s1.setSchDate(new java.sql.Timestamp(new DateUtil().createDate(2017,7,22,12,30,0).getTime()));
        scheduleService.save(s1);
    }

    @Test
    public void testFindByConditions(){
        List<Schedule> schList = scheduleService.findByConditions(null,new Date(),null);
        System.out.println("查到的排期大小："+schList.size());
    }

    @Test
    public void testCreateDate(){
        Date dt = DateUtil.createDate(2017,7,22,12,30,0);
        System.out.println(dt);
    }


    @Test
    public void testSeat(){
        Schedule schedule = scheduleService.findById(1L);
        System.out.println("打印排期"+schedule);

        Set<Seat> set = schedule.getMovieHall().getSeats();
        for (Seat seat : set){
            System.out.println(seat);
        }
    }

    @Test
    public void testFind(){
        Schedule schedule = scheduleService.findById(1L);
        System.out.println(schedule.getMovie().getCover().getImgName());
    }
}
