package com.flw.service;

import com.flw.entity.Area;
import com.flw.entity.Movie;
import com.flw.entity.PageBean;
import com.flw.entity.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.Test;

/**
 * Created by DELL on 2017/7/21.
 */
@ContextConfiguration(locations = "classpath:spring/*.xml")
@WebAppConfiguration
public class TestMovieService extends AbstractTestNGSpringContextTests{

    @Autowired
    private IMovieService movieService;

    @Test
    public void testFindByConditions(){
        String mvName = null;
        Type type = null;
        Area area = null;
        Integer show = 1;
        Integer popular = 1;
        Long pageNow = 1L;
        Long pageSize = 6L;
        PageBean<Movie> movieList = movieService.findByPage(mvName, type, area, show, popular, pageNow, pageSize);

        System.out.println("输出所有符合要求的电影:");
        for(Movie m : movieList.getList()){
            System.out.println(m);
        }
    }

}
