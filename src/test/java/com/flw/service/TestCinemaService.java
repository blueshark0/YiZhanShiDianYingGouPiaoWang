package com.flw.service;

import com.flw.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.Test;

/**
 * Created by DELL on 2017/7/21.
 */
@ContextConfiguration(locations = "classpath:spring/*.xml")
@WebAppConfiguration
public class TestCinemaService extends AbstractTestNGSpringContextTests{

    @Autowired
    private ICinemaService cinemaService;

    @Test
    public void testFindById(){
        Cinema cinema = cinemaService.findById(1L);
        System.out.println(cinema);
    }

}
