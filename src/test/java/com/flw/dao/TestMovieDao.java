package com.flw.dao;

import com.flw.entity.Area;
import com.flw.entity.Movie;
import com.flw.entity.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by DELL on 2017/7/21.
 */
@ContextConfiguration(locations = "classpath:spring/*.xml")
@WebAppConfiguration
public class TestMovieDao extends AbstractTestNGSpringContextTests
{
    @Autowired
    private  IMovieDao movieDao;

    @Test
    public void testFindByName(){
        List<Movie> list = movieDao.findByName("三生三世");
        for (Movie m:list) {
            //System.out.println(m);
            //System.out.println("主演："+m.getMainAct());
            System.out.println("图书图片张数："+m.getImages().size());
            System.out.println(m.getImages());
        }
    }

    @Test
    public void testFindByCondition(){
        String mvName = null;
        Type type = null;
        Area area = null;
        Integer show = 1;
        Integer popular = 2;
        Long pageNow = 1L;
        Long pageSize = 10L;

        List<Movie> list = movieDao.findByConditions(mvName, type, area, show, popular, pageNow, pageSize);

        for (Movie m :list){
            System.out.println("打印电影："+m);
            System.out.println("打印剧照大小："+m.getImages().size());
            //System.out.println("打印封面"+m.getCover());
            System.out.println("=================");
        }
    }

}
