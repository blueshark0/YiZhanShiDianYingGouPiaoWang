package com.flw.dao;

import com.flw.entity.MovieHall;
import com.flw.entity.Seat;
import com.flw.entity.SeatType;
import com.flw.service.IMovieHallService;
import com.flw.service.ISeatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 本类用于:
 *
 * @author Bruce
 * @version v1.0
 * @create 2017-07-27 11:24
 **/
@ContextConfiguration(locations = "classpath:spring/*.xml")
@WebAppConfiguration
public class TestSeatDao  extends AbstractTestNGSpringContextTests{

    @Autowired
    private ISeatService seatService;

    @Autowired
    private IMovieHallService movieHallService;



    @Test
    public void saveTest(){
        MovieHall movieHall = movieHallService.findById(1L);
        Seat seat =new Seat(241L,7,11, SeatType.可选座位,movieHall);
        seatService.save(seat);
    }

    @Test
    public void deleteTest(){

    }

    @Test
    public void updateTest(){

    }

    @Test
    public void findByIdTest(){

    }

    @Test
    public void findByXYTest(){
        MovieHall movieHall = movieHallService.findById(1L);
        Seat seat = seatService.findByXY(movieHall,1,1);
        System.out.println("打印座位:"+seat);
    }

    @Test
    public void  test1(){
        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
        String n = sdf.format(new Date());
        System.out.println("格式化的时间: "+n);
    }





}
