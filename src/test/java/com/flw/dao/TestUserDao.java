package com.flw.dao;

import com.flw.dao.impl.UserDaoImpl;
import com.flw.entity.User;
import com.flw.service.IUserService;
import org.hibernate.validator.constraints.br.TituloEleitoral;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.Test;

import java.util.*;

/**
 * 本类用于：测试UserDao
 *
 * @author Bruce
 * @date 2017/7/20 20:16
 */
@ContextConfiguration(locations = "classpath:spring/*.xml")
@WebAppConfiguration
public class TestUserDao extends AbstractTestNGSpringContextTests {

    @Autowired
    private IUserDao userDao;

    @Autowired
    private IUserService userService;

    @Test(priority = 1)
    public void testSave() {
        User u1 = new User("1001", "123456", "Tom", "12580", "123@qq.com", new Date());
        System.out.println("u1：" + u1);
        System.out.println("userDao：" + userDao);
        userDao.save(u1);
    }

    @Test(priority = 2)
    public void testFindAll() {
        List<User> list = userDao.findAll();
        for (User u : list) {
            System.out.println(u);
        }
    }

    @Test()
    public void update() {
        User user = userDao.findByUserNum("1001");
        System.out.println(user);
        user.setPassword("admin");
        userDao.update(user);
    }

    @Test
    public void test() {
        Map<Integer, String> map = new TreeMap<>();

        map.put(1, "hello");
        map.put(1, "ww");

        Set<Integer> set = map.keySet();
        for (Integer i : set) {
            System.out.println(map.get(i));
        }
    }

    @Test
    public void test01() {
        try {
            User u = userService.findByUserNum("aaaaaaa");
            u.setPhone("123");
            System.out.println("condition 1");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("condition 2");
        } catch (Exception e) {
            System.out.println("condition 3");
        } finally {
            System.out.println("condition 4");
        }
    }

    @Test
    public void oneMethod(){
        User u = userService.findByUserNum("aaaaaaa");
        System.out.println(u);
        u.getPhone();
    }


}
