package com.flw.dao;

import com.flw.entity.*;
import com.flw.service.IOrderService;
import com.flw.service.IScheduleService;
import com.flw.service.ISeatService;
import com.flw.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by DELL on 2017/7/21.
 */
@ContextConfiguration(locations = "classpath:spring/*.xml")
@WebAppConfiguration
public class TestOrderDao extends AbstractTestNGSpringContextTests{

    @Autowired
    private IUserService userService;

    @Autowired
    private IOrderService orderService;

    @Autowired
    private ISeatService seatService;

    @Autowired
    private IScheduleService scheduleService;

    @Test
    public void testItem(){
        Seat seat = seatService.findById(1L);
        System.out.println("打印Seat："+seat);

        OrderItem item = new OrderItem();
        item.setSeat(seat);

        item.setPrice(88.5);
        item.setSchedule(scheduleService.findById(1L));
        item.setPhone("13856231245");
        System.out.println("打印明细:"+item);
    }

    @Test
    public void testSave(){

        OrderItem item = new OrderItem();
        item.setSeat(seatService.findById(1L));
        item.setPrice(88.5);
        item.setSchedule(scheduleService.findById(1L));
        item.setPhone("13856231245");
        System.out.println("打印明细:"+item);

        Set<OrderItem> items = new HashSet<>();
        items.add(item);

        //生成一个订单
        Order order = new Order();

        for(OrderItem it : items){
            it.setOrder(order);
        }

        order.setItems(items);

        order.setCreateDate(new Date());

        //格式化订单号
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
        String n1 = sdf1.format(new Date());

        String n2 = "";
        for (int i = 0; i<5;i++){
            String i2 = String.valueOf((int)(Math.random()*10));
            n2 += i2;
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("HHmmss");
        String n3 = sdf2.format(new Date());
        //拼接成一个 18位的唯一订单号
        String num = n1 + n2 + n3;

        System.out.println("订单编号："+num);
        order.setOrderNum(num);

        User user = userService.findByUserNum("1001");
        System.out.println("用户："+user);
        order.setUser(userService.findByUserNum("1001"));

        order.setOrederStatus(OrderStatus.未付款);

        //保存一个订单
        orderService.save(order);
    }
}
