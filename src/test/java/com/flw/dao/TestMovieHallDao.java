package com.flw.dao;

import com.flw.entity.*;
import com.flw.service.IScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.SystemProfileValueSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Set;

/**
 * 本类用于:
 *
 * @author Bruce
 * @version v1.0
 * @create 2017-07-24 13:37
 **/
@ContextConfiguration(locations = "classpath:spring/*.xml")
@WebAppConfiguration
public class TestMovieHallDao  extends AbstractTestNGSpringContextTests{

    @Autowired
    private  IMovieHallDao movieHallDao;

    @Autowired
    private  ICinemaDao cinemaDao;

    @Test
    public void  save(){
        MovieHall movieHall =new MovieHall(111L,"测试影院",HallType.IMAX,null);
        movieHallDao.save(movieHall);
    }

    @Test
    public void  findById(){
        System.out.println(  movieHallDao.findById(21L));
    }

    @Test
    public void  findByCinema(){

        Cinema cinema = cinemaDao.findById(2L);
        System.out.println(cinema);

        List<MovieHall> list = movieHallDao.findByCinema(cinemaDao.findById(2L));
        for(MovieHall l: list){
            System.out.println(l);
        }
    }

    @Test
    public void  delete(){

    }

    @Test
    public void  update(){

    }

    @Test
    public void addSeatSql(){
        String sql = "";
        int cid = 1;
        //定义座位的行数
        for (int i=0;i<10;i++){
            //定义座位的列数
            for (int j=0;j<6;j++){
                String s = "insert into m_seat(id,hallname,halltype,cinema_id)";
                sql+="";
            }
        }
    }



}
