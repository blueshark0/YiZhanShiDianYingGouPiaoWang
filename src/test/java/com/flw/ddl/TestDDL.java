package com.flw.ddl;

import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class TestDDL {
	
	@Test(priority = 1)
		public void testDDL(){
		Configuration cfg = new Configuration().configure();
		SchemaExport export = new SchemaExport(cfg);
		export.create(true, true);
	}

	@Test(enabled = true)
	public void testDataSource() throws SQLException {
		ApplicationContext acx = new ClassPathXmlApplicationContext("spring/applicationContext-dao.xml");
		//获取连接池的对象
		DataSource ds = (DataSource) acx.getBean("dataSource");
		//获取连接并且输出
		System.out.println(ds.getConnection());

		Connection conn = ds.getConnection();
	}

}
