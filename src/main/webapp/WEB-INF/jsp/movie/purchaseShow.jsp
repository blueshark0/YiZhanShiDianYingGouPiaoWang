<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>

<%--
  Created by IntelliJ IDEA.
  User: bruce
  Date: 2017/7/25
  Time: 13:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!-- saved from url=(0043)http://piao.163.com/suzhou/movie/48488.html -->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" href="#">
    <title>购买</title>

    <meta name="keywords" content="悟空传,兑换券,上映时间,电影排期,预告片">
    <meta name="description" content="网易电影是一个能够让您在线购买电影票的在线选座平台，这里有最详实的悟空传资讯，高清的预告片，还有最及时的影讯排期等。看电影，来网易电影选座">

    <meta name="mobile-agent"
          content="format=html5;url=http://piao.163.com/wap/movie/detail.html?movieId=48488&amp;cityCode=320500">
    <meta name="mobile-agent"
          content="format=xhtml;url=http://piao.163.com/wap/movie/detail.html?movieId=48488&amp;cityCode=320500">
    <meta name="mobile-agent"
          content="format=wml;url=http://piao.163.com/wap/movie/detail.html?movieId=48488&amp;cityCode=320500">

    <link rel="stylesheet" href="${path}/css/base.css">
    <link rel="stylesheet" href="${path}/css/core.css">
    <link rel="stylesheet" href="${path}/css/detail_new.css">
    <script src="${path}/js/jquery-1.4.2.js"></script>

    <script src="${path}/js/easyCore.js"></script>
    <script src="${path}/js/js2/dialog.js"></script>
    <script src="${path}/js/autoComplete.js"></script>


    <script>
        if (!!window.Core) {
            Core.cdnBaseUrl = "http://pimg1.126.net/movie";
            Core.cdnFileVersion = "1495696265";
            Core.curCity = {'name': '苏州', 'id': '1017', 'spell': 'suzhou'};
        }
    </script>


    <style type="text/css">@-webkit-keyframes loginPopAni {
                               0% {
                                   opacity: 0;
                                   -webkit-transform: scale(0);
                               }
                               15% {
                                   -webkit-transform: scale(0.667);
                               }
                               25% {
                                   -webkit-transform: scale(0.867);
                               }
                               40% {
                                   -webkit-transform: scale(1);
                               }
                               55% {
                                   -webkit-transform: scale(1.05);
                               }
                               70% {
                                   -webkit-transform: scale(1.08);
                               }
                               85% {
                                   opacity: 1;
                                   -webkit-transform: scale(1.05);
                               }
                               100% {
                                   opacity: 1;
                                   -webkit-transform: scale(1);
                               }
                           }

    @keyframes loginPopAni {
        0% {
            opacity: 0;
            transform: scale(0);
        }
        15% {
            transform: scale(0.667);
        }
        25% {
            transform: scale(0.867);
        }
        40% {
            transform: scale(1);
        }
        55% {
            transform: scale(1.05);
        }
        70% {
            transform: scale(1.08);
        }
        85% {
            opacity: 1;
            transform: scale(1.05);
        }
        100% {
            opacity: 1;
            transform: scale(1);
        }
    }</style>
</head>
<body>
<noscript>&lt;div id="noScript"&gt;
    &lt;div&gt;&lt;h2&gt;请开启浏览器的Javascript功能&lt;/h2&gt;&lt;p&gt;亲，没它我们玩不转啊！求您了，开启Javascript吧！&lt;br/&gt;不知道怎么开启Javascript？那就请&lt;a
    href="http://www.baidu.com/s?wd=%E5%A6%82%E4%BD%95%E6%89%93%E5%BC%80Javascript%E5%8A%9F%E8%83%BD" target="_blank"&gt;猛击这里&lt;/a&gt;！&lt;/p&gt;&lt;/div&gt;
    &lt;/div&gt;
</noscript>

    <%@include file="../common/header.jsp"%>

<div class="wrap990">
    <section class="mv_info_box clearfix">
        <div id="share" class="share">
            <div class="share_inner">影片分享到<b></b>
                <div class="shareDiv" id="shareDiv"><a class="cpShareLink" rel="neteasyweibo"
                                                       href="http://t.163.com/article/user/checkLogin.do?link=http://piao.163.com&amp;source=%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1&amp;info=%E6%82%9F%E7%A9%BA%E4%BC%A0-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8%2C%E5%85%91%E6%8D%A2%E5%88%B8%2C%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4%2C%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1http%3A%2F%2Fpiao.163.com%2Fsuzhou%2Fmovie%2F48488.html%23from%3Dt.163&amp;togImg=true&amp;images="
                                                       title="网易微博" target="_blank"><em
                        class="cpShareIcon neteasyweibo"></em></a><a class="cpShareLink" rel="sinaweibo"
                                                                     href="http://v.t.sina.com.cn/share/share.php?url=http%3A%2F%2Fpiao.163.com%2Fsuzhou%2Fmovie%2F48488.html%23from%3Dt.sina&amp;title=%E6%82%9F%E7%A9%BA%E4%BC%A0-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8%2C%E5%85%91%E6%8D%A2%E5%88%B8%2C%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4%2C%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1&amp;searchPic=true&amp;pic="
                                                                     title="新浪微博" target="_blank"><em
                        class="cpShareIcon sinaweibo"></em></a><a class="cpShareLink" rel="qqweibo"
                                                                  href="http://share.v.t.qq.com/index.php?c=share&amp;a=index&amp;site=http://caipiao.163.com&amp;url=http%3A%2F%2Fpiao.163.com%2Fsuzhou%2Fmovie%2F48488.html%23from%3Dt.qq&amp;title=%E6%82%9F%E7%A9%BA%E4%BC%A0-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8%2C%E5%85%91%E6%8D%A2%E5%88%B8%2C%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4%2C%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1&amp;pic="
                                                                  title="腾讯微博" target="_blank"><em
                        class="cpShareIcon qqweibo"></em></a><a class="cpShareLink" rel="qqzone"
                                                                href="http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=http%3A%2F%2Fpiao.163.com%2Fsuzhou%2Fmovie%2F48488.html%23from%3Dt.qqzone&amp;desc=%E6%82%9F%E7%A9%BA%E4%BC%A0-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8%2C%E5%85%91%E6%8D%A2%E5%88%B8%2C%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4%2C%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1http%3A%2F%2Fpiao.163.com%2Fsuzhou%2Fmovie%2F48488.html%23from%3Dt.qqzone&amp;summary=%20&amp;title=%E6%82%9F%E7%A9%BA%E4%BC%A0-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8,%E5%85%91%E6%8D%A2%E5%88%B8,%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4,%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1&amp;site=http://caipiao.163.com&amp;otype=share&amp;pics="
                                                                title="QQ空间" target="_blank"><em
                        class="cpShareIcon qqzone"></em></a><a class="cpShareLink" rel="renren"
                                                               href="http://widget.renren.com/dialog/share?resourceUrl=http%3A%2F%2Fpiao.163.com%2Fsuzhou%2Fmovie%2F48488.html%23from%3Dt.renren&amp;title=%E6%82%9F%E7%A9%BA%E4%BC%A0-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8%2C%E5%85%91%E6%8D%A2%E5%88%B8%2C%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4%2C%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1&amp;description=%E6%82%9F%E7%A9%BA%E4%BC%A0-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8%2C%E5%85%91%E6%8D%A2%E5%88%B8%2C%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4%2C%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1http%3A%2F%2Fpiao.163.com%2Fsuzhou%2Fmovie%2F48488.html%23from%3Dt.renren&amp;charset=utf-8&amp;pic="
                                                               title="人人网" target="_blank"><em
                        class="cpShareIcon renren"></em></a><a class="cpShareLink" rel="kaixin"
                                                               href="http://www.kaixin001.com/rest/records.php?content=%E6%82%9F%E7%A9%BA%E4%BC%A0-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8%2C%E5%85%91%E6%8D%A2%E5%88%B8%2C%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4%2C%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1&amp;url=http%3A%2F%2Fpiao.163.com%2Fsuzhou%2Fmovie%2F48488.html%23from%3Dt.kaixin&amp;starid=0&amp;aid=0&amp;style=11&amp;pic="
                                                               title="开心网" target="_blank"><em
                        class="cpShareIcon kaixin"></em></a><a class="cpShareLink" rel="douban"
                                                               href="http://shuo.douban.com/!service/share?href=http%3A%2F%2Fpiao.163.com%2Fsuzhou%2Fmovie%2F48488.html%23from%3Dt.douban&amp;name=%E6%82%9F%E7%A9%BA%E4%BC%A0-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8%2C%E5%85%91%E6%8D%A2%E5%88%B8%2C%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4%2C%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1&amp;image="
                                                               title="豆瓣网" target="_blank"><em
                        class="cpShareIcon douban"></em></a></div>
            </div>
        </div>

        <div class="poster">
            <img src="${path}/images/movieImg/${movie.cover.imgName}" width="200" height="267" alt="${movie.cover.imgName}">
        </div>
        <dl class="mv_info">
            <dt class="overflow">
                <span class="mv_name"><h2>${movie.mvName}</h2></span>
                <span class="star_bg ml10">
	                    <div class="star" style="width:72%"></div>
	                </span>
                <span class="score_big">
                    <%--<fmt:formatNumber value="${8/7}" maxIntegerDigits="0"/>--%>
                    <%--<fmt:parseNumber value="${8/7}"/>--%>
                    <fmt:formatNumber value="${movie.score}" maxFractionDigits="0"/>
                        .
                    <em class="s">
                        <fmt:formatNumber value="${movie.score}" maxIntegerDigits="0"/>
                    </em>
                </span>

            </dt>
            <dd class="summary">"${movie.explain}"</dd>
            <dd class="des">上映：<em class="">${movie.showDate}</em></dd>
            <dd class="des">导演：${movie.director}</dd>
            <dd class="des role" id="role" style="height: auto;">
                <span style="float:left;">主演：</span>
                <div class="role_list" id="roleList">
                    <c:forEach items="${movie.mainAct}" var="act">
                        <span>${act.name}</span>
                    </c:forEach>
                   <%-- <span>彭于晏</span>
                    <span>倪妮</span>
                    <span>欧豪</span>
                    <span>余文乐</span>
                    <span>郑爽</span>
                    <span>乔杉</span>
                    <span>杨迪</span>
                    <span>俞飞鸿</span>--%>
                </div>
            </dd>
            <dd class="other"><span>2D</span><span>${movie.area}</span><span>${movie.type}</span>
                <span>
                    <fmt:formatNumber value="${movie.mvTime / 3600}" maxFractionDigits="0" />
                    小时
                    <fmt:formatNumber value="${movie.mvTime % 3600 / 60}" maxFractionDigits="0" />
                    分钟
                </span>
            </dd>
        </dl>
        <script>
            Core.movieId = 48488;
        </script>
    </section>
    <article class="mv_body_990 clearfix mt10">
        <section class="mainContent">
            <div id="pq">
                <ul class="mv_detail_tab" id="mvTabs">
                    <li rel="#part2" class="active" id="">影院排期</li>
                    <li rel="#part1" class="" id="commentTab">剧情影评</li>
                </ul>
            </div>

            <div class="mv_comment_box" id="part1" style="display: none;">
                <div class="mv_comm_plot">
                    <div class="title">
                        <h2>剧情简介：</h2>
                    </div>
                    <p>${movie.info}</p>
                </div>
                <div class="mv_comm_plot">
                    <div class="title">
                        <h2>剧照</h2>
                        <span class="tip">（<em>${movie.images.size()}</em> 张）</span>
                    </div>
                    <div class="mv_comm_photo ">
                        <div class="photo_bar ">
                            <a href="javascript:;" id="leftPBtn" class="photo_bar_left noLeft"></a>
                        </div>
                        <div class="photo_list_box">
                            <ul class="photo_list" id="sphotoList">

                                <c:forEach items="${movie.images}" var="img" begin="0" end="4">
                                    <li>
                                        <a href="javascript:;">
                                            <img src="${path}/images/movieImg/ju/${img.imgName}"  alt="" title="">
                                            <span></span>
                                        </a>
                                    </li>
                                </c:forEach>
                                <%--<li>
                                    <a href="javascript:;"><img src="images/wukongzhuan.jpg" alt=""
                                                                title=""><span></span></a>
                                </li>
                                <li>
                                    <a href="javascript:;"><img src="images/wukongzhuan.jpg" alt=""
                                                                title=""><span></span></a>
                                </li>
                                <li>
                                    <a href="javascript:;"><img src="images/wukongzhuan.jpg" alt=""
                                                                title=""><span></span></a>
                                </li>
                                <li>
                                    <a href="javascript:;"><img src="images/wukongzhuan.jpg" alt=""
                                                                title=""><span></span></a>
                                </li>--%>

                            </ul>
                        </div>
                        <div class="photo_bar ">
                            <a href="javascript:;" id="rightPBtn" class="photo_bar_right"></a>
                        </div>
                    </div>
                </div>
                <div class="mv_comm_short">
                    <div class="title clearfix">
                        <h2>影评</h2>
                        <span id="commCount_S" class="tip">（共 <em>${movie.comment.size()}</em> 条）</span>
                        <a href="javascript:;" id="writeScommt" class="btn_e34551 btn_89_29">发表新影评</a>
                    </div>

                    <div class="no_short_warp" id="noShortPart">
                        <div class="no_short_box">
                            <b class="icon_no"></b>
                            <span class="text">
										<span class="h">还木有影评！你来说两句吧！</span>
								</span>
                        </div>
                    </div>
                </div>
                <dl class="list clearfix" id="sCommentList">

                    <c:forEach items="${movie.comment}" var="comt">
                        <dd sid="55284">
                            <div class="infor">
                                <div class="name">来自用户-${comt.userName}</div>
                                <div class="time">${comt.commentDate}</div>
                            </div>
                            <div class="detail">${comt.info}</div>
                        </dd>
                    </c:forEach>

                    <%--<dd sid="55284">
                        <div class="infor">
                            <div class="name">来自用户-${movie.comment}</div>
                            <div class="time">2017-07-08</div>
                        </div>
                        <div class="detail">白天播还是晚上？？</div>
                    </dd>
                    <dd sid="55281" class="last">
                        <div class="infor">
                            <div class="name"> 来自QQ的用户-45044</div>
                            <div class="time">2017-07-08</div>
                        </div>
                        <div class="detail">怎么购票</div>
                    </dd>--%>

                </dl>
                <div id="moreScommt" class="more hide">
                    <a href="javascript:;">再显示<em>20</em>条影评</a>
                </div>
            </div>


            <div class="mv_comment_box" id="part2" style="display: block;">
                <div class="movieTabC pos_r movieTabC_in clearfix">
                    <dl class="area clearfix">
                        <dt>日期：</dt>
                        <dd>
                            <ul class="areaList areaList_new overflow clearfix areaList_hidden timeTabsList"
                                id="timeTabs">
                                <li day="20170719" class="active"><i class="low"></i>
                                    <a href="javascript:;"><i class="sub"></i>今天 07-19（周三）</a>
                                </li>
                                <li day="20170720"><i class="low"></i>
                                    <a href="javascript:;"><i class="sub"></i>明天 07-20（周四）</a>
                                </li>
                                <li day="20170721"><i class="low"></i>
                                    <a href="javascript:;"><i class="sub"></i>明天 07-21（周五）</a>
                                </li>
                                <li day="20170722"><i class="low"></i>
                                    <a href="javascript:;"><i class="sub"></i>明天 07-22（周六）</a>
                                </li>
                            </ul>
                        </dd>
                    </dl>
                </div>


                <div class="movieTabC" id="dateContent">

                    <div class="movieTabC_in">
                        <dl id="districtList" class="area clearfix" style="background:none;">
                            <dt>区域：</dt>
                            <dd style="position:relative;z-index:20;">
                                <ul id="district_areaList" class="areaList areaList_new clearfix"
                                    style="float:left;margin:0;padding:0;">
                                    <li id="all_cinema" ctype="ALL" typeid="1" class="all_cinema active"
                                        style="margin:0;">
                                        <a href="javascript:;" class="sel_cinema"
                                           onfocus="this.blur();"><span>全部影院</span><i class="sub"></i></a>
                                        <span class="sel_cinema_left">[筛选　]<i class="triangle"></i></span>

                                        <div id="notesTip" class="notesTip">
                                            <em class="left"></em>可以展开筛选哦~&nbsp;&nbsp;&nbsp;<i class="close"></i>
                                        </div>
                                        <script>
                                            //全部影院处只显示一次的 tips
                                            if (!$.cookie('notesTipShow')) {
                                                $('#notesTip').removeClass('hide');
                                            }
                                        </script>
                                        <div id="all_type_box" class="all_type_box hide">
                                            <i></i>
                                            <a href="javascript:;" class="close" onfocus="this.blur();"></a>
                                            <div class="type_module noBack clearfix " ctype="ALL">
                                                <div class="type_name">全部：</div>
                                                <div class="type_detail">
                                                    <a class="all active" typeid="1"
                                                       href="javascript:;"><span>全部影院</span><em>(3)</em></a>
                                                </div>
                                            </div>
                                            <div class="type_module clearfix" ctype="District">
                                                <div class="type_name">区域：</div>
                                                <div class="type_detail">
                                                    <a href="javascript:;"
                                                       typeid="1310"><span>高新区</span><em>(1)</em></a>
                                                    <a href="javascript:;"
                                                       typeid="1383"><span>吴中区</span><em>(1)</em></a>
                                                    <a href="javascript:;"
                                                       typeid="1225"><span>工业园区</span><em>(1)</em></a>
                                                </div>
                                            </div>
                                        </div>

                                    </li>
                                    <li ctype="LowPrice" style="margin:0;margin-left:55px;" class=""><a
                                            href="javascript:;" onfocus="this.blur();"><i class="sub"></i>最低价影院&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><em
                                            class="lowPrice">¥33</em></li>
                                </ul>
                                <div class="cinema_search_box">
                                    <div id="cinema_search_list" class="cinema_search_list hide"></div>
                                    <input type="text" value="快速搜索影院" id="cinema_search" class="cinema_search textGray">
                                    <a id="cinema_search_btn" href="javascript:;" class="cinema_search_btn"></a>
                                </div>
                            </dd>
                        </dl>
                    </div>


                    <div id="areaContent" style="zoom:1;">
                        <div class="movieTabC_in" style="padding-bottom:19px;">
                            <div class="mv_cinema_rs">
                                <b class="arrow "></b>
                                <div style="">
                                    <ul class="areaList areaList_new clearfix mv_cinema_list" id="cinamaList">
                                        <li class="active" cid="10721" isseatsupportnow="1"><a title="KM影城-苏州店"
                                                                                               href="javascript:;"><i
                                                class="sub"></i>KM影城-苏州店</a></li>
                                        <li cid="5782" isseatsupportnow="1"><a title="星美国际影城李公堤店" href="javascript:;"><i
                                                class="sub"></i>星美国际影城李公堤店</a></li>
                                        <li cid="4945" isseatsupportnow="1"><a title="星美国际影城苏州金鸡" href="javascript:;"><i
                                                class="sub"></i>星美国际影城苏州金鸡</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div id="cinemaContent" class="mv_cinema_sch">

                            <div class="orangeLine" id="ticketHead">
                                <h3><a href="http://piao.163.com/cinema/10721.html" id="ticketCinemaName"><strong>KM影城-苏州店</strong></a>
                                    <span class="score" style="vertical-align:middle;">7.<em class="s">0</em></span>
                                    <span class="icon_z"></span>
                                    <span class="icon_q" style="display:none;"></span>
                                    <span class="icon_t" style="display:none;"></span>
                                    <span class="mtype_imax"></span>
                                </h3>
                                <div class="add">地址：高新区 浒墅关城际路19号永旺梦乐城3楼
                                    <a href="javascript:;" id="10721" title="查看地图"
                                       onclick="Core.cinemaMapDialog(this.id);">[查看地图]</a>
                                </div>
                            </div>


                            <ul class="movie_s_tab" id="mvSubTabs">
                                <li class="active" rel="#subPart1"><span class="seat">在线选座</span></li>
                                <li rel="#subPart2" id="pqDhqTab" class=" "><span class="pq">影讯排期</span></li>
                            </ul>
                            <div class="tab_border_bottom"></div>
                            <div class="movie_s_cont" id="subPart1" style="display:block;">
                                <table class="cinemaAdd" width="100%" id="cnTbl">
                                    <thead>
                                    <tr>
                                        <th width="18%">放映时间</th>
                                        <th width="10%">
			                <span class="copy" id="dmspan">版本<b></b>
			                    <ul class="sel_copy" id="dmFilter">
			                        <li><a href="javascript:;"><span class="mtype_imax"></span></a></li>
			                        <li><a href="javascript:;"><span class="mtype mtype_2d"></span></a></li>
			                        <li><a href="javascript:;"><span class="mtype mtype_3d"></span></a></li>
			                        <li><a href="javascript:;"><span class="mtype mtype_4d"></span></a></li>
			                        <li><a href="javascript:;"><span class="mtype mtype_num"></span></a></li>
			                        <li><a href="javascript:;"><span class="mtype mtype_jp"></span></a></li>
			                    </ul>
			                </span></th>
                                        <th width="14%">语言</th>
                                        <th width="17%">放映厅</th>
                                        <th width="10%">选座预览</th>
                                        <th width="16%">原价/优惠价</th>
                                        <th width="15%">选座购票</th>
                                    </tr>
                                    </thead>
                                    <tbody id="movieTbody" class="movieTbodyAct">
                                    <c:forEach items="${schList}" var="sch">
                                        <tr dm="3D" class="nobg">
                                            <td class="time">
                                                <fmt:formatDate value="${sch.schDate}" type="both" pattern="HH:mm"/>
                                                        <br>
                                                        <span class="time_end">预计13:45结束</span>
                                            </td>

                                            <td>
                                                ${sch.movie.frame}
                                            </td>
                                            <td>
                                                ${sch.movie.area}
                                            </td>
                                            <td>3(请自备3D眼镜)</td>
                                            <td class="preview_seat">
                                                <div class="hallWrap" tid="593401413">
                                                    <span class="hallShow"></span>
                                                    <div class="hallBar"></div>
                                                    <div class="hallTip">
                                                        <i></i>
                                                        <div class="load_text seat_box" seatload="false">
                                                            <img src="./悟空传-在线购票,兑换券,上映时间,预告片-网易电影_files/loading2.gif"
                                                                 width="20" height="20" alt="">
                                                            <div>加载中</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>¥<em class="old">${sch.oldPrice}</em><em class="fav ml10"><i>¥</i>${sch.newPrice}</em></td>
                                            <td>
                                                <a href="${path}/movie/check?schId=${sch.schId}" class="btn_e34551 btn_89_29" rbtn="buy">选座购票</a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    
                                    </tbody>
                                </table>
                            </div>

                            <div id="subPart2" class="movie_s_cont" style="display: none;">
                                <div id="time_cont">
                                    <dl class="scheduleAll noBg" style="border-top:0;">
                                        <dt>《悟空传》苏州星美国际影城李公堤店的排期为：</dt>
                                        <dd>
                                            <div class="date"><em>07-19</em>周三</div>
                                            <div class="time">
                                                <p><label class="mtype mtype_all">全</label>
                                                    <span class=" out">09:30</span>
                                                    <span class=" out">11:45</span>
                                                    <span class=" out">14:00</span>
                                                    <span class=" out">16:15</span>
                                                    <span class=" out">19:15</span>
                                                    <span class=" out">20:10</span>
                                                    <span class="l">22:25</span>
                                                </p>
                                            </div>
                                        </dd>
                                        <dd>
                                            <div class="date"><em>07-20</em>周四</div>
                                            <div class="time">
                                                <p><label class="mtype mtype_all">全</label>
                                                    <span class="">09:30</span>
                                                    <span class="">11:45</span>
                                                    <span class="">14:00</span>
                                                    <span class="">16:15</span>
                                                    <span class="">19:30</span>
                                                    <span class="">20:10</span>
                                                    <span class="">21:50</span>
                                                    <span class="l">22:25</span>
                                                </p>
                                            </div>
                                        </dd>
                                        <dd>
                                            <div class="date"><em>07-21</em>周五</div>
                                            <div class="time">
                                                <p><label class="mtype mtype_all">全</label>
                                                    <span class="">13:25</span>
                                                    <span class="l">15:45</span>
                                                </p>
                                            </div>
                                        </dd>
                                        <dd>
                                            <div class="date"><em>07-22</em>周六</div>
                                            <div class="time">
                                                <p><label class="mtype mtype_all">全</label>
                                                    <span class="">15:25</span>
                                                    <span class="l">21:15</span>
                                                </p>
                                            </div>
                                        </dd>
                                        <dd>
                                            <div class="date"><em>07-23</em>周日</div>
                                            <div class="time">
                                                <p><label class="mtype mtype_all">全</label>
                                                    <span class="">15:25</span>
                                                    <span class="">21:15</span>
                                                    <span class="l">22:05</span>
                                                </p>
                                            </div>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="noWaiting noBg" id="noWaiting" style="display:none;">
                                    <b></b>排期还在路上，再等等~
                                </div>
                            </div>


                            <script type="text/javascript">
                                (function (window, $) {
                                    if (Core.isInFrame) {
                                        //邮箱应用链接增加前缀
                                        var len = $("#cinemaContent").find("a[rBtn]").length;
                                        var rBtn = $("a[rBtn]");
                                        $("#cinemaContent").find(".groupBtn").attr("target", "_blank");//特惠新窗口跳到主站
                                        for (var i = 0; i < len; i++) {
                                            $(rBtn[i]).attr("href", "/mailapp" + $(rBtn[i]).attr("href").replace("http://piao.163.com", ""));
                                        }
                                    }
                                })(window, jQuery);
                            </script>
                        </div>
                        <script type="text/javascript">
                            //126邮箱应用没有影院相关页面，删除链接
                            (function (window, $) {
                                if (Core.isInFrame) {
                                    //邮箱应用链接增加前缀
                                    $("#ticketCinemaName").attr("href", "javascript:;").css({
                                        "cursor": "default",
                                        "text-decoration": "none"
                                    });
                                }
                            })(window, jQuery);
                        </script>
                    </div>
                </div>
            </div>

        </section>
        <section class="siderBox">
            <dl class="sider_hot_box">
                <dt class="title">正在热映</dt>
                <dd>
                    <ul class="sider_hot_mv clearfix">
                        <c:forEach items="${movieList}" var="mv">
                            <li mid="48,542">
                                <div class="poster">
                                    <a href="${path}/movie/detail?mvId=?${mv.id}" target="_blank" title="${mv.mvName}">
                                        <i class="sanD"></i>
                                        <img src="${path}/images/movieImg/${mv.cover.imgName}" alt="${mv.mvName}" width="70" height="93"></a>
                                </div>
                                <dl>
                                    <dt>
                                    <h2><a href="${path}/movie/detail?mvId=?${mv.id}" target="_blank" title="${mv.mvName}">${mv.mvName}</a></h2>
                                    </dt>
                                    <dd class="mv_star">
                                        <span class="star_bg_s"><div style="width:75%" class="star_s"></div></span>
                                    </dd>
                                    <dd class="summary" title="${mv.explain}">${mv.explain}</dd>
                                    <dd class="des" title="">主演：
                                        <c:forEach items="${mv.mainAct}" var="ac">
                                            ${ac.name}&nbsp;
                                        </c:forEach>
                                    </dd>
                                </dl>
                            </li>
                        </c:forEach>
                    </ul>
                </dd>
            </dl>
        </section>
    </article>
</div>
<%--<form id="formTest" action="http://www.baidu.com/" method="get" target="_blank"></form>
<script>
    //用来计算评论时间
    Core.nowTime = '2017/07/19 11:32:28';
</script>
<script type="text/javascript" src="./悟空传-在线购票,兑换券,上映时间,预告片-网易电影_files/api"></script>
<script type="text/javascript" src="./悟空传-在线购票,兑换券,上映时间,预告片-网易电影_files/getscript"></script>
<link rel="stylesheet" href="${path}/css/bmap.css">--%>


<footer id="docFoot">
    <div class="foot-wap">
        <div class="footCont">
            <div class="footLeft">
                <a href="http://piao.163.com/" class="logo"></a>
            </div>
            <div class="footRight">
                <ul class="footRightTop">
                    <li><b class="b1"></b>订好座，不排队</li>
                    <li><b class="b2"></b>优惠多，价格低</li>
                    <li><b class="b3"></b>渠道多，影院多</li>
                </ul>
                <div class="footRightBot">
                    <span>• <a href="http://mall.163.com/help/movie.html" target="_blank" rel="nofollow">常见问题</a></span>
                    <span>• <a href="http://feedback.zxkf.163.com/movie/show.html?flag=1" target="_blank"
                               rel="nofollow">提提意见</a></span>
                    <span>• 商务合作：010-82558368</span>
                    <span>• 客服电话：0571-26201163</span>
                </div>
            </div>
        </div>
    </div>
    <div id="aboutNEST">
        <a href="http://corp.163.com/eng/about/overview.html" rel="nofollow" target="_blank">About NetEase</a> - <a
            href="http://gb.corp.163.com/gb/about/overview.html" rel="nofollow" target="_blank">公司简介</a> - <a
            href="http://gb.corp.163.com/gb/contactus.html" rel="nofollow" target="_blank">联系方法</a> - <a
            href="http://corp.163.com/gb/job/job.html" rel="nofollow" target="_blank">招聘信息</a> - <a
            href="http://help.163.com/" rel="nofollow" target="_blank">客户服务</a> - <a
            href="http://gb.corp.163.com/gb/legal.html" rel="nofollow" target="_blank">相关法律</a> - <a
            href="http://emarketing.biz.163.com/" rel="nofollow" target="_blank">网络营销</a> - <a
            href="http://piao.163.com/" target="_blank">网易电影</a><br>
        增值电信业务经营许可证：浙B2-20110418&nbsp;|&nbsp;<a href="http://www.lede.com/prove.html" target="_blank">网站相关资质证明</a><br>
        网易乐得科技有限公司版权所有 ©2011-2017

    </div>
</footer>
<div id="sideBar" class="">
    <a id="feedback" href="http://feedback.zxkf.163.com/movie/show.html?flag=1" rel="nofollow" target="_blank"
       title="提意见" hidefocus="true"></a>
    <a id="toTop" href="javascript:;" rel="nofollow" title="回到顶部" hidefocus="true"></a>
</div>
sr

<script src="${path}/js/share.js"></script>
<script src="${path}/js/movie/mvCommon.js"></script>
<script src="${path}/js/imgSlide.js"></script>
<script src="${path}/js/imgZoom.js"></script>
<script src="${path}/js/photoview.js"></script>
<script src="${path}/js/mvCinema.js"></script>
<script src="${path}/js/comment.js"></script>
<script src="${path}/js/mvDetail.js"></script>
<script>Core && Core.fastInit && Core.fastInit("1");</script>
<div id="autoCompleteList"></div>
<script type="text/javascript">
    Core.statistics_adsage("ca");
</script>

<script src="${path}/js/ntes.js"></script>
<script>_ntes_nacc = "dianying";
neteaseTracker();</script>


</body>
</html>
