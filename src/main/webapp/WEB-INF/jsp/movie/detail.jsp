<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: bruce
  Date: 2017/7/24
  Time: 9:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.hbm.util.DateUtil" %>
<!DOCTYPE html>
<html>

<head>
    <link rel="shortcut icon" href="http://piao.163.com/favicon.ico">
    <title>${movie.mvName}(2017)-在线购票,兑换券,上映时间,预告片-网易电影</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="${movie.mvName}(2017),兑换券,上映时间,电影排期,预告片">
    <meta name="description" content="枫林电影是一个能够让您在线购买电影票的在线选座平台，这里有最详实的雄狮(2017)资讯，高清的预告片，还有最及时的影讯排期等。看电影，来枫林电影选座">

    <meta name="mobile-agent" content="format=html5;url=http://piao.163.com/wap/movie/detail.html?movieId=48507&amp;cityCode=110000">
    <meta name="mobile-agent" content="format=xhtml;url=http://piao.163.com/wap/movie/detail.html?movieId=48507&amp;cityCode=110000">
    <meta name="mobile-agent" content="format=wml;url=http://piao.163.com/wap/movie/detail.html?movieId=48507&amp;cityCode=110000">
    <link rel="stylesheet" type="text/css" href="${path}/css/base.css" />
    <link rel="stylesheet" type="text/css" href="${path}/css/core.css" />
    <link rel="stylesheet" type="text/css" href="${path}/css/detail_new.css" />
    <script src="${path}/js/jquery-1.js" type="text/javascript" charset="utf-8"></script>
    <script src="${path}/js/easyCore.js" type="text/javascript" charset="utf-8"></script>

    <script>
        if(!!window.Core) {
            Core.cdnBaseUrl = "http://pimg1.126.net/movie";
            Core.cdnFileVersion = "1495696265";
            Core.curCity = {
                'name': '北京',
                'id': '1006',
                'spell': 'beijing'
            };
        }
    </script>

    <style type="text/css">
        @-webkit-keyframes loginPopAni {
            0% {
                opacity: 0;
                -webkit-transform: scale(0);
            }
            15% {
                -webkit-transform: scale(0.667);
            }
            25% {
                -webkit-transform: scale(0.867);
            }
            40% {
                -webkit-transform: scale(1);
            }
            55% {
                -webkit-transform: scale(1.05);
            }
            70% {
                -webkit-transform: scale(1.08);
            }
            85% {
                opacity: 1;
                -webkit-transform: scale(1.05);
            }
            100% {
                opacity: 1;
                -webkit-transform: scale(1);
            }
        }

        @keyframes loginPopAni {
            0% {
                opacity: 0;
                transform: scale(0);
            }
            15% {
                transform: scale(0.667);
            }
            25% {
                transform: scale(0.867);
            }
            40% {
                transform: scale(1);
            }
            55% {
                transform: scale(1.05);
            }
            70% {
                transform: scale(1.08);
            }
            85% {
                opacity: 1;
                transform: scale(1.05);
            }
            100% {
                opacity: 1;
                transform: scale(1);
            }
        }
    </style>
</head>

<body>

<%@include file="../common/header.jsp"%>

<div class="wrap990">
    <section class="mv_info_box clearfix">
        <div id="share" class="share">
            <div class="share_inner">影片分享到<b></b>
                <div class="shareDiv" id="shareDiv">
                    <a class="cpShareLink" rel="neteasyweibo" href="http://t.163.com/article/user/checkLogin.do?link=http://piao.163.com&amp;source=%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1&amp;info=%E9%9B%84%E7%8B%AE%282017%29-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8%2C%E5%85%91%E6%8D%A2%E5%88%B8%2C%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4%2C%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1http%3A%2F%2Fpiao.163.com%2Fbeijing%2Fmovie%2F48507.html%23from%3Dt.163&amp;togImg=true&amp;images=" title="网易微博" target="_blank"><em class="cpShareIcon neteasyweibo"></em></a>
                    <a class="cpShareLink" rel="sinaweibo" href="http://service.weibo.com/share/share.php?url=http%3A%2F%2Fpiao.163.com%2Fmovie%2F48451.html%23from%3Dt.sina&title=%E5%BC%82%E5%BD%A2%EF%BC%9A%E5%A5%91%E7%BA%A6-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8%2C%E5%85%91%E6%8D%A2%E5%88%B8%2C%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4%2C%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1&searchPic=true&pic=" title="新浪微博" target="_blank"><em class="cpShareIcon sinaweibo"></em></a>
                    <a class="cpShareLink" rel="qqweibo" href="http://share.v.t.qq.com/index.php?c=share&amp;a=index&amp;site=http://caipiao.163.com&amp;url=http%3A%2F%2Fpiao.163.com%2Fbeijing%2Fmovie%2F48507.html%23from%3Dt.qq&amp;title=%E9%9B%84%E7%8B%AE%282017%29-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8%2C%E5%85%91%E6%8D%A2%E5%88%B8%2C%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4%2C%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1&amp;pic=" title="腾讯微博" target="_blank"><em class="cpShareIcon qqweibo"></em></a>
                    <a class="cpShareLink" rel="qqzone" href="http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=http%3A%2F%2Fpiao.163.com%2Fbeijing%2Fmovie%2F48507.html%23from%3Dt.qqzone&amp;desc=%E9%9B%84%E7%8B%AE%282017%29-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8%2C%E5%85%91%E6%8D%A2%E5%88%B8%2C%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4%2C%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1http%3A%2F%2Fpiao.163.com%2Fbeijing%2Fmovie%2F48507.html%23from%3Dt.qqzone&amp;summary=%20&amp;title=%E9%9B%84%E7%8B%AE%282017%29-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8,%E5%85%91%E6%8D%A2%E5%88%B8,%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4,%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1&amp;site=http://caipiao.163.com&amp;otype=share&amp;pics=" title="QQ空间" target="_blank"><em class="cpShareIcon qqzone"></em></a>
                    <a class="cpShareLink" rel="renren" href="http://widget.renren.com/dialog/share?resourceUrl=http%3A%2F%2Fpiao.163.com%2Fbeijing%2Fmovie%2F48507.html%23from%3Dt.renren&amp;title=%E9%9B%84%E7%8B%AE%282017%29-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8%2C%E5%85%91%E6%8D%A2%E5%88%B8%2C%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4%2C%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1&amp;description=%E9%9B%84%E7%8B%AE%282017%29-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8%2C%E5%85%91%E6%8D%A2%E5%88%B8%2C%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4%2C%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1http%3A%2F%2Fpiao.163.com%2Fbeijing%2Fmovie%2F48507.html%23from%3Dt.renren&amp;charset=utf-8&amp;pic=" title="人人网" target="_blank"><em class="cpShareIcon renren"></em></a>
                    <a class="cpShareLink" rel="kaixin" href="http://www.kaixin001.com/rest/records.php?content=%E9%9B%84%E7%8B%AE%282017%29-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8%2C%E5%85%91%E6%8D%A2%E5%88%B8%2C%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4%2C%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1&amp;url=http%3A%2F%2Fpiao.163.com%2Fbeijing%2Fmovie%2F48507.html%23from%3Dt.kaixin&amp;starid=0&amp;aid=0&amp;style=11&amp;pic=" title="开心网" target="_blank"><em class="cpShareIcon kaixin"></em></a>
                    <a class="cpShareLink" rel="douban" href="http://shuo.douban.com/%21service/share?href=http%3A%2F%2Fpiao.163.com%2Fbeijing%2Fmovie%2F48507.html%23from%3Dt.douban&amp;name=%E9%9B%84%E7%8B%AE%282017%29-%E5%9C%A8%E7%BA%BF%E8%B4%AD%E7%A5%A8%2C%E5%85%91%E6%8D%A2%E5%88%B8%2C%E4%B8%8A%E6%98%A0%E6%97%B6%E9%97%B4%2C%E9%A2%84%E5%91%8A%E7%89%87-%E7%BD%91%E6%98%93%E7%94%B5%E5%BD%B1&amp;image=" title="豆瓣网" target="_blank"><em class="cpShareIcon douban"></em></a>
                </div>
            </div>
        </div>
        <div class="poster">
            <img src="${path}/images/movieImg/${movie.cover.imgName}" alt="${movie.mvName}" width="200" height="267">
        </div>
        <dl class="mv_info">
            <dt class="overflow">
                <span class="mv_name"><h2>${movie.mvName}</h2></span>
                <span class="star_bg ml10">
	                    <div class="star" style="width:${movie.score/10*100}%"></div>
	                </span>
                <span class="score_big">${movie.score}</span>
            </dt>
            <dd class="summary">"${movie.explain}"</dd>
            <dd class="des">上映：<em class="">${movie.showDate}</em></dd>
            <dd class="des">导演：${movie.director}</dd>
            <dd class="des role" id="role" style="height: 50px; overflow: hidden;">
                <span style="float:left;">主演: </span>
                <div class="role_list" id="roleList">
                    <c:forEach items="${movie.mainAct}" var="act">
                        <span>
                                ${act.name}
                        </span>
                    </c:forEach>
                </div>
            </dd>
            <dd class="other"><span>${movie.frame}</span><span>${movie.area}</span><span>${movie.type}</span><span>${movie.mvTime}</span>
            </dd>
        </dl>
        <script>
            Core.movieId = 48507;
        </script>
    </section>
    <article class="mv_body_990 clearfix mt10">
        <section class="mainContent">
            <div id="pq">
                <ul class="mv_detail_tab" id="mvTabs">
                    <li rel="#part1" id="commentTab" class="active">剧情影评</li>
                </ul>
            </div>

            <div class="mv_comment_box" id="part1" style="">

                <div class="mv_comm_plot">
                    <div class="title">
                        <h2>剧情简介：</h2>
                    </div>
                    <p>${movie.info}
                    </p>
                </div>
                <div class="mv_comm_plot">
                    <div class="title">
                        <h2>剧照</h2>
                        <span class="tip">（<em>${movie.images.size()}</em> 张）</span>
                    </div>
                    <div class="mv_comm_photo ">
                        <div class="photo_bar ">
                            <a href="javascript:;" id="leftPBtn" class="photo_bar_left noLeft"></a>
                        </div>
                        <div class="photo_list_box">
                            <ul class="photo_list" id="sphotoList">
                                <script>
                                    Core.bigPhotoUrls = ["http://pimg1.126.net/movie/product/stills/149560338651611384_webp.jpg", "http://pimg1.126.net/movie/product/stills/149560338600411381_webp.jpg", "http://pimg1.126.net/movie/product/stills/149560338453711375_webp.jpg", "http://pimg1.126.net/movie/product/stills/149560338545511378_webp.jpg", "http://pimg1.126.net/movie/product/stills/149560338376311372_webp.jpg", "http://pimg1.126.net/movie/product/stills/149560338321611369_webp.jpg", "http://pimg1.126.net/movie/product/stills/149560338266211366_webp.jpg", "http://pimg1.126.net/movie/product/stills/149560338206611363_webp.jpg", "http://pimg1.126.net/movie/product/stills/149560338151411360_webp.jpg", "http://pimg1.126.net/movie/product/stills/149560338093911357_webp.jpg"]
                                </script>
                                <c:forEach items="${movie.images}" var="img" begin="0" end="4">
                                    <li>
                                        <a href="javascript:;" ><img src="${path}/images/movieImg/ju/${img.imgName}" alt="" title="" style="margin-left: 20px"><span></span></a>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="photo_bar ">
                            <a href="javascript:;" id="rightPBtn" class="photo_bar_right"></a>
                        </div>
                    </div>
                </div>
                <div class="mv_comm_short">
                    <div class="title clearfix">
                        <h2>影评</h2>
                        <span id="commCount_S" class="tip">（共 <em>${movie.comment.size()}</em> 条）</span>
                        <a href="javascript:;" id="writeScommt" class="btn_e34551 btn_89_29">发表新影评</a>
                    </div>

                    <div class="no_short_warp" id="noShortPart" style="display: block;">
                        <div class="no_short_box">
                            <b class="icon_no"></b>
                            <span class="text">
						<span class="h">还木有影评！你来说两句吧！</span>
									</span>
                        </div>
                    </div>

                    <dl class="list clearfix" id="sCommentList">

                    </dl>
                    <div id="moreScommt" class="more hide">
                        <a href="javascript:;">再显示<em>20</em>条影评</a>
                    </div>
                </div>

            </div>

        </section>
        <section class="siderBox">
            <dl class="sider_hot_box">
                <dt class="title">正在热映</dt>
                <dd>
                    <ul class="sider_hot_mv clearfix">
                        <c:forEach items="${movieList}" var="mv">
                            <li mid="48,542">
                                <div class="poster">
                                    <a href="${path}/movie/detail?mvId=?${mv.id}" target="_blank" title="${mv.mvName}">
                                        <i class="sanD"></i>
                                        <img src="${path}/images/movieImg/${mv.cover.imgName}" alt="${mv.mvName}" width="70" height="93"></a>
                                </div>
                                <dl>
                                    <dt>
                                    <h2><a href="${path}/movie/detail?mvId=?${mv.id}" target="_blank" title="${mv.mvName}">${mv.mvName}</a></h2>
                                    </dt>
                                    <dd class="mv_star">
                                        <span class="star_bg_s"><div style="width:75%" class="star_s"></div></span>
                                    </dd>
                                    <dd class="summary" title="${mv.explain}">${mv.explain}</dd>
                                    <dd class="des" title="">主演：
                                        <c:forEach items="${mv.mainAct}" var="ac">
                                            ${ac.name}&nbsp;
                                        </c:forEach>
                                    </dd>
                                </dl>
                            </li>
                        </c:forEach>

                    </ul>
                </dd>
            </dl>
        </section>
    </article>
</div>
<%--<form id="formTest" action="http://www.baidu.com" method="get" target="_blank"></form>
<script>
    //用来计算评论时间
    Core.nowTime = '2017/07/19 10:35:07';
</script>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.2"></script>
<link rel="stylesheet" type="text/css" href="${path}/css/bmap.css">--%>

<%@include file="../common/footer.jsp"%>

<div id="sideBar" class="none">
    <a id="feedback" href="http://feedback.zxkf.163.com/movie/show.html?flag=1" rel="nofollow" target="_blank" title="提意见" hidefocus="true"></a>
    <a id="toTop" href="javascript:;" rel="nofollow" title="回到顶部" hidefocus="true"></a>
</div>
<script src="${path}/js/share.js" type="text/javascript" charset="utf-8"></script>
<script src="${path}/js/mvCommon.js" type="text/javascript" charset="utf-8"></script>
<script src="${path}/js/imgSlide.js" type="text/javascript" charset="utf-8"></script>
<script src="${path}/js/imgZoom.js" type="text/javascript" charset="utf-8"></script>
<script src="${path}/js/photoview.js" type="text/javascript" charset="utf-8"></script>
<script src="${path}/js/mvCinema.js" type="text/javascript" charset="utf-8"></script>
<script src="${path}/js/comment.js" type="text/javascript" charset="utf-8"></script>
<script src="${path}/js/mvDetail.js" type="text/javascript" charset="utf-8"></script>
<script>
    Core && Core.fastInit && Core.fastInit("1");
</script>
<div id="autoCompleteList"></div>
<script type="text/javascript">
    Core.statistics_adsage("ca");
</script>
<script src="${path}/js/ntes.js" type="text/javascript" charset="utf-8"></script>
<script>
    _ntes_nacc = "dianying";
    neteaseTracker();
</script>

</body>

</html>
