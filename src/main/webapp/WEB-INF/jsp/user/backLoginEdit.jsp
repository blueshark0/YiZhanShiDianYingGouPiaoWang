<%@ page import="java.net.URLDecoder" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Jinx's
  Date: 2017/7/24
  Time: 13:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>登录页面</title>
    <link rel="stylesheet" type="text/css" href="${path}/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="${path}/css/login.css" />
</head>

<body>
<!--<script src="http://cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>-->
<script src="${path}/dist/js/jquery.min.js" type="text/javascript" charset="utf-8"></script>

<!-- 提供一个jsp代码块 -->
<%
    String userNum = "";
    String password = "";
    Cookie[] cookies = request.getCookies();

    if(null != cookies && cookies.length > 0){
        for(Cookie c:cookies){
            if("userNum".equals(c.getName())){
                //获取该cookie的value
                String urr = c.getValue();
                //根据:进行切割
                String[] values = urr.split(":");
                //同样需要对用户名进行一级解码
                userNum = URLDecoder.decode(values[0],"utf-8");
                password = values[1];
            }
        }
    }
    request.setAttribute("userNumc", userNum);
    request.setAttribute("passwordc", password);
%>

<div class="continer">
    <div class="wraps">
        <div class="foot">
            <form role="form" action="${path}/user/backLogin" method="post">
                <span class="login">用户密码登录</span>
                <div class="p">
                    <p>
                        <c:if test="${!empty userNumErr}">
                        <span class="com-md-3">
                            <label class="alert-danger">${userNumErr}</label>
                        </span>
                        </c:if>

                        <c:if test="${!empty userPasswordErr}">
                        <span class="com-md-3">
                            <label class="alert-danger">${userPasswordErr}</label>
                        </span>
                        </c:if>
                    </p>
                </div>
                <div>
                    <div class="innerbox" id="innerbox">
                        <input type="text" name="userNum" placeholder="请输入用户名" value="${empty userNumc? userNum : userNumc}">
                        <span class="glyphicon  form-control-feedback"></span>
                    </div>
                </div>
                <div>
                    <div class="innerbox">
                        <input type="password" name="password" placeholder="请输入密码" value="${empty passwordc? password : passwordc}">
                        <span class="glyphicon  form-control-feedback"></span>
                    </div>
                </div>
                <div>
                    <div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check" checked="${check}">请记住我
                            </label>
                        </div>
                    </div>
                </div>
                <div>
                    <button type="submit" class="btn">登录</button>
                    <a href="${path}/user/backRegisterEdit" class="regist">注册</a>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">

</script>

</html>
