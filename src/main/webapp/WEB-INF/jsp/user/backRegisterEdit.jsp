<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/7/25
  Time: 9:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>用户注册</title>
    <link rel="stylesheet" type="text/css" href="${path}/css/register.css"/>
</head>
<body>
<div id="container">
    <div class="label">用户注册</div>
    <br /><br />
    <div id="tw-outer">
        <div id="tw">
            <form action="${path}/user/backRegister" method="post">
                <div id='user' class='outerDiv'>
                    <label for="user">用户帐户&nbsp;&nbsp;</label>
                    <input type="user" name="userNum" value="${user.userNum}"/>
                    <c:if test="${!empty userErr}">
                        <div class='message' id='nameDiv'>${userErr}</div>
                    </c:if>
                    <c:if test="${!empty userNumErr}">
                        <div class='message' id='nameDiv'>${userNumErr}</div>
                    </c:if>
                </div>
                <div class='clearfix'></div>
                <div id='username' class='outerDiv'>
                    <label for="username">昵称&nbsp;&nbsp;</label>
                    <input type="text" name="userName" value="${user.userName}" />
                    <%--<div class='message' id='usernameDiv'>用户名已存在</div>--%>
                </div>
                <div class='clearfix'></div>
                <div id='password' class='outerDiv'>
                    <label for="password">密码&nbsp;&nbsp;</label>
                    <input type="password" name="password" />
                    <c:if test="${!empty passwordErr}">
                        <div class='message' id='websiteDiv'>${passwordErr}</div>
                    </c:if>
                </div>
                <div class='clearfix'></div>
                <div id='password' class='outerDiv'>
                    <label for="password">确认密码&nbsp;&nbsp;</label>
                    <input type="password" name="pwd1" />
                    <c:if test="${! empty password1Err}">
                        <div class='message' id='websiteDiv'>${password1Err}</div>
                    </c:if>
                </div>
                <div class='clearfix'></div>
                <div id='email' class='outerDiv'>
                    <label for="email">邮箱&nbsp;&nbsp;</label>
                    <input type="email" name="email" value="${user.email}"/>
                    <c:if test="${!empty emailErr}">
                        <div class='message' id='emailDiv'>${emailErr}</div>
                    </c:if>
                </div>

                <%--<div class='clearfix'></div>
                <div id='email' class='outerDiv'>
                    <label for="email">性别&nbsp;&nbsp;</label>
                    <div style="display: inline-flex;" >
                        <input type="radio" name="sex" style="width: 30px;"/>男
                        <input type="radio" name="sex" checked="checked" style="width: 30px;" />女
                    </div>
                </div>--%>

                <div class='clearfix'></div>
                <div id='phone' class='outerDiv'>
                    <label for='phone'>手机号&nbsp;&nbsp;</label>
                    <input type="text" name="phone" value="${user.phone}"/>
                    <c:if test="${!empty phoneErr}">
                        <div class='message' id='emailDiv'>${phoneErr}</div>
                    </c:if>
                </div>
                <div class='clearfix'></div>
                <div id='submit' class='outerDiv'>
                    <input type="submit" value="注册帐号" />
                </div>
                <div class='clearfix'></div>
            </form>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
</body>
</html>
