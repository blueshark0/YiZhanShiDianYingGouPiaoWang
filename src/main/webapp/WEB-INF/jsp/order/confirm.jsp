<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/7/27
  Time: 9:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!-- saved from url=(0068)http://piao.163.com/order/pay.html?order_id=2017072010GORDER63485894 -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" href="http://piao.163.com/favicon.ico">
    <title>枫林晚电影-选座购票,购买电影票,在线支付,电影院查询</title>

    <meta name="keywords" content="枫林晚电影,选座购票,买电影票,在线购票,支付,热映影片">
    <meta name="description" content="枫林晚电影是一个能够让您在线购买电影票的在线选座平台，同时枫林晚电影还提供电影排期，影院信息查询等服务，方便您足不出户，在家中在线支付。看电影，来枫林晚电影选座">
    <meta http-equiv="X-Frame-Options" content="DENY">


    <link rel="stylesheet" href="${path}/css/base.css">
    <link rel="stylesheet"  href="${path}/css/core.css">
    <link rel="stylesheet" href="${path}/css/detail.css">
    <script src="${path}/js/jquery-1.4.2.js"></script>

    <script src="${path}/js/easyCore.js"></script>
    <script src="${path}/js/dialog.js"></script>
    <script src="${path}/js/autoComplete.js"></script>


    <script>
        if(!!window.Core){
            Core.cdnBaseUrl="http://pimg1.126.net/movie";
            Core.cdnFileVersion="1495696265";
            Core.curCity={'name':'苏州','id':'1017','spell':'suzhou'};
        }
    </script>




    <script src="${path}/js/xframe.js"></script>
    <style type="text/css">@-webkit-keyframes loginPopAni{0%{opacity:0;-webkit-transform:scale(0);}15%{-webkit-transform:scale(0.667);}25%{-webkit-transform:scale(0.867);}40%{-webkit-transform:scale(1);}55%{-webkit-transform:scale(1.05);}70%{-webkit-transform:scale(1.08);}85%{opacity:1;-webkit-transform:scale(1.05);}100%{opacity:1;-webkit-transform:scale(1);}}@keyframes loginPopAni{0%{opacity:0;transform:scale(0);}15%{transform:scale(0.667);}25%{transform:scale(0.867);}40%{transform:scale(1);}55%{transform:scale(1.05);}70%{transform:scale(1.08);}85%{opacity:1;transform:scale(1.05);}100%{opacity:1;transform:scale(1);}}</style></head>
<body>
<%@include file="../common/header.jsp"%>


<div class="wrap">
    <form action="http://piao.163.com/order/to_pay.html?order_id=2017072010GORDER63485894" id="payForm" method="post" target="_blank">
        <div class="procedure3"></div>
        <div class="box_gray mt10">
            <ul class="playList sel_playList pay_playList clearfix">
                <input type="hidden" value="593411258" id="productId">
                <input type="hidden" value="2017072010ORDER63484376" id="order_id" name="orderid">
                <input type="hidden" value="2017072010GORDER63485894" id="displayGorderId" name="displayGorderId">
                <li>
                    <div class="poster"><img src="${path}/images/movieImg/${schedule.movie.cover.imgName}" width="68" height="90" alt="${schedule.movie.mvName}"></div>
                    <dl>
                        <dt>
                        <h2>${schedule.movie.mvName}</h2>
                        </dt>
                        <dd class="summary" style="overflow:hidden;">"${schedule.movie.explain}"</dd>
                        <dd class="des">影院：${schedule.cinema.cmName}-${schedule.cinema.cmAddress.city}店</dd>
                        <dd class="des">场次：<fmt:formatDate value="${schedule.schDate}" pattern="yyyy-MM-dd HH:mm (EEEE)"/> </dd>

                    </dl>
                    <table cellpadding="0" cellspacing="0" border="0">
                        <thead>
                        <tr>
                            <th width="25%" class="tel">
                                <div class="">
                                    <div class="notes"><em class="em1"></em><em class="em2"></em>请确认该号码无误，以免无法接收取票密码和打印电影票!</div>
                                    手机号码<b id="telWarming"></b>
                                </div>
                            </th>
                            <th width="20%">原价(元)</th>
                            <th width="18%">优惠价(元)</th>
                            <th width="22%">数量</th>
                            <th width="15%" style="padding-left:19px;text-align:left;">总额(元)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr id="onShowTR">
                            <td>${userOrder.phone}</td>
                            <td class="mk">¥${schedule.oldPrice}</td>
                            <td>¥${schedule.newPrice}</td>
                            <td>${userOrder.items.size()}</td>
                            <td class="count" id="totalPrice" style="padding-left:19px;text-align:left;"><span class="imp">¥${userOrder.allPrice}</span></td>
                        </tr>
                        </tbody>
                    </table>

                    <%--<div class="coupon_num" id="coupon_num">
                        <div class="title">使用优惠码：</div>
                        <div class="coupon_num_left">

                            <div class="use_coupon_row">
                                <input type="radio" name="use_radio" value="0" autocomplete="off" id="use_radio_1" class="rd_css"><label for="use_radio_1">使用我的优惠券</label>
                                <select id="userFavSel" autocomplete="off" class="select" style="display:inline-block">
                                    <option value="0000000000" selected="selected">暂无可用优惠券</option>
                                </select>
                            </div>

                            <div class="use_coupon_row">
                                <input type="radio" name="use_radio" value="1" autocomplete="off" id="use_radio_2" class="rd_css"><label for="use_radio_2">手动输入优惠码</label>
                                <input type="text" class="text textGray" id="userFavText" maxlength="10" value="输入十位优惠码" autocomplete="off" style="display: inline-block">
                                <span class="fav_err" id="fav_err" style="display: block"></span>
                            </div>

                        </div>
                        <span class="price" id="favPrice">-¥<em>0</em></span>
                    </div>--%>

                    <input type="hidden" name="code" value="" id="code">
                    <div class="coupon_count" style="padding-bottom:20px;border-bottom:1px dotted #808080;">
                        <div class="m">应付金额：<em id="totalFavPrice">¥${userOrder.allPrice}</em></div>
                        <%--<div class="tip">支付成功后可获得<span id="scoreNum">96</span>分积分</div>--%>
                    </div>

                    <div class="timer"><script type="text/javascript">Core.closeTime ="898",Core.productId ="593411258"</script>
                        <div id="last" class="last">
                            <div>
                                <span class="title"><b></b>剩余支付时间：</span>
                                <span class="time">
		                    	<span class="min" id="min">14</span>分
		                        <span class="sec" id="sec">24</span>秒
		                    </span>
                            </div>
                            <div class="notes">请在倒计时内完成付款！否则系统将自动释放已选座位。</div>
                        </div>
                        <div id="invalid" class="invalid" style="display:none;">
                            <span class="intitle"><b></b>该订单已失效</span>
                            <span class="btn ml8"><a href="http://piao.163.com/order/seat.html?ticket_id=593411258&amp;seatArea=1">重新选座</a></span>
                        </div>
                    </div>
                    <div id="methodUserBox" style="display:none;">
                        <div class="selPayMethod payMethod mt30">
                            <a href="javascript:;" class="edit" id="payUserEdit">修改支付方式</a>
                            <div class="box ml10">
                                <input type="hidden" checked="checked" value="" id="bank_sel_0000" name="paymethod">
                                <label><span class="bank_img " id="bankImg"></span></label>
                            </div>
                            <h3>：</h3>
                        </div>
                        <div class="subPay">
                            <div class="sub" style="padding-top:20px;"><input type="button" value="确认无误，去付款" id="subPay1"></div>
                            <div class="agree">
                                <input type="checkbox" id="btnAgree1" checked="checked" style="position:relative;margin-right:0;_margin-bottom:5px;">
                                <label>本人同意并接受<a href="http://mall.163.com/protocol.html" target="_blank">枫林晚商城服务协议</a></label>
                            </div>
                        </div>
                    </div>
                    <div id="methodBox" style="display: block;">
                        <div class="subPay">
                            <div class="sub" style="_zoom:1;">
                                <%--<input type="button" value="选择支付方式" id="payEdit" onclick="pay()"></div>--%>
                                    <a href="${path}/order/list" >选择支付方式</a>
                        </div>
                    </div>
                    <div id="exchangeBox" style="display:none;">
                        <div class="subPay">
                            <div class="sub" style="padding-top:20px;"><input type="button" value="立即兑票" id="exchangePay"></div>
                            <div class="agree">
                                <input type="checkbox" id="btnAgree3" checked="checked" style="position:relative;margin-right:0;_margin-bottom:5px;">
                                <label>本人同意并接受<a href="http://mall.163.com/protocol.html" target="_blank">枫林晚商城服务协议</a></label>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </form>
</div>
<script type="text/javascript">
    order={'id':'2017072010ORDER63484376','amount':'96'};
</script>


<%@include file="../common/footer.jsp"%>

<script src="${path}/js/pay.js"></script>
<script>Core && Core.fastInit && Core.fastInit("1");</script><div id="autoCompleteList"></div>

<script src="${path}/js/ntes.js"></script>
<script>_ntes_nacc = "dianying";neteaseTracker();</script>


<div id="cover"></div>
<form>
    <div class="payment" id="payment" style="top: 49.5px; visibility: hidden;">
        <div class="title">
            <h2><a href="${path}/order/list" >选择支付方式</a></h2>
            <%--<div class="close" id="closeEdit" onclick="closeEdit()"></div>--%>
        </div>

        <dl class="dt_net">
            <dt>快捷支付：</dt>
            <dd>
                <ul class="payTab" id="payTab">
                    <li rel="#payTabC_1" class="active">请扫码支付</li>
                </ul>
                <div id="payTabC_1">
                    <ul class="payList payMethod">
                    </ul>
                    <h3>二维码</h3>
                    <ul class="payList otherBank other payMethod">

                    </ul>
                    <div class="showAllBank"><b></b>其他支付方式</div>
                </div>
            </dd>
        </dl>
    </div>
</form>
<script>
    /*function pay(){
        var i = document.getElementById("payment").style.visibility;
        document.getElementById("payment").style.visibility = "visible"
    }
    function closeEdit(){
        var i = document.getElementById("payment").style.visibility;
        if(i="visible"){
            document.getElementById("payment").style.visibility = "hidden"
        }

    }*/
</script>

</body>
</html>