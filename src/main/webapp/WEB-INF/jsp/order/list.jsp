<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/7/28
  Time: 11:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!-- saved from url=(0041)http://order.mall.163.com/movie/list.html -->
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" href="${path}/order/list">

    <meta name="keywords" content="枫林晚商城,购买电影票,话费充值,点卡直充,枫林晚电影票">
    <meta name="description" content="枫林晚商城是一个集电影票、手机话费、游戏点卡的一个生活服务类消费平台，为用户提供便捷的在线购物充值服务">
    <title>万东商城</title>
    <link rel="stylesheet" type="text/css" href="${path}/css/order/base.css"/>
    <link rel="stylesheet" type="text/css" href="${path}/css/order/bmap.css"/>
    <link rel="stylesheet" type="text/css" href="${path}/css/order/core.css"/>
    <link rel="stylesheet" type="text/css" href="${path}/css/order/iSelect.css"/>
    <link rel="stylesheet" type="text/css" href="${path}/css/order/myorder.css"/>

    <script src="${path}/js/order/jquery-1.4.2.js" type="text/javascript" charset="utf-8"></script>
    <script src="${path}/js/order/dialog.js" type="text/javascript" charset="utf-8"></script>
    <script src="${path}/js/order/easyCore.js" type="text/javascript" charset="utf-8"></script>
    <script src="${path}/js/order/iSelect.js" type="text/javascript" charset="utf-8"></script>
    <script src="${path}/js/order/list.js" type="text/javascript" charset="utf-8"></script>


</head>

<body>
<noscript><div id="noScript">
    <div>
        <h2>请开启浏览器的Javascript功能</h2><p>亲，没它我们玩不转啊！求您了，开启Javascript吧！<br/>不知道怎么开启Javascript？那就请<a href="http://www.baidu.com/s?wd=%E5%A6%82%E4%BD%95%E6%89%93%E5%BC%80Javascript%E5%8A%9F%E8%83%BD" target="_blank">猛击这里</a>！</p></div>
</div>
</noscript>
<nav id="topNav">
    <div id="topNavWrap">
        <div id="topNavLeft">枫林晚电影
            <c:if test="${!empty user || !empty user.userName}">
                <a href="#" >欢迎您 - ${user.userName}</a>
            </c:if>
            <c:if test="${empty user}">
                <a href="${path}/user/backLoginEdit">登录</a>
                <a href="${path}/user/backRegisterEdit">立即注册&gt;&gt;</a>
            </c:if>

        </div>
        <ul id="topNavRight">
            <li>
                <c:if test="${empty user}">
                <a href="${path}/user/backLoginEdit" target="_blank">我的订单</a>&nbsp;&nbsp;<span id="topEpayInfo"></span>|</li>
            </c:if>
            <c:if test="${!empty user}">
                <a href="${path}/order/list" target="_blank">我的订单</a>&nbsp;&nbsp;<span id="topEpayInfo"></span>|</li>
            </c:if>

            <c:if test="${!empty user}">
                <a href="${path}/user/safeExit" > 安全退出</a>
            </c:if>
        </ul>
        <script>
            Core.getUnPayedOrderCount();
        </script>
    </div>
</nav>
<header id="docHead">
    <div id="docMvHeadWrap">
        <a href="${path}" class="logoLnk1" title="枫林晚商城" hidefocus="true">
            <img src="${path}/images/logos/mall.png" alt="枫林晚商城" title="枫林晚商城">
        </a>
        <a href="${path}" class="logoLnk2" title="枫林晚电影票" hidefocus="true">
            <img src="${path}/images/piao.jpg" alt="枫林晚电影票" title="枫林晚电影票">
        </a>
    </div>
</header>
<div class="ctLocation">
    <div class="topLine"></div>
    <div class="location">您的位置：
        <a href="${path}">首页</a><em>&gt;</em>我的电影票</div>
</div>
<article class="ctMain clearfix">
    <section class="ctSider mt10">
        <ul class="leftNav" id="leftNav">
            <li>

                <a href="${path}/order/list" class="out">我的帐户</a>
            </li>
            <li class="active">

                <a href="${path}/order/list" class="out">我的电影票</a>
                <dl>
                    <dd class="active">
                        <a href="${path}/order/list">我的订单</a>
                    </dd>
                    <dd>
                        <a href="${path}/order/list">我的电影票优惠券</a>
                    </dd>
                    <dd>
                        <a href="${path}/order/list">我的电影票积分</a>
                    </dd>
                </dl>
            </li>
        </ul>
    </section>
    <form action="${path}/order/list" method="post" id="form1">
        <section class="ctCont mt10">
            <div class="ctSearch">
                <div class="left">
                    <span>交易时间：</span>
                    <div class="ctTimer">
                        <a href="${path}/order/list" id="otimeInput" class="iSelect input"><span style="display: block;">所有</span></a>
                        <input type="hidden" name="orderTime" id="orderTime" value="">
                    </div>
                </div>
                <div class="right">
                    <input type="text" value="请输入电影名称、影院名称、订单号" class="text textGray" name="searchValue" id="searchValue">
                    <input type="submit" value="搜索" class="sub" id="search">
                </div>
            </div>
            <table class="ctTable">
                <thead>
                <tr>
                    <th width="16%" class="num SimSun">订单号</th>
                    <th width="16%">下单日期</th>
                    <th width="12%">影片</th>
                    <th width="15%">影院</th>
                    <th width="13%">票数(张) / 总额(元)</th>
                    <th width="28%" colspan="3">
                        <div class="ctTimer">
                            <a href="${path}/order/list" id="statusInput" class="iSelect input"><span style="display: block;">所有</span></a>
                        </div>
                        <input type="hidden" name="orderStatus" id="orderStatus" value="">

                    </th>
                </tr>
                </thead>
            </table>
            <c:forEach items="${orders}" var="order">
                <dl class="ctList active">
                    <dt>
                    <table>
                        <thead>
                        <tr>
                            <th width="16%" class="num SimSun" sk="1">${order.orderNum}</th>
                            <th width="16%" class="SimSun">
                                <fmt:formatDate value="${order.createDate}"  pattern="yyyy-MM-dd HH:mm"/>
                            </th>
                            <c:forEach items="${order.items}" var="item" begin="0" end="0">
                                <th width="10%" title="" sk="1">${item.schedule.movie.mvName}</th>
                                <th width="15%" title="${item.schedule.cinema.cmName}-${item.schedule.cinema.cmAddress.city}店" sk="1">
                                        ${item.schedule.cinema.cmName}-${item.schedule.cinema.cmAddress.city}店
                                </th>
                            </c:forEach>
                            <th width="10%">${order.items.size()}张/
                                <em class="count">
                                    <fmt:formatNumber value="${order.allPrice}" maxFractionDigits="2"/>
                                </em>
                            </th>
                            <th width="12%">${order.orederStatus}</th>
                            <th width="13%">
                                <a href="${path}/movie/list" target="_blank" class="resBuy">重新购票</a>
                            </th>
                            <th width="8%">
                                <span class="detail">详情</span>
                            </th>
                        </tr>
                        </thead>
                    </table>
                    </dt>
                    <dd>
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                            <tr>
                                <th><span class="blank">&nbsp;</span>订票类型：</th>
                                <td>
                                    订座票
                                    <span class="blank">&nbsp;</span>
                                </td>
                            </tr>
                            <tr>
                                <th><span class="blank">&nbsp;</span>付款时间：</th>
                                <td>
                                    ${order.orederStatus}
                                    <span class="blank">&nbsp;</span>
                                </td>
                            </tr>
                            <tr>
                                <th><span class="blank">&nbsp;</span>手机号码：</th>
                                <td>${order.phone}<span class="blank">&nbsp;</span></td>
                            </tr>
                            <c:forEach items="${order.items}" var="it" begin="0" end="0">
                                <tr>
                                    <th><span class="blank">&nbsp;</span>放映时间：</th>
                                    <td>
                                        <fmt:formatDate value="${it.schedule.schDate}" pattern="yyyy-MM-dd(EEEE) HH:mm"/>
                                        <span class="blank">&nbsp;</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th><span class="blank">&nbsp;</span>座位信息：</th>
                                    <td>
                                        全景声影厅(请自备3D眼镜）
                                        ${order.items.size()}张座
                                        <c:forEach items="${order.items}" var="tt">
                                            (${tt.seat.seatX}排${tt.seat.seatY}座)
                                        </c:forEach>
                                        <span class="blank">&nbsp;</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th><span class="blank">&nbsp;</span>影院地址：</th>
                                    <td>${it.schedule.cinema.cmAddress.city}
                                        ${it.schedule.cinema.cmName}
                                        <span class="blank">&nbsp;</span>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </dd>
                </dl>
            </c:forEach>

            <input type="hidden" name="recordPerPage" value="20">
            <input type="hidden" name="currentPage" id="currentPage">
        </section>
    </form>
</article>

<%--<script type="text/javascript" src="./枫林晚商城-购买电影票,话费充值,点卡直充-枫林晚电影票_files/api"></script>
<script type="text/javascript" src="./枫林晚商城-购买电影票,话费充值,点卡直充-枫林晚电影票_files/getscript"></script>
<link rel="stylesheet" type="text/css" href="${path}/css/bmap.css"/>--%>
<script type="text/javascript">
    window.onload = function() {
        //统计页面加载完成
        try {
            neteaseTracker(true, "${path}/order/list", "我的订单", null);
        } catch(e) {}
    }
</script>

<footer id="docFoot">
    <div class="foot-wap">
        <ul id="guideList" class="clearfix">
            <li class="first">
                <a href="${path}" target="_blank"><img title="枫林晚商城" src="${path}/images/logos/mall.png"></a>
            </li>
            <li><em class="guide_2"><b></b>客服电话</em><span>
					·<a href="javascript:;" class="noLink">话费/点卡&nbsp;0571-26201163</a><br>
					·<a href="javascript:;" class="noLink">彩票&nbsp;0571-26201163</a><br>
					·<a href="javascript:;" class="noLink">保险&nbsp;0571-26201163</a><br>
				</span></li>
            <li><em class="guide_3"><b></b>支付相关</em><span>
					·<a target="_blank" href="${path}help/pay.html#Q2">枫林晚宝余额支付</a><br>
					·<a target="_blank" href="${path}help/pay.html#Q3">网银/支付宝支付</a><br>
					·<a target="_blank" href="${path}help/pay.html#Q5">手机充值卡支付</a><br>
					·<a target="_blank" href="${path}help/payProblem.html">支付异常</a>
		        </span></li>
            <li><em class="guide_4"><b></b>商务合作</em><span>
					·<a target="_blank" href="${path}contactUs.html" rel="nofollow">合作联系</a><br>
					·<a target="_blank" href="http://emarketing.163.com/" rel="nofollow">广告加盟</a><br>
				</span></li>
            <li><em class="guide_5"><b></b>友情链接</em><span>
					·<a target="_blank" href="${path}/order/list">枫林晚宝</a>&nbsp;
		            <a target="_blank" href="${path}/order/list">优惠券</a><br>
		            ·<a target="_blank" href="${path}/order/list">枫林晚购卡直通车</a><br>
			        ·<a target="_blank" href="${path}/order/list">魔兽在线购卡</a><br>
		            ·<a target="_blank" href="${path}/order/list">美美</a>
				</span></li>
        </ul>
    </div>
    <div id="aboutNEST">
        <a href="${path}/order/list" rel="nofollow" target="_blank">About NetEase</a> -
        <a href="${path}/order/list" rel="nofollow" target="_blank">公司简介</a> -
        <a href="${path}/order/list" rel="nofollow" target="_blank">联系方法</a> -
        <a href="${path}/order/list" rel="nofollow" target="_blank">招聘信息</a> -
        <a href="${path}/order/list" rel="nofollow" target="_blank">客户服务</a> -
        <a href="${path}/order/list" rel="nofollow" target="_blank">相关法律</a> -
        <a href="${path}/order/list" rel="nofollow" target="_blank">网络营销</a><br> 网络文化经营许可证：苏网文[2017]xxxx-xxx号&nbsp;|&nbsp;增值电信业务经营许可证：苏B1-xxxxxxxx&nbsp;|&nbsp;
        <a href="${path}/order/list" target="_blank">网站相关资质证明</a><br> 枫林晚科技有限公司版权所有 ©2011-2017
    </div>
</footer>

<script>
    Core && Core.fastInit && Core.fastInit("1");
</script>
<script src="${path}/js/order/ntes.js" type="text/javascript" charset="utf-8"></script>
<script>
    _ntes_nacc = "shop";
    neteaseTracker();
</script>

<div id="iOptions"></div>
</body>

</html>