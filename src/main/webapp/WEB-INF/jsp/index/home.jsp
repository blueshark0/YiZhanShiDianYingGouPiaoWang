<%--
  Created by IntelliJ IDEA.
  User: brcue
  Date: 2017/7/21
  Time: 8:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" import="java.util.Date" %>
<%@ page import="com.hbm.util.DateUtil" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<html xmlns:wb="http://open.weibo.com/wb">

<head>
    <link rel="shortcut icon" href="${path}/images/favicon.ico" />
    <title>枫林晚影院欢迎您!</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="枫林晚,电影,影院" />
    <meta name="description" content="枫林晚电影是一个能够让您在线购买电影票的在线选座平台，同时网易电影还提供电影排期，影院信息查询等服务，方便您足不出户，在家中在线购票。" />
    <meta name="baidu-site-verification" content="JbG7IdK46dmV88mo" />
    <meta name="baidu-site-verification" content="YikRVdr4Vs" />
    <meta http-equiv="X-Frame-Options" content="DENY" />
    <link rel="stylesheet" href="${path}/dist/css/bootstrap.min.css">
    <script src="${path}/dist/js/jquery.min.js"></script>
    <script src="${path}/dist/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="${path}/css/base.css" />
    <link rel="stylesheet" href="${path}/css/core.css" />
    <link rel="stylesheet" href="${path}/css/index/index2014.css" />
    <link rel="stylesheet" href="${path}/css/mv_list.css" />
    <script src="${path}/js/jquery-1.4.2.js"></script>
    <script src="${path}/js/easyCore.js"></script>
    <script src="${path}/js/js2/dialog.js"></script>
    <script src="${path}/js/autoComplete.js"></script>
    <script src="${path}/js/wb.js" type="text/javascript" charset="utf-8"></script>

    <script>
        if(!!window.Core) {
            Core.cdnBaseUrl = "http://pimg1.126.net/movie";
            Core.cdnFileVersion = "1495696323";
            Core.curCity = { 'name': '北京', 'id': '1006', 'spell': 'beijing' };
        }
    </script>

    <script src="${path}/js/xframe.js"></script>


    <style>
        .modal-header {
            text-align: center;
            background-color: #E34551;
            border-radius: 5px;
            letter-spacing: 7px;
            font-size: large;
        }

        ul li{
            margin-left: 10px;
            /*margin-top: 40px;*/
        }

        .hotCinema li{
            width: 915px;
            height: 98px;
        }
        #mvTopSearch{
            pos-right: 0px;
        }
        .search{
            padding: 0px;
        }
    </style>
</head>

<body>

<noscript><div id="noScript">
    <div>
        <h2>请开启浏览器的Javascript功能</h2><p>亲，没它我们玩不转啊！求您了，开启Javascript吧！<br/>不知道怎么开启Javascript？那就请<a href="http://www.baidu.com/s?wd=%E5%A6%82%E4%BD%95%E6%89%93%E5%BC%80Javascript%E5%8A%9F%E8%83%BD" target="_blank">猛击这里</a>！</p></div>
</div>
</noscript>
<nav id="topNav">
    <div id="topNavWrap">
        <div id="topNavLeft">枫林晚电影
            <c:if test="${!empty user || !empty user.userName}">
                <a href="#" >欢迎您 - ${user.userName}</a>
            </c:if>
            <c:if test="${empty user}">
                <a href="#" data-toggle="modal" data-target="#login">登录</a>
                <a href="#" data-toggle="modal" data-target="#register">立即注册&gt;&gt;</a>
            </c:if>

        </div>
        <ul id="topNavRight">
            <li>
            <c:if test="${empty user}">
                <a href="${path}/user/backLoginEdit" target="_blank">我的订单</a>&nbsp;&nbsp;<span id="topEpayInfo"></span>|</li>
            </c:if>
            <c:if test="${!empty user}">
                <a href="${path}/order/list" target="_blank">我的订单</a>&nbsp;&nbsp;<span id="topEpayInfo"></span>|</li>
            </c:if>

            <c:if test="${!empty user}">
                | <a href="${path}/user/safeExit" > 安全退出</a>
            </c:if>
            <li class="last">
                <a href="javascript:;" rel="nofollow" target="_blank" onMouseOver="$(this).parent().addClass('kf');" onMouseOut="$(this).parent().removeClass('kf');">联系客服</a>&nbsp;&nbsp;
                <div class="none">客服电话：0512-12345678</div>
            </li>
        </ul>
        <script>
            Core.getUnPayedOrderCount();
        </script>
    </div>
</nav>
<section class="searchBoxInd clearfix2">
    <div class="searchWrap">
        <a href="homePage.html" class="logo2014" title="枫林晚电影" style="float:left;"></a>
        <div id="switchTopCity" class="switchTopCity">
            <div class="curCity  " id="curCity" pid="1006" pspell="beijing">
                <span class="cityName myCityBar" id="myCity" pid="1006" pspell="beijing">苏州</span>
                <i class="triangle2"></i>
                <input id="cityUrl" class="cityUrl" type="hidden" value="/beijing/movie/page-1-type-0.html">
                <div class="cityList" id="cityTopList">
                    <div class="title">
                        <a href="javascript:;" class="close"></a>
                        <input type="text" class="cityTopSearch textGray" value="请输入城市或城市拼音" autocomplete="off" maxlength="15">
                        <input type="button" title="" class="cityTopSearchBtn" value="">
                        <ul class="titleChar">
                            <li class="on first" rel="#cityList_0">热门</li>
                            <li class="" rel="#cityList_1">A~G</li>
                            <li class="" rel="#cityList_2">H~L</li>
                            <li class="" rel="#cityList_3">M~T</li>
                            <li class="" rel="#cityList_4">W~Z</li>
                        </ul>
                    </div>
                    <div id="cityListBox" class="cityListBox">
                        <div id="cityList_0" class="cityListGroup hotCity">
                            <dl>
                                <dd>
                                    <a href="/beijing/movie/page-1-type-0.html" rel="nofollow">北京</a>
                                    <a href="/shanghai/movie/page-1-type-0.html" rel="nofollow">上海</a>
                                    <a href="/guangzhou/movie/page-1-type-0.html" rel="nofollow">广州</a>
                                    <a href="/shenzhen/movie/page-1-type-0.html" rel="nofollow">深圳</a>
                                    <a href="/hangzhou/movie/page-1-type-0.html" rel="nofollow">杭州</a>
                                    <a href="/nanjing/movie/page-1-type-0.html" rel="nofollow">南京</a>
                                    <a href="/chengdu/movie/page-1-type-0.html" rel="nofollow">成都</a>
                                    <a href="/chongqing/movie/page-1-type-0.html" rel="nofollow">重庆</a>
                                </dd>
                            </dl>
                        </div>
                        <div id="cityList_1" class="cityListGroup none">
                            <dl>
                                <dt>A</dt>
                                <dd>
                                    <a href="/anlu/movie/page-1-type-0.html" rel="nofollow">安陆</a>
                                    <a href="/anning/movie/page-1-type-0.html" rel="nofollow">安宁</a>
                                    <a href="/ankang/movie/page-1-type-0.html" rel="nofollow">安康</a>
                                    <a href="/anshun/movie/page-1-type-0.html" rel="nofollow">安顺</a>
                                    <a href="/anyang/movie/page-1-type-0.html" rel="nofollow">安阳</a>
                                    <a href="/anqing/movie/page-1-type-0.html" rel="nofollow">安庆</a>
                                    <a href="/anshan/movie/page-1-type-0.html" rel="nofollow">鞍山</a>
                                </dd>
                            </dl>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <ul class="shift">
            <li>
                <a class="active" href="${path}" rel="nofollow">首页</a>
            </li>
            <li class="movie" id="movieLink">
                <a class="" href="javascript:;">电影<i class="triangle2"></i></a>
                <dl id="movieMenu">
                    <dd>
                        <a href="onshow.html" rel="nofollow">正在热映</a>
                    </dd>
                    <dd>
                        <a href="upComing.html" rel="nofollow">即将上映</a>
                    </dd>
                </dl>
            </li>
            <li>
                <a class="" href="cinema.html" rel="nofollow">影院</a>
            </li>
        </ul>
        <div class="search" >
            <div class="ie6">
                <form action="/search.html#from=search" id="top_sform">
                    <input type="text" value="${mvName}" placeholder="请输入影片名称" class="text textGray" name="keywords" id="mvTopSearch" autocomplete="off" maxlength="20"
                           style="bottom: 0px;right: 0px; width: 208px;height: 36px;"/>
                    <input type="hidden" name="city" value="suzhou" />
                    <input type="submit" value="" class="sub" id="topSearchBtn" title="" />
                </form>
            </div>
        </div>
    </div>
</section>
<div class="photo_box">
    <script>
        Core.autoBanner = true;
    </script>
    <a href="javascript:;" class="photo_big_bar photo_big_left"><b style="display: inline;"></b></a>
    <a href="javascript:;" class="photo_big_bar photo_big_right"><b style="display: block;"></b></a>
    <div class="photo_b_box">
        <ul class="photo_b_list ">
            <li style="background-image: url(${path}/images/bianxingjinggang.jpg);background-color: #070709;">
                <a href="purcase.html" target="_blank"></a>
            </li>
            <li style="background-image: url(${path}/images/xiongshi.jpg);background-color: #f1cf4a;">
                <a href="purcase.html" target="_blank"></a>
            </li>
            <li style="background-image: url(${path}/images/yixing.jpg);background-color: #020001;">
                <a href="purcase.html" target="_blank"></a>
            </li>
            <li style="background-image: url(${path}/images/shenqinvxia.jpg);background-color: #02100a;">
                <a href="purcase.html" target="_blank"></a>
            </li>
        </ul>
    </div>
</div>

<!-- 登录模态框（Modal） -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    用户登录
                </h4>
            </div>
            <div class="modal-body">
                <!--登录的form表单-->
                <form class="form-horizontal" role="form" onsubmit="return doLogin('p')" method="post">
                    <div class="form-group has-feedback" id="div1">
                        <label for="name" class="col-sm-2 control-label">用户名</label>
                        <div class="col-sm-5" id="user">

                            <input type="text" class="form-control" id="name" placeholder="请输入用户名" name="username"
                                   onblur="a()" value="${uNum}">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="col-sm-3" style="padding-top: 10px;">
                            <label class="alert-danger" id="uErr"></label>
                        </div>
                    </div>
                    <div class="form-group has-feedback" id="div2">
                        <label for="pwd" class="col-sm-2 control-label">密码</label>
                        <div class="col-sm-5" id="pd">
                            <input type="text" class="form-control" id="pwd" placeholder="请输入密码" name="password"
                                   onblur="a()" value="${uPwd}">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="col-sm-3" style="padding-top: 10px;">
                            <label class="alert-danger" id="pErr"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="flag">请记住我
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        <input type="submit" class="btn btn-primary" value="登录" <%--onclick="doLogin('p')"--%>>
                    </div>
                </form>
                <!--form结束-->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- 登录modal end/.modal -->

<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myModalRegister" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalRegister">
                    用户注册
                </h4>
            </div>
            <div class="modal-body">
                <!--注册的form表单-->
                <form action="${path}/user/modelRegister" class="form-horizontal" role="form"
                      onsubmit="return doRegister('p')" method="post">
                    <div class="form-group has-feedback">
                        <label for="userNum" class="col-sm-2 control-label">用户账号</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="userNum" name="userNum"  placeholder="小写字母开头,不含中文.">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="col-sm-3" style="padding-top: 10px;">
                            <label class="alert-danger" id="labelu" >请输入合法用户账户</label>
                        </div>
                    </div>

                    <div class="form-group has-feedback">
                        <label for="userName" class="col-sm-2 control-label">昵称</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="userName" name="userName"  placeholder="小写字母开头,不含中文.">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="col-sm-3" style="padding-top: 10px;">
                            <label class="alert-danger">请输入合法昵称</label>
                        </div>
                    </div>

                    <div class="form-group has-feedback">
                        <label for="passw" class="col-sm-2 control-label">密码</label>
                        <div class="col-sm-5">
                            <input type="password" class="form-control" id="passw" name="password"  placeholder="密码长度6-8位" >
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="col-sm-3" style="padding-top: 10px;">
                            <label class="alert-danger">请输入合法密码</label>
                        </div>
                    </div>

                    <div class="form-group has-feedback">
                        <label class="col-sm-2 control-label">确认密码</label>
                        <div class="col-sm-5">
                            <input type="password" class="form-control" id="passw2" name="passw2"   placeholder="和密码保持一致">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="col-sm-5" style="padding-top: 10px;">
                            <label class="alert-danger">两次密码必须一致</label>
                        </div>
                    </div>

                    <div class="form-group has-feedback">
                        <label class="col-sm-2 control-label">邮箱</label>
                        <div class="col-sm-5">
                            <input type="email" class="form-control" id="email" name="email"  placeholder="合法邮箱格式">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <div class="col-sm-3" style="padding-top: 10px;">
                            <label class="alert-danger">请输入合法邮箱</label>
                        </div>
                    </div>

                    <div class="form-group has-feedback">
                        <label class="col-sm-2 control-label">电话</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="tel" name="phone"  placeholder="合法手机格式">
                            <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
                        </div>
                        <div class="col-sm-3" style="padding-top: 10px;">
                            <label class="alert-danger">输入合法手机格式</label>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        <input type="submit" class="btn btn-primary" value="注册">
                    </div>

                </form>
                <!--form结束-->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>

<section class="bodyMain mainCont1">
    <a class="fastBuyNav " id="fastBuyNav" href="javascript:;"></a>
    <section class="mainCont clearfix">

        <div class="mainLeft">
            <h2 class="colTitle">
                <span class="colText">热映电影推荐</span>
                <span class="colTip">北京共有24部热映电影，104家影院支持购票</span>
                <a href="onshow.html" rel="nofollow" target="_blank" class="colMore">全部热映电影&gt;</a>
            </h2>
            <div class="hotMovie clearfix">

                <C:forEach items="${movieList}" var="movie" begin="0" end="1">
                    <ul class="movie_con">
                        <li class="l1">
                            <div class="showImg">
                                <em class="mvType mvType3d"></em>
                                <a target="_blank" title="${movie.mvName}" href="${path}/movie/detail?mvId=${movie.id}" rel="nofollow"><img width="220" height="300" alt="${movie.mvName}" src="${path}/images/movieImg/${movie.cover.imgName}"></a>
                            </div>
                        </li>
                        <li class="l2" style="height: 300px;">
                            <h3>
                                <a target="_blank" title="${movie.mvName}"  href="${path}/movie/detail?mvId=${movie.id}" rel="nofollow">${movie.mvName}</a>
                            </h3>
                            <p class="p2">
                                <span class="star_bg"><b style="width:${movie.score / 10 *100}%" class="star"></b></span>
                                <em>${movie.score}</em>
                            </p>
                            <p class="p2">${movie.showDate}上映</p>
                            <p class="p2" style="font-size:12px;">${movie.explain}</p>

                            <%--<span class="lowPrice">未获取<i>元起</i></span>--%>
                            <a target="_blank" class="showBtn" href="${path}/movie/detail?mvId=${movie.id}" rel="nofollow">选座购票</a>
                        </li>
                    </ul>
                </C:forEach>

            </div>
            <div class="hotMovie2 clearfix">

                <ul class="posterStyle clearfix">
                    <c:forEach items="${movieList}" var="movie" begin="2" end="5">
                        <li>
                            <div class="showImg">
                                <a target="_blank" title="${movie.mvName}" href="${path}/movie/detail?mvId=${movie.id}" rel="nofollow"><img width="220" height="300" alt="${movie.mvName}" src="${path}/images/movieImg/${movie.cover.imgName}" title="图片加载异常"></a>
                            </div>
                            <div class="m_con">
                                <em>${movie.score}</em>
                                <%--<strong>未获取</strong>元起--%>

                            </div>
                            <br>
                            <p>
                                <a target="_blank" class="showBtn" href="${path}/movie/detail?mvId=${movie.id}" rel="nofollow">选座购票</a>
                            </p>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
        <div class="mainRight">
            <div class="side1">
                <div class="side1Client"></div>
                <dl class="side1Pro">
                    <dt>购票流程</dt>
                    <dd><b class="b1"></b>1.选择影院和场次</dd>
                    <dd><b class="b2"></b>2.在线选座位并支付</dd>
                    <dd><b class="b3"></b>3.短信获取取票码</dd>
                    <dd><b class="b4"></b>4.凭码自助取票</dd>
                </dl>
            </div>
        </div>
    </section>
    <section class="mainCont clearfix mt15">
        <div class="mainLeft upIndex">
            <h2 class="showTitle clearfix">
                <a href="upComing.html" rel="nofollow" class="colMore" target="_blank">全部即将上映电影&gt;</a>
                <a href="javascript:;" class="active upMovieTab" rel="upMovie1">最受期待</a><span class="fg">/</span><a href="javascript:;" class="upMovieTab" rel="upMovie2">即将上映</a>
            </h2>
            <div class="upMovie" id="upMovie1">
                <ul class="posterStyle clearfix">
                    <c:forEach items="${popularList}" var="pm">
                        <li>
                            <i class="dot"></i>
                            <div class="wantNum"><em class="wantSeeSum">${pm.popular}</em>人想看</div>
                            <div class="showImg">
                                <a target="_blank" title="${pm.mvName}" href="${path}/movie/detail?mvId=${pm.id}" rel="nofollow"><img width="220" height="300" alt="${pm.mvName}" src="${path}/images/movieImg/${pm.cover.imgName}"></a>
                            </div>
                            <div class="title">
                                <span class="playTime2">${pm.showDate} 上映</span>
                                <p class="t1">
                                    <a target="_blank" title="${pm.mvName}" href="/beijing/movie/48491.html" rel="nofollow">${pm.mvName}</a>
                                </p>
                            </div>

                            <p>
                                <a href="javascript:;" class="want_see " pid="${pm.popular}">想看</a>
                            </p>
                        </li>
                    </c:forEach>

                </ul>
            </div>
            <div class="upMovie hide" id="upMovie2">
                <ul class="posterStyle clearfix">
                    <c:forEach items="${commingList}" var="cm">
                        <li>
                            <i class="dot"></i>
                            <div class="playTime">${cm.showDate}上映</div>
                            <div class="showImg">
                                <a target="_blank" title="${cm.mvName}" href="/beijing/movie/48647.html" rel="nofollow"><img width="220" height="300" alt="${cm.mvName}" src="${path}/images/movieImg/${cm.cover.imgName}"></a>
                            </div>
                            <div class="title">
                                <span class="see_sum"><em class="wantSeeSum">${cm.popular}</em>人想看</span>
                                <p class="t1">
                                    <a target="_blank" title="${cm.mvName}" href="/beijing/movie/48647.html" rel="nofollow">${cm.mvName}</a>
                                </p>
                            </div>
                            <p>
                                <a href="javascript:;" class="want_see " pid="${cm.popular}">想看</a>
                            </p>
                        </li>
                    </c:forEach>

                </ul>
            </div>
        </div>
        <div class="mainRight">
            <h2 class="colTitle"><span class="colText">电影周排行榜</span></h2>
            <ul class="weeklyTop">
                <li><em class="score fr">7.5</em><b class="b0"></b>
                    <a href="/beijing/movie/48542.html" target="_blank" title="神偷奶爸3">神偷奶爸3</a>
                </li>
                <li><em class="score fr">7.0</em><b class="b1"></b>
                    <a href="/beijing/movie/48489.html" target="_blank" title="大护法">大护法</a>
                </li>
                <li><em class="score fr">7.2</em><b class="b2"></b>
                    <a href="/beijing/movie/48488.html" target="_blank" title="悟空传">悟空传</a>
                </li>
                <li><em class="score fr">8.0</em><b class="b3"></b>
                    <a href="/beijing/movie/48522.html" target="_blank" title="变形金刚5：最后的骑士">变形金刚5：最后...</a>
                </li>
                <li><em class="score fr">8.0</em><b class="b4"></b>
                    <a href="/beijing/movie/48484.html" target="_blank" title="京城81号Ⅱ">京城81号Ⅱ</a>
                </li>
            </ul>
        </div>
    </section>
    <section class="mainCont clearfix mt15">
        <div class="mainLeft" style="height:556px;">
            <h2 class="colTitle"><span class="colText">热门影院</span><a class="colMore" target="_blank" href="/beijing/cinema/category-ALL-area-0-type-0.html?keywords=" rel="nofollow">全部影院&gt;</a></h2>
            <ul class="hotCinema">
                <li>
                    <a href="morecinema.html" rel="nofollow" class="showBtn btnView" target="_blank">查看</a>
                    <span class="lowPrice">43.2<i>元起</i></span>
                    <div class="cName">
                        <a href="/cinema/1448.html" target="_blank">保利国际影城(首地大峡谷店)</a>
                        <em class="score ml20">8.3</em><em class="icon_z ml10">座</em><em class="icon_q ml10">券</em>
                    </div>
                    <div class="cAdd"><b></b>地址：北京市丰台区南三环首地大峡谷5层电话：010-87578551</div>
                </li>
                <li>
                    <a href="morecinema.html" rel="nofollow" class="showBtn btnView" target="_blank">查看</a>
                    <span class="lowPrice">43<i>元起</i></span>
                    <div class="cName">
                        <a href="/cinema/1570.html" target="_blank">北京搜秀影城</a>
                        <em class="score ml20">7.3</em><em class="icon_z ml10">座</em><em class="icon_q ml10">券</em>
                    </div>
                    <div class="cAdd"><b></b>地址：北京市崇文区崇外大街40号搜秀城9层电话：010-51671298</div>
                </li>
                <li>
                    <a href="morecinema.html" rel="nofollow" class="showBtn btnView" target="_blank">查看</a>
                    <span class="lowPrice">43<i>元起</i></span>
                    <div class="cName">
                        <a href="/cinema/4708.html" target="_blank">中影国际影城(北京千禧街店)</a>
                        <em class="score ml20">8.1</em><em class="icon_z ml10">座</em><em class="icon_q ml10">券</em>
                    </div>
                    <div class="cAdd"><b></b>地址：丰台区靛厂路千禧购物街4号楼F1-F3电话：010-88177970 010-88177579</div>
                </li>
                <li>
                    <a href="morecinema.html" rel="nofollow" class="showBtn btnView" target="_blank">查看</a>
                    <span class="lowPrice">32<i>元起</i></span>
                    <div class="cName">
                        <a href="/cinema/4186.html" target="_blank">17.5影城(今日家园店)</a>
                        <em class="score ml20">7.0</em><em class="icon_z ml10">座</em><em class="icon_q ml10">券</em>
                    </div>
                    <div class="cAdd"><b></b>地址：北京市海淀区西翠路5号今日家园8号楼F101室电话：010-88283459</div>
                </li>
                <li style="border-bottom:0">
                    <a href="morecinema.html" rel="nofollow" class="showBtn btnView" target="_blank">查看</a>
                    <span class="lowPrice">31<i>元起</i></span>
                    <div class="cName">
                        <a href="/cinema/1527.html" target="_blank">星博正华影城</a>
                        <em class="score ml20">6.5</em><em class="icon_z ml10">座</em><em class="icon_q ml10">券</em>
                    </div>
                    <div class="cAdd"><b></b>地址：北京市丰台区宋家庄顺八条4号政馨园三区5号楼底正华商城地下一层电话：010-87688666</div>
                </li>
            </ul>
        </div>
        <div class="mainRight">
            <h2 class="colTitle"><span class="colText">关注我们</span></h2>
            <div class="followMe">
                <a href="http://weibo.com" target="_blank" class="followTitle">官方微博</a>
                <span class="followBtn"><wb:follow-button uid="3084971975" type="red_1" width="67" height="24"></wb:follow-button></span>
            </div>
        </div>
    </section>
    <!--<section class="mainCont clearfix mt15">
        <dl class="partner">
            <dt>智能挑选价最低</dt>
            <dd>
                <img src="${path}/images/logo_1.jpg?v=1495696323" width="128" height="48" alt="">
                <img src="${path}/images/logo_2.jpg?v=1495696323" width="128" height="48" alt="">
                <img src="${path}/images/logo_3.jpg?v=1495696323" width="128" height="48" alt="">
                <img src="${path}/images/logo_4.jpg?v=1495696323" width="128" height="48" alt="">
                <img src="${path}/images/logo_5.jpg?v=1495696323" width="128" height="48" alt="">
                <img src="${path}/images/logo_6.jpg?v=1495696323" width="128" height="48" alt="">
            </dd>
        </dl>
    </section>-->
</section>
    
    <%@include file="../common/footer.jsp"%>
    
<div id="sideBar">
    <a id="feedback" href="" rel="nofollow" target="_blank" title="提意见" hidefocus="true"></a>
    <a id="toTop" href="javascript:;" rel="nofollow" title="回到顶部" hidefocus="true"></a>
</div>

<script src="${path}/js/imgScroll.js"></script>
<script src="${path}/js/movie/mvCommon.js"></script>
<script src="${path}/js/index/index2014.js"></script>

<script src="${path}/js/alogin.js"></script>
<script src="${path}/js/doRegister.js"></script>


<script>
    Core && Core.fastInit && Core.fastInit("1");
</script>
<script type="text/javascript">
    Core.statistics_adsage("ca");
</script>

<script src="${path}/js/ntes.js"></script>
<script>
    _ntes_nacc = "dianying";
    neteaseTracker();
</script>
<script>
    neteaseClickStat();
</script>
</body>

</html>
