package com.flw.controller.movie;

import com.flw.entity.*;
import com.flw.service.IMovieService;
import com.flw.service.IScheduleService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.*;

/**
 * 本类用于:
 *
 * @author Bruce
 * @create 2017-07-21 8:35
 **/

@Controller
@RequestMapping("/movie")
public class MovieController {

    /**
     * 获得movieService
     */
    @Autowired
    private IMovieService movieService;

    @Autowired
    private IScheduleService scheduleService;

    @RequestMapping("/list")
    public String homePage(Model model, String mvName){

        System.out.println("========进入Index控制器========");

        /**
         * 通过Service获取到所选推荐电影的分页的数据
         */
        PageBean<Movie> movieList = movieService.findByPage(mvName,null,null,1, null,1L,6L);

        /**
         * 获取最受期待电影分页数据
         */
        PageBean<Movie> popularList = movieService.findByPage(mvName,null,null,null,1,1L,4L);

        /**
         * 获取即将上映的电影分页数据
         */
        PageBean<Movie> commingList = movieService.findByPage(mvName,null,null,-1,null,1L,4L);

        //将电影的列表放到作用域中
        model.addAttribute("movieList", movieList.getList());

        //将最受欢迎电影列表放到作用域中
        model.addAttribute("popularList",popularList.getList());

        //将即将上映的电影列表放到作用域中
        model.addAttribute("commingList",commingList.getList());

        if(mvName == null){
            mvName = "";
        }

        model.addAttribute("mvName", mvName);

        return "index/home";
    }

    @RequestMapping("/detail")
    public String movieDetails(Model model, String mvId){
        System.out.println("=========进入电影详情后台=======");
        //将前台传过来的Id转换成Long类型
        Long mId = Long.valueOf(mvId);

        //调用Service方法来获取这个电影
        Movie movie = movieService.findById(mId);
        model.addAttribute("movie",movie);

        //根据电影来获取排期
        List<Schedule> scheduleList = scheduleService.findByConditions(movie,new Date(),null);
        model.addAttribute("schList",scheduleList);

        System.out.println("找到该电影的排期有："+scheduleList.size());

        int cinemeCount = 0;
        List<Cinema> cinemaList = new ArrayList<>();
        for (Schedule sch:scheduleList) {
            if(!cinemaList.contains(sch.getCinema())){
                cinemaList.add(sch.getCinema());
            }
        }
        //电影院列表
        model.addAttribute("cinemaList",cinemaList);

        //查找当前热映的电影
        PageBean<Movie> movieList = movieService.findByPage(null,null,null,5,-1,1L,6L);
        model.addAttribute("movieList",movieList.getList());

        if(scheduleList.size()<1){
            return "movie/detail";
        }else {
            return "movie/purchaseShow";
        }


    }

    @RequestMapping("/purchaseShow")
    public String purchase(){
        return "movie/purchaseShow";
    }

    @RequestMapping("/check")
    public String buy(Model model, String schId){
        Long sId = Long.valueOf(schId);
        Schedule schedule =  scheduleService.findById(sId);
        model.addAttribute("schedule",schedule);

        Set<Seat> seats = schedule.getMovieHall().getSeats();

        //计算座位的行数以及列数
        List<Integer> xs = new ArrayList<>();
        List<Integer> ys = new ArrayList<>();

        //座位映射
        Map<Integer,List<Seat>> maps = new TreeMap<>();

        for (Seat seat : seats){
            //遍历的到的座位信息，查看X的个数
            if(!xs.contains(seat.getSeatX())){
                xs.add(seat.getSeatX());
            }
            //遍历的到的座位信息，查看Y的个数
            if(!ys.contains(seat.getSeatY())){
                ys.add(seat.getSeatY());
            }

//            System.out.println("得到的Seat："+seat);
            //得到Map的键
            if(maps.containsKey(seat.getSeatX())){
                List<Seat> list = maps.get(seat.getSeatX());
                list.add(seat);
                Collections.sort(list);
                maps.put(seat.getSeatX(),list);
            }else {
                List<Seat> list = new ArrayList<>();
                list.add(seat);
                Collections.sort(list);
                maps.put(seat.getSeatX(),list);
            }

            //map中有这个x行
            /*if(list != null){
                list.add(seat);
                Collections.sort(list);
                maps.put(seat.getSeatX(),list);
            }else {
                //map中没有x行
                List<Seat> newList = new ArrayList<>();
                newList.add(seat);
                maps.put(seat.getSeatX(),list);
            }*/

        }
        //将行,列排序
        Collections.sort(xs);
        Collections.sort(ys);

        //将座位映射表存入作用域中
        model.addAttribute("maps",maps);
        System.out.println("找到的座位有："+maps.size());
       /* Set<Integer> set = maps.keySet();
        for (Integer i : set){

//            System.out.println(maps.get(i));
            System.out.println("第"+i+"排的座位如下：");
            for(Seat seat : maps.get(i)){
                System.out.println(seat);
            }
        }*/

        return "movie/check";
    }


}
