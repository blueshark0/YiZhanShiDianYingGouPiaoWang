package com.flw.controller.schedule;

import com.flw.entity.*;
import com.flw.service.IOrderService;
import com.flw.service.IScheduleService;
import com.flw.service.ISeatService;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 本类用于：
 *
 * @author Bruce
 * @date 2017/7/23 20:19
 */
@Controller
@RequestMapping("/order")
public class ScheduleController {

    @Autowired
    private IScheduleService scheduleService;

    @Autowired
    private ISeatService seatService;

    @Autowired
    private IOrderService orderService;

    @RequestMapping("/confirm")
    public String confirm(Model model, String schId, String sflwSeat, String phone, HttpSession session){

        System.out.println("==========进入支付后台==========");
        System.out.println("schid: "+schId);
        System.out.println("seatList: "+sflwSeat);
        System.out.println("mobile: "+phone);

        //先获得排期
        Long sId = Long.valueOf(schId);
        Schedule schedule = scheduleService.findById(sId);

        //得到这个排期的电影厅，它是唯一的
        MovieHall movieHall = schedule.getMovieHall();

        String[] ss = sflwSeat.split(":");

        Set<OrderItem> items = new HashSet<>();

        double allPrice = 0;

        //在修改座位之前需要得到座位对应的行和列
        //通过查找Seat，对Seat进行更新操作
        for (int i = 0; i< ss.length;i++){
            String[] s = ss[i].split("-");
            Integer x = Integer.valueOf(s[0]);
            Integer y = Integer.valueOf(s[1]);
            Seat seat = seatService.findByXY(movieHall,x,y);

            //实例化一个订单明细
            OrderItem item = new OrderItem();
            item.setPhone(phone);
            item.setSchedule(schedule);
            item.setPrice(schedule.getNewPrice());
            item.setSeat(seat);
            items.add(item);
            //System.out.println(items);
            allPrice += item.getPrice();
            //将座位变成不可选择
            seat.setSeatType(SeatType.不可选座位);
            seatService.update(seat);
        }


        //生成一个订单
        Order order = new Order();

        order.setCreateDate(new Date());

        for(OrderItem it : items){
            it.setOrder(order);
        }
        order.setItems(items);

        //格式化订单号
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
        String n1 = sdf1.format(new Date());

        String n2 = "";
        for (int i = 0; i<5;i++){
            String i2 = String.valueOf((int)(Math.random()*10));
            n2 += i2;
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("HHmmss");
        String n3 = sdf2.format(new Date());
        //拼接成一个 18位的唯一订单号
        String num = n1 + n2 + n3;

        System.out.println("订单编号："+num);
        order.setOrderNum(num);

        order.setUser((User) session.getAttribute("user"));

        order.setOrederStatus(OrderStatus.未付款);

        order.setAllPrice(allPrice);

        order.setPhone(phone);

        //orderService.save(order);
        //保存一个订单 -- 存到Session作用域中
/*        *//**
         * 其中Map:
         * Key-> OrderItem.Id
         * value-> Order.Id
         *//*
        Map<Long,Order> cart = new HashMap<>();*/

        //将Order存到作用域中
        session.setAttribute("userOrder",order);

        //将排期存到作用域中
        model.addAttribute("schedule",schedule);

        return "order/confirm";
    }

    @RequestMapping("buy")
    public String buy(HttpSession session){
        Order order = (Order) session.getAttribute("userOrder");
        if(order != null){
            order.setOrederStatus(OrderStatus.已付款);
            orderService.save(order);
        }
        return "order/list";
    }


    @RequestMapping("/list")
    public String list(Model model,HttpSession session){
        User user = (User) session.getAttribute("user");
        List<Order> userOrderList =  orderService.findByUser(user);
//        model.addAttribute("userOrderList",userOrderList);
        Order order = (Order) session.getAttribute("userOrder");
        List<Order> orders = new ArrayList<>();
        orders.add(order);
        for(Order od : userOrderList){
            orders.add(od);
        }
        model.addAttribute("orders",orders);
        return "order/list";
    }

}
