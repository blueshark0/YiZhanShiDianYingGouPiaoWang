/**
 * 
 */
package com.flw.entity;

/**
 * 本类是用来：表示订单状态枚举实体
 * 2017年6月26日 下午8:47:55
 */
public enum OrderStatus {
	已付款,未付款,交易成功;
}
