package com.flw.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 本类：地址实体类
 *
 * @date 2017/7/18 20:02
 */
@Entity
@Table(name="m_address")
public class Address implements Serializable{

    //地址ID
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "address_id")
    @SequenceGenerator(name = "address_id",sequenceName = "m_address_id",initialValue = 1,allocationSize = 1)
    private Long addressId;

    //省
    @Column
    private String provence;

    //市
    @Column
    private String city;

    //县,区
    @Column
    private String county;

    //详细地址-街道，路，门牌号
    @Column
    private String detailAdress;

    //邮编
    @Column
    private String emailCode;

    public Address() {
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public String getProvence() {
        return provence;
    }

    public void setProvence(String provence) {
        this.provence = provence;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getDetailAdress() {
        return detailAdress;
    }

    public void setDetailAdress(String detailAdress) {
        this.detailAdress = detailAdress;
    }

    public String getEmailCode() {
        return emailCode;
    }

    public void setEmailCode(String emailCode) {
        this.emailCode = emailCode;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Address{");
        sb.append("addressId=").append(addressId);
        sb.append(", provence='").append(provence).append('\'');
        sb.append(", city='").append(city).append('\'');
        sb.append(", county='").append(county).append('\'');
        sb.append(", detailAdress='").append(detailAdress).append('\'');
        sb.append(", emailCode='").append(emailCode).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
