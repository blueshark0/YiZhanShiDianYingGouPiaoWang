package com.flw.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by john on 2017/7/19.
 */
@Entity
@Table(name = "m_question")
public class Question implements Serializable{

    //问答Id
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "question_id")
    @SequenceGenerator(name = "question_id",sequenceName = "m_question_id",initialValue = 1,allocationSize = 1)
    private Long quesId;

    //问题
    @Column
    private String ques;

    //答案
    @Column
    private String ans;

    //问题的标题-也就是对应的帮助
    @ManyToOne(cascade = CascadeType.ALL,optional = false,fetch = FetchType.EAGER)
    @JoinColumn(name = "help_id")
    private Help help;

    public Question() {
    }

    public Long getQuesId() {
        return quesId;
    }

    public void setQuesId(Long quesId) {
        this.quesId = quesId;
    }

    public String getQues() {
        return ques;
    }

    public void setQues(String ques) {
        this.ques = ques;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }

    public Help getHelp() {
        return help;
    }

    public void setHelp(Help help) {
        this.help = help;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Qusetion{");
        sb.append("quesId=").append(quesId);
        sb.append(", ques='").append(ques).append('\'');
        sb.append(", ans='").append(ans).append('\'');
        sb.append(", help=").append(help);
        sb.append('}');
        return sb.toString();
    }
}
