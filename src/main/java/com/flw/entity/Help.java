package com.flw.entity;

import javax.persistence.*;
import java.io.Serializable;

import java.util.Set;

/**
 * 本类用于:网站的帮助文档
 *
 * @create 2017-07-19 8:10
 **/
@Entity
@Table(name = "m_help")
public class Help implements Serializable {

    //帮助文档ID
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "help_id")
    @SequenceGenerator(name = "help_id",sequenceName = "m_help_id",initialValue = 1,allocationSize = 1)

    private Long helpId;

    //帮助的主要问题分类-标题
    @Column
    private String helpType;

    //问题-答案列表
    @OneToMany(mappedBy = "help",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private Set<Question> questionList;

    public Help() {
    }

    public Long getHelpId() {
        return helpId;
    }

    public void setHelpId(Long helpId) {
        this.helpId = helpId;
    }

    public String getHelpType() {
        return helpType;
    }

    public void setHelpType(String helpType) {
        this.helpType = helpType;
    }

    public Set<Question> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(Set<Question> questionList) {
        this.questionList = questionList;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Help{");
        sb.append("helpId=").append(helpId);
        sb.append(", helpType='").append(helpType).append('\'');
        sb.append(", questionList=").append(questionList);
        sb.append('}');
        return sb.toString();
    }
}
