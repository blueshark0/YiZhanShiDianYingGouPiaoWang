package com.flw.entity;

import javax.persistence.*;
import java.io.Serializable;

import java.util.Set;

/**
 * 本类：电影院实体类
 *
 * @date 2017/7/18 19:59
 */
@Entity
@Table(name = "m_cinema")
public class Cinema  implements Serializable{

    //影院ID
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "cinemam_id")
    @SequenceGenerator(name = "cinemam_id",sequenceName = "m_cinema_id",initialValue = 1,allocationSize = 1)
    private Long id;

    //电影名
    @Column
    private String cmName;

    //电影院地址
    @OneToOne
    @JoinColumn(name="address_id")
    private Address cmAddress;

    //电影院类型
    @Enumerated(EnumType.STRING)
    private HallType cmType;

    //电影院影厅
    @OneToMany(mappedBy = "cinema",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private Set<MovieHall> movieHall;

    public Cinema() {
    }

    public Cinema(Long id, String cmName, Address cmAddress, HallType cmType, Set<MovieHall> movieHall) {
        this.id = id;
        this.cmName = cmName;
        this.cmAddress = cmAddress;
        this.cmType = cmType;
        this.movieHall = movieHall;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCmName() {
        return cmName;
    }

    public void setCmName(String cmName) {
        this.cmName = cmName;
    }

    public Address getCmAddress() {
        return cmAddress;
    }

    public void setCmAddress(Address cmAddress) {
        this.cmAddress = cmAddress;
    }

    public HallType getCmType() {
        return cmType;
    }

    public void setCmType(HallType cmType) {
        this.cmType = cmType;
    }

    public Set<MovieHall> getMovieHall() {
        return movieHall;
    }

    public void setMovieHall(Set<MovieHall> movieHall) {
        this.movieHall = movieHall;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Cinema{");
        sb.append("cmId=").append(id);
        sb.append(", cmName='").append(cmName).append('\'');
        sb.append(", cmAddress=").append(cmAddress);
        sb.append(", cmType=").append(cmType);
        sb.append(", movieHall=").append(movieHall);
        sb.append('}');
        return sb.toString();
    }
}
