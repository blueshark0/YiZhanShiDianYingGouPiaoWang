package com.flw.entity;

/**
 * Created by dgxz on 2017/7/18.
 */
public enum Frame {
    twoD,
    threeD,
    VR,
    AR
}
