package com.flw.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 本类用于：电影的封面
 *
 * @author Bruce
 * @date 2017/7/22 19:28
 */
@Entity
@Table(name = "m_cover")
public class Cover implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "cover_id")
    @SequenceGenerator(name = "cover_id",sequenceName = "m_cover_id",initialValue = 1,allocationSize = 1)
    private Long id;

    //封面的图片
    @Column
    private  String imgName;

    public Cover(){}

    public void setId(Long id) {
        this.id = id;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public Long getId() {
        return id;
    }

    public String getImgName() {
        return imgName;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Cover{");
        sb.append("id=").append(id);
        sb.append(", imgName='").append(imgName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
