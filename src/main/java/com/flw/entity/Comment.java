package com.flw.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 本类：
 *
 * @date 2017/7/18 19:47
 */
@Entity
@Table(name ="m_comment")
public class Comment implements Serializable{

    //影评ID
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "comment_id")
    @SequenceGenerator(name = "comment_id",sequenceName = "m_comment_id",initialValue = 1,allocationSize = 1)
    private Long id;

    //评论者昵称
    @Column
    private String userName;

    //评论时间
    @Temporal(TemporalType.DATE)
    private Date commentDate;

    //点赞
    @Column
    private Integer up;

    //差评
    @Column
    private Integer down;

    //评论的电影
    @ManyToOne
    @JoinColumn(name = "movie_id")
    private Movie movie;

    //评论的内容
    @Column
    private String info;

    public Comment() {
    }

    public Comment(Long id, String userName, Date commentDate, Integer up, Integer down) {
        this.id = id;
        this.userName = userName;
        this.commentDate = commentDate;
        this.up = up;
        this.down = down;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public Integer getUp() {
        return up;
    }

    public void setUp(Integer up) {
        this.up = up;
    }

    public Integer getDown() {
        return down;
    }

    public void setDown(Integer down) {
        this.down = down;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Comment{");
        sb.append("id=").append(id);
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", commentDate=").append(commentDate);
        sb.append(", up=").append(up);
        sb.append(", down=").append(down);
        sb.append(", movie=").append(movie);
        sb.append(", info='").append(info).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
