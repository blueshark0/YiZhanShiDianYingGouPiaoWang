package com.flw.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 本类用于:用户实体类
 *
 * @create 2017-07-18 17:03
 **/
@Entity
@Table(name = "m_user")
public class User implements Serializable{
    //用户的ID
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "user_id")
    @SequenceGenerator(name = "user_id",sequenceName = "m_user_id",initialValue = 1,allocationSize = 1)
    private Long id;

    //用户帐号
    @Column
    private String userNum;

    //用户密码
    @Column
    private String password;

    //昵称
    @Column
    private String userName;

    //手机号
    @Column
    private String phone;

    //邮箱
    @Column
    private String email;

    //注册时间
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date registerDate;

    public User() {
    }

    public User( String userNum, String password, String userName, String phone, String email, Date registerDate) {
        this.userNum = userNum;
        this.password = password;
        this.userName = userName;
        this.phone = phone;
        this.email = email;
        this.registerDate = registerDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserNum() {
        return userNum;
    }

    public void setUserNum(String userNum) {
        this.userNum = userNum;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("id=").append(id);
        sb.append(", userNum='").append(userNum).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", phone=").append(phone);
        sb.append(", email='").append(email).append('\'');
        sb.append(", registerDate=").append(registerDate);
        sb.append('}');
        return sb.toString();
    }
}
