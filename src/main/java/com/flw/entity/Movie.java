package com.flw.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import java.util.HashSet;
import java.util.Set;

/**
 * 本类用于:电影实体类
 *
 * @create 2017-07-18 17:39
 **/
@Entity
@Table(name = "m_movie")
public class Movie implements Serializable{

    //电影ID
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "movie_id")
    @SequenceGenerator(name = "movie_id",sequenceName = "m_movie_id",initialValue = 1,allocationSize = 1)
    private Long id;

    //电影名
    @Column
    private String mvName;

    //导演
    @Column
    private String director;

    //主演
    @OneToMany(mappedBy = "movie",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private Set<MainAct> mainAct = new HashSet<>();

    //电影一句话解释
    @Column
    private String explain;

    //电影画面-2D,3D
    @Enumerated(EnumType.STRING)
    private Frame frame;

    //电影片长--用秒来表示
    @Column
    private Integer mvTime;

    //电影地区-国产，欧美，日韩
    @Column
    @Enumerated(EnumType.STRING)
    private Area area;

    //电影类型 - 喜剧，历史，动画
    @Column
    @Enumerated(EnumType.STRING)
    private Type type;

    //剧情简介
    @Column
    private String info;

    //电影影评(可以解决懒加载异常)
    @OneToMany(mappedBy = "movie",fetch = FetchType.EAGER)
    private Set<Comment> comment = new HashSet<>();

    //电影剧照(可以解决懒加载异常)
    @OneToMany(mappedBy = "movie",fetch = FetchType.EAGER)
    private Set<Image> images = new HashSet<>();

    //电影评分
    @Column
    private Double score;

    //电影人气
    @Column
    private Integer popular;

    //上映时间
    @Column
    @Temporal(TemporalType.DATE)
    private Date showDate;

    //电影封面照片
    @OneToOne
    @JoinColumn(name="cover_id")
    private Cover cover;

//   //电影是否是热映，是否是受期待，是否是即将上映
//   @Column
//   @Enumerated(EnumType.STRING)
//   private ShowType showType;

    public Movie() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMvName() {
        return mvName;
    }

    public void setMvName(String mvName) {
        this.mvName = mvName;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public Set<MainAct> getMainAct() {
        return mainAct;
    }

    public void setMainAct(Set<MainAct> mainAct) {
        this.mainAct = mainAct;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public Frame getFrame() {
        return frame;
    }

    public void setFrame(Frame frame) {
        this.frame = frame;
    }

    public Integer getMvTime() {
        return mvTime;
    }

    public void setMvTime(Integer mvTime) {
        this.mvTime = mvTime;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Set<Comment> getComment() {
        return comment;
    }

    public void setComment(Set<Comment> comment) {
        this.comment = comment;
    }

    public Set<Image> getImages() {
        return images;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public void setPopular(Integer popular) {
        this.popular = popular;
    }

    public Integer getPopular() {
        return popular;
    }

    public void setShowDate(Date showDate) {
        this.showDate = showDate;
    }

    public Date getShowDate() {
        return showDate;
    }

    public void setCover(Cover cover) {
        this.cover = cover;
    }

    public Cover getCover() {
        return cover;
    }

    /*    public void setShowType(ShowType showType) {
        this.showType = showType;
    }

    public ShowType getShowType() {
        return showType;
    }*/

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Movie{");
        sb.append("id=").append(id);
        sb.append(", mvName='").append(mvName).append('\'');
        sb.append(", director='").append(director).append('\'');
        sb.append(", explain='").append(explain).append('\'');
        sb.append(", frame=").append(frame);
        sb.append(", mvTime=").append(mvTime);
        sb.append(", area=").append(area);
        sb.append(", type=").append(type);
        sb.append(", info='").append(info).append('\'');
        sb.append(", score=").append(score);
        sb.append(", popular=").append(popular);
        sb.append(", showDate=").append(showDate);
        sb.append(", cover=").append(cover);
        sb.append('}');
        return sb.toString();
    }
}
