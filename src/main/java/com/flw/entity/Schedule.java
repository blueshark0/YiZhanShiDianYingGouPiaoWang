package com.flw.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import java.util.Set;

/**
 * 本类用于：电影院电影排期实体类
 *
 * @date 2017/7/18 21:07
 */
@Entity
@Table(name="m_schedule")
public class Schedule implements Serializable {

    //排期的ID
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "schedule_id")
    @SequenceGenerator(name = "schedule_id",sequenceName = "m_schedule_id",initialValue = 1,allocationSize = 1)
    private Long schId;

    //电影院
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cinema_id")
    private Cinema cinema;

    //排期的日期 --例如 2017-07-21 2017-07-28  ...
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date schDate;

//    //排期的时间点 --例如 10:35  11:45  12:15  12:50  ...
//    @Column
//    @Temporal(TemporalType.TIME)
//    private Date schTime;

    //要播放的电影
    @ManyToOne(fetch = FetchType.EAGER)
    private Movie movie;

    //排期的电影影厅
    @OneToOne
    @JoinColumn(name = "movieHall_id")
    private MovieHall movieHall;

    //排期的电影原价
    @Column
    private Double oldPrice;

    //排期电影优惠后价格
    @Column
    private double newPrice;

    public Schedule() {
    }

    public Long getSchId() {
        return schId;
    }

    public void setSchId(Long schId) {
        this.schId = schId;
    }

    public Cinema getCinema() {
        return cinema;
    }

    public void setCinema(Cinema cinema) {
        this.cinema = cinema;
    }

    public Date getSchDate() {
        return schDate;
    }

    public void setSchDate(Date schDate) {
        this.schDate = schDate;
    }



    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public void setMovieHall(MovieHall movieHall) {
        this.movieHall = movieHall;
    }

    public MovieHall getMovieHall() {
        return movieHall;
    }

    public Double getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(Double oldPrice) {
        this.oldPrice = oldPrice;
    }

    public double getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(double newPrice) {
        this.newPrice = newPrice;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Schedule{");
        sb.append("schId=").append(schId);
        sb.append(", schDate=").append(schDate);
        sb.append(", oldPrice=").append(oldPrice);
        sb.append(", newPrice=").append(newPrice);
        sb.append('}');
        return sb.toString();
    }
}
