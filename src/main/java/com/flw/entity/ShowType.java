package com.flw.entity;

/**
 * 本枚举用于：电影的上映类型：
 *
 * @author Bruce
 * @date 2017/7/21 22:43
 */
public enum ShowType {
    热映,
    热映推荐,
    即将上映,
    最受期待
}
