package com.flw.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 本类用于:管理员实体类
 *
 * @create 2017-07-18 17:30
 **/
@Entity
@Table(name = "m_admin")
public class Admin implements Serializable{

    //管理员ID
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "admin_id")
    @SequenceGenerator(name = "admin_id",sequenceName = "m_admin_id",initialValue = 1,allocationSize = 1)
    private Long id;

    //管理员帐号
    @Column
    private String admNum;

    //管理员密码
    @Column
    private String password;

    //管理员姓名
    @Column
    private String admName;

    //管理员角色-用于分别是哪种管理员
    @ManyToOne(cascade = CascadeType.ALL,optional = false,fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    private Role role;

    public Admin() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdmNum() {
        return admNum;
    }

    public void setAdmNum(String admNum) {
        this.admNum = admNum;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAdmName() {
        return admName;
    }

    public void setAdmName(String admName) {
        this.admName = admName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Admin{");
        sb.append("id=").append(id);
        sb.append(", admNum='").append(admNum).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", admName='").append(admName).append('\'');
        sb.append(", role=").append(role);
        sb.append('}');
        return sb.toString();
    }
}
