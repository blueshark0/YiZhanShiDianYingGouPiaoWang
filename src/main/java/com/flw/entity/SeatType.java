package com.flw.entity;

/**
 * 本枚举用于：座位类型实体类
 *
 * @date 2017/7/18 20:49
 */
public enum SeatType {
    可选座位,
    不可选座位
}
