package com.flw.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by john on 2017/7/19.
 */
@Entity
@Table(name="m_role")
public class Role implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "role_id")
    @SequenceGenerator(name = "role_id",sequenceName = "m_role_id",initialValue = 1,allocationSize = 1)
    private Long roleId;

    @Column
    private String roleName;
}
