package com.flw.entity;

import jdk.nashorn.internal.ir.annotations.Reference;
import org.springframework.context.annotation.Primary;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * 本类用于：用户购买电影票的订单实体类
 *
 * @date 2017/7/18 20:54
 */
@Entity
@Table(name = "m_order")
public class Order implements Serializable {

    //订单ID
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "order_id")
    @SequenceGenerator(name = "order_id",sequenceName = "m_order_id",initialValue = 1,allocationSize = 1)
    private Long orderId;

    //订单编号
    @Column
    private String orderNum;

    //订单生成时间
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    //订单的明细
    @OneToMany(mappedBy = "order",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Set<OrderItem> items = new HashSet<>();

    //该订单的用户
    @ManyToOne
    private User user;

    //总价
    @Column
    private double allPrice;

    //支付状态
    @Column
    @Enumerated(EnumType.STRING)
    private OrderStatus orederStatus;

    //手机号
    @Column
    private String phone;

    public Order() {
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Set<OrderItem> getItems() {
        return items;
    }

    public void setItems(Set<OrderItem> items) {
        this.items = items;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public OrderStatus getOrederStatus() {
        return orederStatus;
    }

    public void setOrederStatus(OrderStatus orederStatus) {
        this.orederStatus = orederStatus;
    }

    public void setAllPrice(double allPrice) {
        this.allPrice = allPrice;
    }

    public double getAllPrice() {
        return allPrice;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Order{");
        sb.append("orderId=").append(orderId);
        sb.append(", orderNum='").append(orderNum).append('\'');
        sb.append(", createDate=").append(createDate);
//        sb.append(", items=").append(items);
        sb.append(", user=").append(user);
        sb.append(", orederStatus=").append(orederStatus);
        sb.append(", allPrice=").append(allPrice);
        sb.append(", phone=").append(phone);
        sb.append('}');
        return sb.toString();
    }
}
