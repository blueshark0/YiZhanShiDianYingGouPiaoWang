package com.flw.entity;

import org.springframework.web.bind.annotation.Mapping;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 本类用于：用户购买电影票的订单详细实体类
 *
 * @date 2017/7/18 20:58
 */
@Entity
@Table(name = "m_item")
public class OrderItem implements Serializable{

    //明细ID
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "item_id")
    @SequenceGenerator(name = "item_id",sequenceName = "m_item_id",initialValue = 1,allocationSize = 1)
    private Long itemId;

    //电影场次排期
    @OneToOne
    private Schedule schedule;

    //电影票价格
    @Column
    private double price;

    //接受电子票的手机号码
    @Column
    private String phone;

    //座位
    @OneToOne
    @JoinColumn(name = "seat_id")
    private Seat seat;

    //对应的订单
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="order_id")
    private Order order;

    public OrderItem() {
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }

    public Seat getSeat() {
        return seat;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("OrderItem{");
        sb.append("itemId=").append(itemId);
        sb.append(", price=").append(price);
        sb.append(", cinemaName=").append(schedule.getCinema().getCmName());
        sb.append(", movieHallName=").append(schedule.getMovieHall().getHallName());
        sb.append(", phone=").append(phone);
        sb.append(", order=").append(order);
        sb.append('}');
        return sb.toString();
    }
}
