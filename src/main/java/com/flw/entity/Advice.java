package com.flw.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 本类用于：用户对网站的评价
 *
 * @create 2017-07-19 8:00
 **/
@Entity
@Table(name = "m_advice")
public class Advice implements Serializable{

    //评价ID
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "advice_id")
    @SequenceGenerator(name = "advice_id",sequenceName = "m_advice_id",initialValue = 1,allocationSize = 1)

    private Long adId;

    //评价内容
    @Column
    private String content;

    //评价人邮箱
    @Column
    private String email;

    //评价人手机号
    @Column
    private Long phone;

    public Advice() {
    }

    public Advice(Long adId, String content, String email, Long phone) {
        this.adId = adId;
        this.content = content;
        this.email = email;
        this.phone = phone;
    }

    public Long getAdId() {
        return adId;
    }

    public void setAdId(Long adId) {
        this.adId = adId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Advice{");
        sb.append("adId=").append(adId);
        sb.append(", content='").append(content).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", phone=").append(phone);
        sb.append('}');
        return sb.toString();
    }
}
