package com.flw.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by john on 2017/7/19.
 */
@Entity
@Table(name="m_mainact")
public class MainAct implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "maid")
    @SequenceGenerator(name = "maid",sequenceName = "m_maid",initialValue = 1,allocationSize = 1)
    private  Long id;

    //主演名字
    @Column
    private  String name;

    //主演的电影
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "movie_id")
    private Movie movie;

    public MainAct() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Movie getMovie() {
        return movie;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("MainAct{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", movie=").append(movie);
        sb.append('}');
        return sb.toString();
    }
}
