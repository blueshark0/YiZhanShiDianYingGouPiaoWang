package com.flw.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 本类用于：影院座位实体
 *
 * @date 2017/7/18 20:44
 */
@Entity
@Table(name ="m_seat")
public class Seat implements Serializable,Comparable<Seat>{

    //座位ID
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "seat_id")
    @SequenceGenerator(name = "seat_id",sequenceName = "m_seat_id",initialValue = 1,allocationSize = 1)

    private Long seatId;

    //座位的X坐标
    @Column
    private  Integer seatX;

    //座位的Y坐标
    @Column
    private Integer seatY;

    //座位类型
    @Column
    @Enumerated(EnumType.STRING)
    private SeatType seatType;

    //座位对应的影厅   一对多的关系
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "movieHall_id")
    private MovieHall movieHall;

    public Seat() {
    }

    public Seat(Long seatId, Integer seatX, Integer seatY, SeatType seatType, MovieHall movieHall) {
        this.seatId = seatId;
        this.seatX = seatX;
        this.seatY = seatY;
        this.seatType = seatType;
        this.movieHall = movieHall;
    }

    public Long getSeatId() {
        return seatId;
    }

    public void setSeatId(Long seatId) {
        this.seatId = seatId;
    }

    public Integer getSeatX() {
        return seatX;
    }

    public void setSeatX(Integer seatX) {
        this.seatX = seatX;
    }

    public Integer getSeatY() {
        return seatY;
    }

    public void setSeatY(Integer seatY) {
        this.seatY = seatY;
    }

    public SeatType getSeatType() {
        return seatType;
    }

    public void setSeatType(SeatType seatType) {
        this.seatType = seatType;
    }

    public MovieHall getMovieHall() {
        return movieHall;
    }

    public void setMovieHall(MovieHall movieHall) {
        this.movieHall = movieHall;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Seat{");
        sb.append("seatId=").append(seatId);
        sb.append(", seatX=").append(seatX);
        sb.append(", seatY=").append(seatY);
        sb.append(", seatType=").append(seatType);
        sb.append(", movieHall=").append(movieHall);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int compareTo(Seat s1) {
        return s1.seatY-this.seatY;
    }
}
