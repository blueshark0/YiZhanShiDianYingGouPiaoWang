package com.flw.entity;

/**
 * 本枚举用于:电影类型
 * @create 2017-07-18 17:56
 **/

public enum Type {
    历史,
    动画,
    喜剧,
    冒险,
    幻想,
    悬念,
    惊悚,
    记录,
    战争,
    西部,
    爱情,
    剧情,
    恐怖,
    动作,
    科幻,
    音乐,
    家庭,
    犯罪,
    其他
}
