package com.flw.entity;

import javax.persistence.*;
import java.io.Serializable;

import java.util.Set;

/**
 * 本类：影厅实体类
 *
 * @date 2017/7/18 20:31
 */
@Entity
@Table(name = "m_movieHall")
public class MovieHall implements Serializable{

    //影厅ID
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "moviehall_id")
    @SequenceGenerator(name = "moviehall_id",sequenceName = "m_moviehall_id",initialValue = 1,allocationSize = 1)
    private Long id;

    @Column
    private String hallName;

    //影厅类型
    @Column
    @Enumerated(EnumType.STRING)
    private HallType hallType;

    //影厅座位
    @OneToMany(mappedBy = "movieHall",fetch = FetchType.EAGER)
    private Set<Seat> seats;

    //影厅对应的影院
    @ManyToOne
    @JoinColumn(name = "cinema_id")
    private Cinema cinema;

    public MovieHall() {
    }

    public MovieHall(Long mhId,String hallName, HallType hallType, Set<Seat> seats) {
        this.id = mhId;
        this.hallType = hallType;
        this.seats = seats;
        this.hallName=hallName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long mhId) {
        this.id = mhId;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public String getHallName() {
        return hallName;
    }

    public HallType getHallType() {
        return hallType;
    }

    public void setHallType(HallType hallType) {
        this.hallType = hallType;
    }

    public Set<Seat> getSeats() {
        return seats;
    }

    public void setSeats(Set<Seat> seats) {
        this.seats = seats;
    }

    public void setCinema(Cinema cinema) {
        this.cinema = cinema;
    }

    public Cinema getCinema() {
        return cinema;
    }



    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("MovieHall{");
        sb.append("mhId=").append(id);
        sb.append(", hallType=").append(hallType);
 //       sb.append(", seats=").append(seats);
//        sb.append(", cinema=").append(cinema);
        sb.append('}');
        return sb.toString();
    }
}
