package com.flw.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 本类：
 *
 * @date 2017/7/18 19:44
 */
@Entity
@Table(name = "m_image")
public class Image implements Serializable{

    //图片ID
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "image_id")
    @SequenceGenerator(name = "image_id",sequenceName = "m_image_id",initialValue = 1,allocationSize = 1)
    private Long id;

    //图片名称
    @Column
    private String imgName;

    //图片对应的电影
    @ManyToOne(cascade={CascadeType.REFRESH, CascadeType.MERGE}, optional=false, fetch=FetchType.LAZY)
    @JoinColumn(name = "movie_id")
    private Movie movie;

    public Image() {
    }

    public Image(Long id, String imgName, Movie movie) {
        this.id = id;
        this.imgName = imgName;
        this.movie = movie;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Image{");
        sb.append("id=").append(id);
        sb.append(", imgName='").append(imgName).append('\'');
        sb.append(", movie=").append(movie);
        sb.append('}');
        return sb.toString();
    }
}
