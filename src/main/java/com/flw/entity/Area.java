package com.flw.entity;

/**
 * Created by Administrator on 2017/7/18.
 */
public enum Area {
    中国,
    香港,
    大陆,
    美国,
    韩国,
    日本,
    台湾,
    法国,
    英国,
    泰国,
    印度,
    其他
}
