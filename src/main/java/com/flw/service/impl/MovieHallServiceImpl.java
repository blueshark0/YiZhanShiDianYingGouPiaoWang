package com.flw.service.impl;

import com.flw.dao.IMovieHallDao;
import com.flw.entity.Cinema;
import com.flw.entity.MovieHall;
import com.flw.service.IMovieHallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 本类用于:
 *
 * @author Bruce
 * @version v1.0
 * @create 2017-07-24 13:25
 **/

@SuppressWarnings("all")
@Service
@Transactional
public class MovieHallServiceImpl implements IMovieHallService{

    @Autowired
    private IMovieHallDao movieHallDao;

    @Override
    public void save(MovieHall movieHall) {
        movieHallDao.save(movieHall);
    }

    @Override
    public void delete(Long mhId) {
        movieHallDao.delete(mhId);
    }

    @Override
    public void update(MovieHall movieHall) {
        movieHallDao.update(movieHall);
    }

    @Override
    public MovieHall findById(Long mhId) {
        return movieHallDao.findById(mhId);
    }

    @Override
    public List<MovieHall> findByCinema(Cinema cinema) {
        return movieHallDao.findByCinema(cinema);
    }
}
