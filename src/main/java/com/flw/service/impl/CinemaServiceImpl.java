package com.flw.service.impl;

import com.flw.dao.ICinemaDao;
import com.flw.entity.Address;
import com.flw.entity.Cinema;
import com.flw.entity.PageBean;
import com.flw.service.ICinemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 本类用于:实现影院Service接口
 *
 * @author Bruce
 * @create 2017-07-21 13:30
 **/

@SuppressWarnings("all")
@Service
@Transactional
public class CinemaServiceImpl implements ICinemaService {

    @Autowired
    private ICinemaDao cinemaDao;

    @Override
    public void save(Cinema cinema) {
        cinemaDao.save(cinema);
    }

    @Override
    public void delete(Cinema cinema) {
        cinemaDao.delete(cinema);
    }

    @Override
    public void update(Cinema cinema) {
        cinemaDao.update(cinema);
    }

    @Override
    public Cinema findById(Long id) {
        return cinemaDao.findById(id);
    }

    @Override
    public List<Cinema> findByAddress(Address address) {
        return cinemaDao.findByAddress(address);
    }

    @Override
    public Long getRowCount(String cmName) {
        return cinemaDao.getRowCount(cmName);
    }

    @Override
    public PageBean<Cinema> findByConditions(String cmName, Long pageNow, Long pageSize) {
        //实例化pageBean
        PageBean<Cinema> pageBean = new PageBean<>();

        //先得到这个集合
        List<Cinema> list = cinemaDao.findByConditions(cmName,pageNow,pageSize);

        pageBean.setList(list);


        Long count = getRowCount(cmName) / pageSize;

        /**
         * 如果搜索到的条数不能被页整除，也就是多了几条，那么就将页加1
         */
        if( getRowCount(cmName) % pageSize != 0){
            count++;
        }
        pageBean.setPageCount(count);

        pageBean.setPageNow(pageNow);

        pageBean.setPageSize(pageSize);

        return pageBean;
    }
}
