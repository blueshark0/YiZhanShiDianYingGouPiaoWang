package com.flw.service.impl;

import com.flw.dao.IUserDao;
import com.flw.entity.User;
import com.flw.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 本类用于:
 *
 * @author Bruce
 * @version v1.0
 * @create 2017-07-24 10:10
 **/
@SuppressWarnings("all")
@Service
@Transactional
public class UserServiceImpl implements IUserService {

    @Autowired
    private IUserDao userDao;

    @Override
    public void save(User user) {
        userDao.save(user);
    }

    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }

    @Override
    public User findByUserNum(String userNum) {
        return userDao.findByUserNum(userNum);
    }

    @Override
    public User update(User user) {
        return userDao.update(user);
    }

    @Override
    public void delById(Long id) {
        userDao.delById(id);
    }

    @Override
    public User findByPhone(String phone) {
        return userDao.findByPhone(phone);
    }
}
