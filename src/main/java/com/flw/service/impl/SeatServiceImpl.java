package com.flw.service.impl;

import com.flw.dao.ISeatDao;
import com.flw.entity.MovieHall;
import com.flw.entity.Seat;
import com.flw.service.ISeatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * 本类用于:
 *
 * @author Bruce
 * @version v1.0
 * @create 2017-07-27 10:27
 **/

@SuppressWarnings("all")
@Service
@Transactional
public class SeatServiceImpl  implements ISeatService{

    @Autowired
    private ISeatDao seatDao;

    @Override
    public void save(Seat seat) {
        seatDao.save(seat);
    }

    @Override
    public void delete(Long Id) {
        seatDao.delete(Id);

    }

    @Override
    public void update(Seat seat) {
        seatDao.update(seat);

    }

    @Override
    public Seat findById(Long id) {
        return seatDao.findById(id);
    }

    @Override
    public Seat findByXY(MovieHall movieHall, Integer x, Integer y) {
        return seatDao.findByXY(movieHall, x, y);
    }

}
