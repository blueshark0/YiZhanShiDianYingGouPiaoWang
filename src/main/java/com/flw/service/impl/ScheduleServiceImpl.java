package com.flw.service.impl;

import com.flw.dao.IScheduleDao;
import com.flw.entity.Cinema;
import com.flw.entity.Movie;
import com.flw.entity.Schedule;
import com.flw.service.IScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * 本类用于：
 *
 * @author Bruce
 * @date 2017/7/23 21:35
 */
@SuppressWarnings("all")
@Service
@Transactional
public class ScheduleServiceImpl implements IScheduleService {

    @Autowired
    private IScheduleDao scheduleDao;

    @Override
    public void save(Schedule schedule) {
        scheduleDao.save(schedule);
    }

    @Override
    public void delete(Long schId) {
        scheduleDao.delete(schId);
    }

    @Override
    public void update(Schedule schedule) {
        scheduleDao.update(schedule);
    }

    @Override
    public Schedule findById(Long schId) {
        return scheduleDao.findById(schId);
    }

    @Override
    public List<Schedule> findByConditions(Movie movie, Date schDate, Cinema cinema) {
        return scheduleDao.findByConditions(movie,schDate,cinema);
    }

}
