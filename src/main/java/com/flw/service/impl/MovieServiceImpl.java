package com.flw.service.impl;

import com.flw.dao.IMovieDao;
import com.flw.entity.Area;
import com.flw.entity.Movie;
import com.flw.entity.PageBean;
import com.flw.entity.Type;
import com.flw.service.IMovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 本类用于:
 *
 * @author Bruce
 * @create 2017-07-21 8:44
 **/
@Service
@Transactional
public class MovieServiceImpl implements IMovieService {

    @Autowired
    private IMovieDao movieDao;

    @Override
    public void save(Movie movie) {
        movieDao.save(movie);
    }

    @Override
    public void delete(Movie movie) {
        movieDao.delete(movie);
    }

    @Override
    public void update(Movie movie) {
        movieDao.update(movie);
    }

    @Override
    public List<Movie> findAll(String mvName) {
        return movieDao.findByName(mvName);
    }

    @Override
    public Movie findById(Long id) {
        return movieDao.findById(id);
    }

    @Override
    public Movie findBySchid(Long schid) {
        return movieDao.findBySchid(schid);
    }

    @Override
    public List<Movie> findByMainAct(String name) {
        return movieDao.findByMainAct(name);
    }

    @Override
    public PageBean<Movie> findByPage(String movieName, Type type, Area area, Integer show,
                                      Integer popular, Long pageNow, Long pageSize) {

        //实例化这个分页对象
        PageBean<Movie> pageBean = new PageBean<>();

        //获取所有的电影信息
        List<Movie> bookList = movieDao.findByConditions(movieName, type, area, show, popular, pageNow, pageSize);

        pageBean.setList(bookList);

        //获得总条数
        Long rowCount = movieDao.getRowCount(movieName,type,area,show,popular);

        pageBean.setPageCount(rowCount);

        //获得总页数
        Long pageCount = rowCount / pageSize;

        if(rowCount % pageSize != 0){
            pageCount++;
        }

        pageBean.setPageCount(pageCount);

        return pageBean;
    }
}
