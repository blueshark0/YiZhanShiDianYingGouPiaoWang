package com.flw.service.impl;

import com.flw.dao.IOrderDao;
import com.flw.entity.Order;
import com.flw.entity.OrderItem;
import com.flw.entity.Schedule;
import com.flw.entity.User;
import com.flw.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 本类用于:
 *
 * @author Bruce
 * @version v1.0
 * @create 2017-07-27 13:18
 **/
@SuppressWarnings("all")
@Service
@Transactional
public class OrderServiceImpl implements IOrderService {

    @Autowired
    private IOrderDao orderDao;

    @Override
    public void save(Order order) {
        orderDao.save(order);
    }

    @Override
    public void delete(Schedule schedule) {
        orderDao.delete(schedule);
    }

    @Override
    public OrderItem find(Long schid) {
        return orderDao.find(schid);
    }

    @Override
    public Map<Long, OrderItem> getItems() {
        return orderDao.getItems();
    }

    @Override
    public void update(Order order) {
        orderDao.update(order);
    }

    @Override
    public List<Order> findByUser(User user) {
        return orderDao.findByUser(user);
    }
}
