package com.flw.service;

import com.flw.entity.Cinema;
import com.flw.entity.MovieHall;

import java.util.List;

/**
 * 本类用于:
 *
 * @author Bruce
 * @version v1.0
 * @create 2017-07-24 13:25
 **/

public interface IMovieHallService {

    void save(MovieHall movieHall);

    void  delete(Long mhId);

    void update(MovieHall movieHall);

    MovieHall findById(Long mhId);

    List<MovieHall> findByCinema(Cinema cinema);
}
