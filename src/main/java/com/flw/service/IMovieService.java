package com.flw.service;

import com.flw.entity.Area;
import com.flw.entity.Movie;
import com.flw.entity.PageBean;
import com.flw.entity.Type;

import java.util.List;

/**
 * 本接口用于:
 *
 * @author Bruce
 * @create 2017-07-21 8:42
 **/

public interface IMovieService {

    /**
     * 电影管理员新增电影
     */
    public void save(Movie movie);

    /**
     * 管理员删除电影
     */
    public  void delete(Movie movie);

    /**
     * 管理员更改电影
     */
    public void update(Movie movie);


    /**
     * 管理员和用户
     * 电影名称 模糊查询 所有电影
     */
    public List<Movie> findAll(String mvName);

    /**
     * 管理员和用户
     * 根据电影的id来查询电影
     */
    public Movie findById(Long id);

    /**
     * 管理员和用户
     * 根据电影的排期的id来查询电影
     */
    public Movie findBySchid(Long schid);


    /**
     * 用户
     * 根据电影主演名称
     * 模糊查询所有的电影
     */
    public List<Movie> findByMainAct(String name);

    /**
     * 通过条件分页
     * @param movieName 电影名称
     * @param type 电影类型
     * @param area 电影地区
     * @param show 是否上映
     * @param popular 想看的人数
     * @param pageNow 当前页
     * @param pageSize 页面显示条数
     * @return
     */
    public PageBean<Movie> findByPage(String movieName, Type type, Area area, Integer show, Integer popular, Long pageNow, Long pageSize);

}
