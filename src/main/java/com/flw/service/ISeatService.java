package com.flw.service;

import com.flw.entity.MovieHall;
import com.flw.entity.Seat;

/**
 * 本接口用于:
 *
 * @author Bruce
 * @version v1.0
 * @create 2017-07-27 10:26
 **/

public interface ISeatService {

    void save(Seat seat);

    void delete(Long Id);

    void update(Seat seat);

    Seat findById(Long id);

    Seat findByXY(MovieHall movieHall,Integer x, Integer y);
}
