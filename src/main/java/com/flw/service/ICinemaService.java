package com.flw.service;

import com.flw.entity.Address;
import com.flw.entity.Cinema;
import com.flw.entity.PageBean;

import java.util.List;

/**
 * 本类用于:实现影院接口
 *
 * @author Bruce
 * @create 2017-07-21 13:29
 **/

public interface ICinemaService {
    /**
     * 管理员
     * 新增一个电影院
     */
    void save(Cinema cinema);

    /**
     * 管理员删除一个电影院
     */
    void delete(Cinema cinema);

    /**
     * 管理员修改一个电影院
     */
    void  update(Cinema cinema);

    /**
     * 通过影院的Id来查找影院
     * @param id
     * @return
     */
    Cinema findById(Long id);


    /**
     *  用户
     * 根据地址查找所有影院
     * 如果address为null就是查找所有影院
     */
    List<Cinema> findByAddress(Address address);


    /**
     * 查询出所有电影院的数量
     * @param cmName 电影院名称
     * @return
     */
    Long getRowCount(String cmName);

    /**
     * 通过电影院名称来查询电影院并分页
     * @param cmName 电影院名称
     * @param pageNow 当前页
     * @param pageSize 每页显示多少个
     * @return
     */
    PageBean<Cinema> findByConditions(String cmName, Long pageNow, Long pageSize);
}
