package com.flw.dao;

import com.flw.entity.Order;
import com.flw.entity.OrderItem;
import com.flw.entity.Schedule;
import com.flw.entity.User;
import org.hibernate.sql.Update;

import java.util.List;
import java.util.Map;

/**
 * Created by JC on 2017/7/20.
 * 购票用户中
 * 订单的接口
 */
public interface IOrderDao {

    /**
     * 保存一个订单
     * @param order
     */
    void save(Order order);

    /**
     * 根据Long类型的schid来先查询到schedule
     * 根据排期Schedule来删除订单详情OrderItem(因为排期和订单详情是一对一)
     * @param schedule
     */
    void delete(Schedule schedule);

    /**
     * 根据 排期Schedule的Long类型schid来
     * 获取订单详情
     * @param schid
     * @return
     */
    OrderItem find(Long schid);

    /**
     * 获取订单map集合
     * Long类型的key是排期的id 也就是schid
     * @return
     */
    public Map<Long,OrderItem> getItems();

    /**
     * 更新Order
     * @param order
     */
    void update(Order order);

    /**
     * 通过用户查找Order
     * @param user
     * @return
     */
    List<Order> findByUser(User user);


}
