package com.flw.dao;

import com.flw.entity.Area;
import com.flw.entity.Movie;
import com.flw.entity.Type;

import java.util.Date;
import java.util.List;

/**
 *
 * 电影接口
 * Created by JC on 2017/7/20.
 */
public interface IMovieDao {

    /**
     * 电影管理员新增电影
     */
    public void save(Movie movie);

    /**
     * 管理员删除电影
     */
    public  void delete(Movie movie);

    /**
     * 管理员更改电影
     */
    public void update(Movie movie);


    /**
     * 管理员和用户
     * 电影名称 模糊查询 所有电影
     */
    public List<Movie> findByName(String mvName);

    /**
     * 管理员和用户
     * 根据电影的id来查询电影
     */
    public Movie findById(Long id);

    /**
     * 管理员和用户
     * 根据电影的排期的id来查询电影
     */
    public Movie findBySchid(Long schid);


    /**
     * 用户
     * 根据电影主演名称
     * 模糊查询所有的电影
     */
    public List<Movie> findByMainAct(String name);

    /**
     * 通过条件去数据库查找有多少条记录
     * @param movieName 电影名
     * @param type 电影类型
     * @param area 电影地区
     * @param show 是否上映 1 上映 -1 未上映
     * @param popular 受欢迎程度 - 想看的人数
     * @return
     */
    public Long getRowCount(String movieName, Type type, Area area, Integer show, Integer popular);

    /**
     * 通过条件来查找电影，返回一个集合
     * @param movieName 电影名
     * @param type 电影类型
     * @param area 电影地区
     * @param show 是否上映 上映日期 1 上映 -1 未上映
     * @param popular 受欢迎程度 - 想看的人数
     * @param pageNow 当前页
     * @param pageSize 页面显示多少条
     * @return
     */
    public List<Movie> findByConditions(String movieName, Type type, Area area, Integer show, Integer popular, Long pageNow, Long pageSize);

}
