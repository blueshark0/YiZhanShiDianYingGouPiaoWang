package com.flw.dao;

import com.flw.entity.Cinema;
import com.flw.entity.MovieHall;

import java.util.List;

/**
 * 本接口用于:电影厅Dao数据访问层接口
 *
 * @author Bruce
 * @version v1.0
 * @create 2017-07-24 13:01
 **/

public interface IMovieHallDao {

    /**
     * 保存一个影厅
     * @param movieHall
     */
    void save(MovieHall movieHall);

    /**
     * 通过影厅的ID来删除影厅
     * @param mhId
     */
    void  delete(Long mhId);

    /**
     * 修改这个影厅的信息
     * @param movieHall
     */
    void update(MovieHall movieHall);

    /**
     * 通过影厅Id来查找影厅
     * @param mhId
     * @return
     */
    MovieHall findById(Long mhId);

    /**
     * 通过电影院来查找这个电影院内所有的影厅
     * @param cinema
     * @return
     */
    List<MovieHall> findByCinema(Cinema cinema);
}
