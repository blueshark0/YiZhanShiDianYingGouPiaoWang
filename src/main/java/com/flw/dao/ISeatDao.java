package com.flw.dao;

import com.flw.entity.MovieHall;
import com.flw.entity.Seat;

/**
 * 本接口用于:座位的数据访问层接口
 *
 * @author Bruce
 * @version v1.0
 * @create 2017-07-27 10:18
 **/

public interface ISeatDao {

    /**
     * 保存一个影厅的座位
     * @param seat
     */
    void save(Seat seat);

    /**
     * 删除一个影厅的座位
     * @param Id
     */
    void delete(Long Id);

    /**
     * 更新影厅座位状态
     * @param seat
     */
    void update(Seat seat);

    /**
     * 通过座位Id查找这个座位
     * @param id
     */
    Seat findById(Long id);

    /**
     * 通过座位的x和y坐标来查找这个座位
     * @param x x坐标
     * @param y y坐标
     * @return
     */
    Seat findByXY(MovieHall movieHall,Integer x, Integer y);

}

