package com.flw.dao;

import com.flw.entity.Cinema;
import com.flw.entity.Movie;
import com.flw.entity.Schedule;

import java.util.Date;
import java.util.List;

/**
 * 本接口用于：排期的接口
 *
 * @author Bruce
 * @date 2017/7/23-20:31
 */
public interface IScheduleDao {

    /**
     * 保存一个排期
     * @param schedule
     */
    void save(Schedule schedule);

    /**
     * 跟根据排期的ID来删除这个排期
     * @param schId
     */
    void delete(Long schId);

    /**
     * 更新一个排期
     * @param schedule
     */
    void update(Schedule schedule);


    /**
     * 通过排期的Id寻找这个排期
     * @param schId
     * @return
     */
    Schedule findById(Long schId);

    /**
     * 通过条件来查找排期列表
     * @param movie 电影
     * @param schDate 排期的时间
     * @param cinema 排期的电影院
     * @return
     */
    List<Schedule> findByConditions(Movie movie, Date schDate, Cinema cinema);
}
