package com.flw.dao;

import com.flw.entity.User;

import java.util.List;

/**
 * Created by JC on 2017/7/20.
 * 用户接口
 */
public interface IUserDao {
    /**
     * 保存一个用户
     */
    public void save(User user);

    /**
     * 查询所有的用户
     * @return
     */
    public List<User> findAll();

    /**
     * 根据用户名来查询用户
     * 实际开发中,注册的账号usernum必然是唯一的.
     */
    public User findByUserNum(String userNum);

    /**
     * 修改用户
     */
    public User update(User user);

    /**
     *根据用户的id进行删除操作
     * 因为订单中的外键是user_id
     * 进行级联操作是需要用到id 而不是usernum
     */
    public void delById(Long id);


    /**
     * 通过电话号码查找人
     * @param phone
     * @return
     */
    User findByPhone(String phone);



}
