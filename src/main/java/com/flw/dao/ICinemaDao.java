package com.flw.dao;

import com.flw.entity.Address;
import com.flw.entity.Cinema;

import java.util.List;

/**
 * Created by JC on 2017/7/20.
 * 电影院接口
 */
public interface ICinemaDao {

    /**
     * 通过影院的Id来查找影院
     * @param id
     * @return
     */
    public Cinema findById(Long id);

    /**
     * 管理员
     * 新增一个电影院
     */
    public void save(Cinema cinema);

    /**
     * 管理员删除一个电影院
     */
    public void delete(Cinema cinema);

    /**
     * 管理员修改一个电影院
     */
    public  void  update(Cinema cinema);

    /**
     *  用户
     * 根据地址查找所有影院
     * 如果address为null就是查找所有影院
     */
     public List<Cinema> findByAddress(Address address);

    /**
     * 查询出所有电影院的数量
     * @param cmName 电影院名称
     * @return
     */
     Long getRowCount(String cmName);

    /**
     * 通过电影院名称来查询电影院并分页
     * @param cmName 电影院名称
     * @param pageNow 当前页
     * @param pageSize 每页显示多少个
     * @return
     */
     List<Cinema> findByConditions(String cmName, Long pageNow, Long pageSize);

}
