package com.flw.dao.impl;

import com.flw.dao.IMovieHallDao;
import com.flw.entity.Cinema;
import com.flw.entity.Movie;
import com.flw.entity.MovieHall;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 本类用于:
 *
 * @author Bruce
 * @version v1.0
 * @create 2017-07-24 13:07
 **/
@SuppressWarnings("all")
@Repository
@Transactional
public class MovieHallDaoImpl implements IMovieHallDao{

    //获取Session
    @Autowired
    private SessionFactory sessionFactory;

    //获取SessionFactory
    public Session getSession(){
        return this.sessionFactory.getCurrentSession();
    }


    @Override
    public void save(MovieHall movieHall) {

        getSession().save(movieHall);
    }

    @Override
    public void delete(Long mhId) {
        Movie movie = (Movie) getSession().get(MovieHall.class, mhId);
        if(movie != null){
            getSession().delete(movie);
        }
    }

    @Override
    public void update(MovieHall movieHall) {
        MovieHall mo = (MovieHall) getSession().get(MovieHall.class,movieHall.getId());
        if(mo!=null){
            getSession().clear();
            getSession().delete(movieHall);
        }
    }

    @Override
    public MovieHall findById(Long mhId) {
        return (MovieHall) getSession().get(MovieHall.class,mhId);
    }

    @Override
    public List<MovieHall> findByCinema(Cinema cinema) {
        Criteria c = getSession().createCriteria(MovieHall.class,"m");
        c.add(Restrictions.eq("cinema",cinema));
        return c.list();
    }
}
