package com.flw.dao.impl;

import com.flw.dao.IMovieDao;
import com.flw.entity.*;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.RowCountProjection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 本类用于:
 *
 * @author Bruce
 * @create 2017-07-20 15:04
 **/
@SuppressWarnings("all")
@Repository
@Transactional
public class MovieDaoImpl implements IMovieDao {

    //获取SessionFactory
    @Autowired
    private SessionFactory sessionFactory;

    //获取session
    public Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void save(Movie movie) {
        getSession().save(movie);
    }

    @Override
    public void delete(Movie movie) {
        getSession().delete(movie);
    }

    @Override
    public void update(Movie movie) {
        Movie m = (Movie) getSession().get(Movie.class, movie.getId());
        if (m != null) {
            getSession().clear();
            getSession().update(movie);
        }
    }

    @Override
    public List<Movie> findByName(String mvName) {
        Criteria c = getSession().createCriteria(Movie.class, "m");
        c.add(Restrictions.like("mvName", "%" + mvName + "%"));
        return c.list();
    }

    @Override
    public Movie findById(Long id) {
        return (Movie) getSession().get(Movie.class, id);
    }

    @Override
    public Movie findBySchid(Long schid) {
        Schedule schedule = (Schedule) getSession().get(Schedule.class, schid);
        return schedule.getMovie();
    }

    @SuppressWarnings("all")
    @Override
    public List<Movie> findByMainAct(String name) {
        List<MainAct> listAct = getSession().createQuery("from MainAct where name=:actName").setParameter("actName", name).list();
        List<Movie> listMovie = new ArrayList<>();
        for (MainAct act : listAct) {
            Movie m = (Movie) getSession().get(Movie.class, act.getMovie().getId());
            listMovie.add(m);
        }
        return listMovie;
    }

    @Override
    public Long getRowCount(String movieName, Type type, Area area, Integer show, Integer popular) {

        Criteria c = getSession().createCriteria(Movie.class,"mv");

        if(null != movieName && movieName.trim().length()>0){
            c.add(Restrictions.like("mvName","%"+movieName+"%"));
        }
        if(null != type){
            c.add(Restrictions.eq("type",type));
        }
        if(null != area){
            c.add(Restrictions.like("areq","%"+area+"%"));
        }
        if(null != show) {
            //就是在上映
            if (show > 0) {
                c.add(Restrictions.lt("showDate", new Date()));
            } else {
                c.add(Restrictions.gt("showDate", new Date()));
            }
        }
        if( null != popular){
            c.add(Restrictions.gt("popular",popular));
        }
//        从性能上说不推荐使用集合的Size方法来获得条数
//        Integer count = c.list().size();

        Long count = (Long) c.setProjection(Projections.rowCount()).uniqueResult();
        return count;
    }

    @Override
    public List<Movie> findByConditions(String movieName, Type type, Area area, Integer show,
                                        Integer popular, Long pageNow, Long pageSize) {

        /*
        Criteria c = getSession().createCriteria(Movie.class,"mv");

//        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        if(null != movieName && movieName.trim().length()>0){
            c.add(Restrictions.like("mvName","%"+movieName+"%"));
        }
        if(null != type){
            c.add(Restrictions.eq("type",type));
        }
        if(null != area){
            c.add(Restrictions.like("areq","%"+area+"%"));
        }
        if(null != show) {
            //就是在上映
            if (show > 0) {
                c.add(Restrictions.le("showDate", new Date()));
            } else {
                c.add(Restrictions.ge("showDate", new Date()));
            }
        }else if(null != popular){
            c.add(Restrictions.gt("popular",popular));
        }

//        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        c.setFirstResult((int)((pageNow-1)*pageSize)).setMaxResults(pageSize.intValue());

        return c.list();
        */
        String hql = "from Movie where 1=1";
        if(null != movieName && movieName.trim().length()>0){
            hql=" and mvName like '%'"+movieName+"'%'";
        }
        if(null != type){
            hql+=" and type='"+type+"'";
        }
        if(null != area){
            hql += " and area='"+area+"'";
        }
        if(null != show) {
            if (show > 0) {
                //就是在上映
                hql += " and showDate<sysdate";
            } else {
                //还没上映
                hql += " and showDate>sysdate order by showDate";
            }
        }else if(null != popular){
            hql += " and popular>"+popular+" order by popular desc";
        }
        Query query = getSession().createQuery(hql);

        query.setFirstResult((int) ((pageNow-1)*pageSize));
        query.setMaxResults(pageSize.intValue());
        return query.list();
    }

}


