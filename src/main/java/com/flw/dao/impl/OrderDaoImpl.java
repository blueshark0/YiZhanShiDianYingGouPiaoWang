package com.flw.dao.impl;

import com.flw.dao.IOrderDao;
import com.flw.entity.Order;
import com.flw.entity.OrderItem;
import com.flw.entity.Schedule;
import com.flw.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 本类用于:
 *
 * @author Bruce
 * @create 2017-07-20 15:49
 **/
@SuppressWarnings("all")
@Repository
@Transactional
public class OrderDaoImpl implements IOrderDao {

    //获取SessionFactory
    @Autowired
    private SessionFactory sessionFactory;

    //获取session
    public Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void save(Order order) {
        getSession().save(order);
    }

    @Override
    public void delete(Schedule schedule) {
        getSession().delete(schedule);
    }

    @Override
    public OrderItem find(Long schid) {
        Schedule schedule = (Schedule) getSession().get(Schedule.class, schid);
        return (OrderItem) getSession().createQuery("from OrderItem where schedule=:sch").setParameter("sch",schedule).uniqueResult();
    }

    @Override
    public Map<Long, OrderItem> getItems() {
        return null;
    }

    @Override
    public void update(Order order) {
        Order ord = (Order) getSession().get(Order.class, order.getOrderId());
        if(ord != null){
            getSession().update(order);
        }
    }

    @Override
    public List<Order> findByUser(User user) {
        return getSession().createQuery("from Order where user = :u").setParameter("u",user).list();
    }

}
