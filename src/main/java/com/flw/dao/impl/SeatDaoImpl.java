package com.flw.dao.impl;

import com.flw.dao.ISeatDao;
import com.flw.entity.MovieHall;
import com.flw.entity.Seat;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * 本类用于:
 *
 * @author Bruce
 * @version v1.0
 * @create 2017-07-27 10:25
 **/
@SuppressWarnings("all")
@Repository
@Transactional
public class SeatDaoImpl implements ISeatDao {

    //获取Session
    @Autowired
    private SessionFactory sessionFactory;

    //获取SessionFactory
    public Session getSession(){
        return this.sessionFactory.getCurrentSession();
    }

    @Override
    public void save(Seat seat) {
        getSession().save(seat);
    }

    @Override
    public void delete(Long Id) {
        Seat seat = (Seat) getSession().get(Seat.class,Id);
        if (seat!=null) {
            getSession().delete(seat);
        }

    }

    @Override
    public void update(Seat seat) {
        Seat seats= (Seat) getSession().get(Seat.class,seat.getSeatId());
        if (seats!=null){
            getSession().clear();
            getSession().update(seats);
        }

    }

    @Override
    public Seat findById(Long id) {
        return (Seat) getSession().get(Seat.class,id);
    }

    @Override
    public Seat findByXY(MovieHall movieHall, Integer x, Integer y) {
        return (Seat) getSession().createQuery("from Seat where movieHall=:mh and seatX=:sx and seatY=:sy")
                .setParameter("mh",movieHall)
                .setParameter("sx",x).setParameter("sy",y).uniqueResult();
    }


}
