package com.flw.interceptor;

import com.flw.entity.User;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Administrator on 17-7-13.
 */
public class LoginInterceptor implements HandlerInterceptor{
    /**
     * 进入handler之前执行
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("===login interceptor===");

        //获取session作用域中的User对象
        User user = (User) request.getSession().getAttribute("user");

        System.out.println("user:"+user);

        //判断是否存在
        if(null!=user){
            return true;
        }

        //如果不存在...转发到登陆界面.
        request.getRequestDispatcher("/WEB-INF/jsp/user/login.jsp").forward(request,response);
        return false;//放行
        //return false;//不放行.
    }

    /**
     * 进入handler方法之后,以及返回ModelAndView之前
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    /**
     * handler执行后执行
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
