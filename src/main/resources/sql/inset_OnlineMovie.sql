
--电影封面
INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'atangqiyun.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'bianxingjingang5.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'daerduotutu.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'dahufa.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'fuzixiongbing.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'jianjundaye.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'jingcheng81hao.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'jueshigaoshou.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'kongbubiyezhao2.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'miaoxingren.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'miyuzhishihunlin.jpg');


--封面
INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'qinhenangao.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'rangaihuoxiaqu.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'sanguangshaonv.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'sanshengsanshishilitaohua.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'sdemimi.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'shentounaiba.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'shenyeshitang2.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'shuishiqiuwang.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'wukongzhuan.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'yebanxionglin.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'zhandaotulang.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'zhandaotulang.jpg');

INSERT INTO M_COVER(ID,IMGNAME) VALUES(m_cover_id.nextval,'zhaungxie31hao.jpg');


--电影
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'中国','王微','一茶一世界，一步一片天','twoD','在南方一座热闹城市里的一间茶叶店中，生活着一群陶瓷茶宠。他们以被浇茶变色为荣。而阿唐是其中唯一一个怎么浇茶都不会变色的茶宠。有一天，一个来自未来的神奇机器人小来，意外飞进了茶叶店，为阿唐带来一丝希望。为了找到让自己颜色变漂亮的方法，中华小茶宠阿唐决定为美大步走，与机器人小来一起出发，踏上一段寻找未来的历险旅程。','阿唐奇遇','5880','7.0','冒险','0',"TO_DATE"('2017-07-21','yy-MM-dd'),'1');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'美国','迈克尔·贝','擎天柱对战','threeD','影片将会有两条故事线，一边是以凯德·伊格尔（马克·沃尔伯格 饰）为主的人类，将与恐龙金刚一起面对即将到来的新威胁；另一边是擎天柱在太空中搜寻其统治者“五面怪”，也是它派赏金猎人“禁闭”四处搜捕领袖，而擎天柱将在途中遭遇超级反派“宇宙大帝”。','变形金刚5：最后的骑士','7380','8.0','动作','0',"TO_DATE"('2017-06-23','yy-MM-dd'),'2');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'中国','速达','萌萌图图做美食','twoD','电视系列片《大耳朵图图》在小朋友中具有很高 人气,美影厂和图图公司将推出动画电影《大耳朵图图之美食狂想曲》。','大耳朵图图之美食狂想曲','5400','7.0','动画','0',"TO_DATE"('2017-07-28','yy-MM-dd'),'3');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'中国','杨志刚','感谢给我逆境的众生','twoD','《大护法》是一部手绘山水背景，江湖风格的成人动画。奕卫国大护法为寻找失踪已久的太子，来到一个陌生小镇，小镇上空漂浮着一个巨大的黑色花生状球体，故名为花生镇。这里的居民外形酷似花生，当身上长出鬼蘑菇时便会被守卫枪决。他们反对一切外来事物，麻木而愚昧。在躲避守卫追杀的过程中，大护法偶遇太子，并一同卷入了一场关于欲望的冒险故事。','大护法','6000','7.0','动画','0',"TO_DATE"('2017-07-13','yy-MM-dd'),'4');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'大陆','袁卫东','啼笑皆非生死考验消隔阂','twoD','梦想靠大项目咸鱼翻身的范小兵（大鹏饰）始终无法摆脱一个阴影，那就是退伍军人出身的老爹范英雄（范伟饰）。小兵油嘴滑舌爱忽悠，英雄刚正不阿会武术，小兵斗英雄，天雷勾地火。 卤煮店小老板刘雯（张天爱饰）与小兵相识十年，两人友达之上恋爱未满，她是小兵唯一愿意吐露真心的港湾。范小兵欠下大老板OK哥（任达华饰）的钱，被追债小弟方健（乔杉饰）一路折磨，倒霉不断，还面临凶狠狗哥（梁龙饰）的生命威胁。范小兵不得已将弄钱对象转向老爹，不知情的范英雄此时正躲闪着邻居邬仙仙（邬君梅饰）如狼似虎的猛烈追求……','父子雄兵','5400','7.0','剧情','0',"TO_DATE"('2017-07-21','yy-MM-dd'),'5');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'大陆','刘伟强','建国三部曲”第三部','twoD','1927年4月，上海爆发了四一二反革命政变，大批党员干部被屠杀。共产党人拿起武器，在周恩来、毛泽东、朱德等人的领导下，先后发动了南昌起义和秋收起义，一支全新的强大军队诞生了.','建军大业','5400','7.0','历史','747',"TO_DATE"('2017-07-28','yy-MM-dd'),'6');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'中国','钱人豪','团结湖地宫惊现，尸婴案孽缘再起','twoD','《京城81号2》取材自东方四大鬼宅之首的朝内81号地宫真实故事。民国末期，正逢乱世，京城军阀哗变，少帅张骘生（张智霖 饰）被逼迎娶大军阀之女纪金翠（钟欣潼饰），随即府内怪事丛生，人心惶惶。不久后京城涌现大批“尸婴案”，一切矛头指向少帅原配夫人钮梦鹤（梅婷 饰），随后钮梦鹤离奇失踪，一夜之间京城81号惨遭灭门，留下一座破败鬼宅。百年后，文物修复师宋腾在修复古宅过程中挖出大量婴儿尸骨与诡异符咒，一桩桩血淋淋的旧事似乎要将所有人拉进深渊，前世今生的恩怨情仇都一一浮现，而地宫冤魂鬼门索命的故事也就此拉开……','京城81号2','7800','8.0','恐怖','0',"TO_DATE"('2017-07-06','yy-MM-dd'),'7');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'大陆','卢正雨','混混遇吃货 走上正义路','twoD','混混卢小鱼，在一次执行破坏任务的过程中遇到了大大咧咧的吃货小曼，小鱼顺势利用小曼去完成他的“大任务”，由此卷入了美食江湖，见识了光怪陆离的烹饪绝技，也被各位厨师高手的真诚感化，走上正义之路。','绝世高手','5400','7.0','动作','0',"TO_DATE"('2017-07-07','yy-MM-dd'),'8');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'中国','陆诗雨','毕业盛典，女魂不散','twoD','苏亮没想到还会再次见到自己的女朋友尹飘飘，可是几年前，尹飘飘已经意外坠崖死亡。原来，在失去飘飘以后，苏亮被身边的多名女生都主动追求过，苏亮即使难以从失去爱人的阴霾中走出来但最后还是选择了一名女生黄小米作为自己的女朋友。在一次度假中，苏亮突然见到了尹飘飘，同时身边的伙伴也随着尹飘飘的出现而接连陷入灾难之中，灵异事件如影随形，众人陷入巨大的恐慌中，莫名的痛感再次向苏亮袭来，而有关尹飘飘死亡的一系列诡谲真相也渐渐浮出水面……','恐怖毕业照2','5400','7.0','恐怖','2698',"TO_DATE"('2017-08-18','yy-MM-dd'),'9');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'中国','陈木胜','喵趣横生','twoD','千百年来，地球上一直住着一种外星生物，名叫喵星人。它们从遥远的喵星来到地球，化身为猫科动物，分散在世界每个角落。萌萌的外表，加上机智聪明的头脑，轻易就得到人类的宠爱。不愁吃喝的它们桀骜不驯，活像饱经沧桑的江湖大佬。犀犀利就是他们中的一员，作为一名正牌的喵星来客，它肩负使命长途跋涉来到地球，却意外遇到了吴守龙（古天乐饰演）一家，上演了一幕相运相生，喵趣横生的人猫奇遇记。','喵星人','7080','7.0','喜剧','0',"TO_DATE"('2017-07-14','yy-MM-dd'),'10');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'中国','戴维','中国恐怖片？期待吧','twoD','20年前神秘的北部深山，深夜，迷雾笼罩的幽谷密林，深不可测的幽幽湖水，一个中年男人迷失其中，几天后他回到村里，但村民们感到他中了邪，随之而来的不可思议的神秘恐怖事件更令人视他为邪灵附体。一队调查者为揭开当年事件真相深入幽谷，等待他们的却是越来越多的谜团：神秘的群体失踪、面目全非血泊中的女尸、血腥残忍的行刑者、飘忽不定的幽魂鬼影、暗藏湖中的邪灵、催眠唤醒的恐怖记忆，神秘恐怖事件变得越来越复杂。随着他们一点点靠近真相，却不停遭遇恶兽、邪灵、罪恶、阴谋，调查越深入变得越来越诡异而恐怖。最后的真相是什么？','谜域之噬魂岭','5580','7.0','恐怖','0',"TO_DATE"('2017-07-21','yy-MM-dd'),'11');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'中国','蒋卓原','放学别走','twoD','在一所名为青禾男高的学校里，荆浩和阿屁、谭家木、幺鸡、二饼四位少年在特别的校园环境中结成了生死与共的挚友兄弟，并成为这所男高里唯一对抗校园霸凌的团体，压制了柴田等人横行霸道的校园恶行。在特殊的时代氛围里，两方势力保持着微妙的平衡，直到女教师柳禾的出现，平衡被打破，这所男高里的少年们将何去何从，就此展开的激战如箭在弦。','青禾男高','5400','7.0','爱情','0',"TO_DATE"('2017-07-13','yy-MM-dd'),'12');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'中国','查文白','真情常驻人间','twoD','该片改编自“宁波爱心大使”梁国华的真实事迹，主要讲述了湖北籍外来务工人员梁国华虽然患上尿毒症，却用自己的善良收获了宁波人民的爱心，之后不仅成功战胜了疾病且收获了爱情，从一个“绝症患者”重生成为幸福的“新宁波人”的暖心故事。','让爱活下去','5280','7.0','剧情','733',"TO_DATE"('2017-07-25','yy-MM-dd'),'13');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'中国','王冉','不一样，又怎样','twoD','江湖人称“神经”的少女陈惊，在男闺蜜、神秘人物的支持下共组2.5次元乐团，战权威破成见，展开一场生猛搞笑又奇葩热血的“闪光少女”逆袭之旅。','闪光少女','5400','7.0','剧情','0',"TO_DATE"('2017-07-21','yy-MM-dd'),'14');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'大陆','赵小丁 安东尼·拉默里纳拉','三生三世，非你莫属','twoD','改编自唐七公子同名小说，讲述了天族太子夜华与一凡间女子素素相知相爱，素素怀孕后，夜华将其带往天庭。','三生三世十里桃花','5400','8.0','爱情','1262',"TO_DATE"('2017-08-03','yy-MM-dd'),'15');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'中国','徐林','一觉醒来，我变成了他\她','twoD','香港实力派男星吴启华饰演男一号方浪，身为内衣设计师的方浪和御姐港星麦家琪饰演的女主于莎莎是一对互相看不对眼的欢喜冤家。在经历了一次意想不到的奇妙意外后，两人的生活工作发生了翻天覆地的变化，逐渐认识到了爱情和婚姻的真谛。','S的秘密','5400','7.0','爱情','0',"TO_DATE"('2017-07-20','yy-MM-dd'),'16');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'美国','凯尔·巴尔达 皮艾尔·柯芬','小黄人激萌归来','threeD','《神偷奶爸3》将延续前两部的温馨、搞笑风格，聚焦格鲁和露西的婚后生活，继续讲述格鲁和三个女儿的爆笑故事。“恶棍”奶爸格鲁将会如何对付大反派巴萨扎·布莱德，调皮可爱的小黄人们又会如何耍贱卖萌，无疑让全球观众万分期待。该片配音也最大程度沿用前作阵容，史蒂夫·卡瑞尔继续为男主角格鲁配音，皮埃尔·柯芬也将继续为经典角色小黄人配音，而新角色巴萨扎·布莱德则由《南方公园》主创元老崔·帕克为其配音。','神偷奶爸3','5760','7.5','动画','0',"TO_DATE"('2017-07-07','yy-MM-dd'),'17');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'日本','松冈锭司','温情，感动，这是一部暖心的电影','twoD','位于繁华街区角落、只在深夜开店的“饭屋”餐馆，它的菜单永远中有猪肉酱汤套餐一种，但店老板（小林薰 饰）却根据不同的客人利用现有食材做出各种料理，同时他会带出不同阶层的客人们充满人情味的风格各异的故事，刻画了店老板以及来店里吃饭的各类客人们的个性形象。','深夜食堂2','6400','7.7','剧情','0',"TO_DATE"('2017-07-18','yy-MM-dd'),'18');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'中国','裘仲维','从头再战，实现梦想','twoD','电影主要讲述了主人公洪旭东，因当年一个错误决定，彻底与他所钟爱的足球事业告别，为扭转命运，实现梦想，他决心从头再战，通过不懈努力重聚了分崩离析的球队，实现自我救赎的故事。','谁是球王','5400','7.0','喜剧','0',"TO_DATE"('2017-08-03','yy-MM-dd'),'19');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'大陆','郭子健','这是悟空的故事','twoD','这不是西游记的任何章节，这是悟空的故事，彼时孙悟空还不是震撼天地的齐天大圣，他只是只桀傲不驯的猴子。天庭毁掉他的花果山以掌控众生命运，他便决心跟天庭对抗，毁掉一切戒律。在天庭，孙悟空遇到不能爱的阿紫，一生的宿敌杨戬，和思念昔日爱人阿月的天蓬，他们的身份注定永生相杀，但其实不甘命运摆布的又何止孙悟空一人？却没想到反抗却带来更大的浩劫。他们所做的一切，究竟是不知天高地厚的热血轻狂，还是无奈宿命难改的压抑绝望？难道命运真的早已注定？悟空不服，他再次挥动金箍棒，要让这诸佛都烟消云散!','悟空传','5400','7.0','动作','0',"TO_DATE"('2017-07-13','yy-MM-dd'),'20');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'中国','李耀东','铃声再起 咒怨重生','twoD','影片讲述了因一起未知来电而被无影怨灵索命的恐怖故事。雨夜鬼雾弥漫，阴气深重的宅院中，女作家周琪琪接到一起陌生来电，因好奇心而陷入到一段暗无天日的致命游戏中，各种离奇事件接踵而至，身边的朋友接连惨遭毒害，神秘诡影如影随行，就在周琪琪最恐惧的时刻，凶铃再次响起......','夜半凶铃','5520','7.0','惊悚','626',"TO_DATE"('2017-07-28','yy-MM-dd'),'21');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'中国','刘艳杰','深入狼穴，为国缉毒','twoD','鹏城市公安局禁毒大队民警蓝海从警8年，业务能力极强，抓获无数吸贩毒人员，被毒贩列为“敌为我用，非用必杀”之人。某日，辖区内发生一起因吸食毒品砍人事件，经查明，这是一种使人极易上瘾的新型毒品“火冰”，缉毒工作随即展开。　　东南亚毒枭“毒狼”伙同化学专家白酬之以中国境内为主要市场，长期制毒贩毒，中国警方派遣绰号“红蜻蜓”“老刁”两名侦查员打入“毒狼”内部，在计划抓捕“毒狼”的同时一并抓捕已经仪容的白酬之。随着事件的不断升级，蓝海弟弟蓝天阴差阳错的卷入事件中，先是白酬之诱骗蓝天以快递员身份助其转送毒品样本','战刀屠狼','5400','7.0','动作','0',"TO_DATE"('2017-07-21','yy-MM-dd'),'22');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'中国','吴京','孤身犯险，生死逃亡','twoD','冷锋突然被卷入了一场非洲国家叛乱，本可以安全撤离，却因无法忘记曾经为军人的使命，孤身犯险冲回沦陷区，带领身陷屠杀中的同胞和难民，展开生死逃亡。','战狼2','6000','7.5','动作','980',"TO_DATE"('2017-07-28','yy-MM-dd'),'23');
INSERT INTO M_MOVIE(ID,AREA,DIRECTOR,EXPLAIN,FRAME,INFO,MVNAME,MVTIME,SCORE,TYPE,POPULAR,SHOWDATE,COVER_ID)
			VALUES(m_movie_id.nextval,'中国','李克龙','31号别墅交手鬼魂','twoD','《撞邪31号》讲述的是一段因设计师宇浩与热恋女友安妮因工作入住31号别墅而引发一系列离奇事件的故事。别墅半夜经常有鬼影出现，被居民定义为鬼宅。鬼影在炎炎夏日蓄谋攻击女友安妮，深夜中几度与安妮交手。事情恶化，宇浩被不明魂魄拖下深水险些丧命，后托梦物品被盗，宇浩被诬陷，宇浩决定要找出鬼魂还自己清白……','撞邪31号','5400','7.0','惊悚','0',"TO_DATE"('2017-06-30','yy-MM-dd'),'24');

--电影剧照
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'atqy1.jpg',1);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'atqy2.jpg',1);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'atqy3.jpg',1);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'atqy4.jpg',1);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'bxjg1.jpg',2);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'bxjg2.jpg',2);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'bxjg3.jpg',2);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'bxjg4.jpg',2);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'bxjg5.jpg',2);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'bxjg6.jpg',2);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'dedtt1.jpg',3);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'dedtt2.jpg',3);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'dedtt3.jpg',3);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'dhf1.jpg',4);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'dhf2.jpg',4);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'dhf3.jpg',4);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'dhf4.jpg',4);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'dhf5.jpg',4);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'dhf6.jpg',4);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'dhf7.jpg',4);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'fzxb1.jpg',5);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'fzxb2.jpg',5);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'fzxb3.jpg',5);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'fzxb4.jpg',5);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'fzxb5.jpg',5);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'fzxb6.jpg',5);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'fzxb7.jpg',5);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'jjdy1.jpg',6);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'jjdy2.jpg',6);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'jjdy3.jpg',6);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'jjdy4.jpg',6);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'jjdy5.jpg',6);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'jjdy6.jpg',6);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'jjdy7.jpg',6);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'jc1.jpg',7);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'jc2.jpg',7);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'jc3.jpg',7);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'jsgs1.jpg',8);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'kbbyz1.jpg',9);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'kbbyz2.jpg',9);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'mxr1.jpg',10);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'mxr2.jpg',10);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'mxr3.jpg',10);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'mxr4.jpg',10);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'mxr5.jpg',10);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'mxr6.jpg',10);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'mxr7.jpg',10);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'myzshl1.jpg',11);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'myzshl2.jpg',11);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'myzshl3.jpg',11);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'myzshl4.jpg',11);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'myzshl5.jpg',11);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'myzshl6.jpg',11);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'myzshl7.jpg',11);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'qmng1.jpg',12);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'qmng2.jpg',12);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'qmng3.jpg',12);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'qmng4.jpg',12);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'qmng5.jpg',12);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'qmng6.jpg',12);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'qmng7.jpg',12);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'rahxq1.jpg',13);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'rahxq2.jpg',13);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'rahxq3.jpg',13);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'rahxq4.jpg',13);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'rahxq5.jpg',13);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'rahxq6.jpg',13);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'rahxq7.jpg',13);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'sgsn1.jpg',14);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'sgsn2.jpg',14);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'sgsn3.jpg',14);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'sgsn4.jpg',14);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'sgsn5.jpg',14);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'sgsn6.jpg',14);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'sgsn7.jpg',14);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'ssssslth1.jpg',15);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'ssssslth2.jpg',15);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'ssssslth3.jpg',15);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'ssssslth4.jpg',15);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'ssssslth5.jpg',15);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'sdmm1.jpg',16);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'sdmm2.jpg',16);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'sdmm3.jpg',16);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'sdmm4.jpg',16);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'stnb1.jpg',17);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'stnb2.jpg',17);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'stnb3.jpg',17);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'stnb4.jpg',17);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'stnb5.jpg',17);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'stnb6.jpg',17);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'stnb7.jpg',17);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'syst1.jpg',18);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'syst2.jpg',18);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'syst3.jpg',18);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'syst4.jpg',18);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'syst5.jpg',18);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'syst6.jpg',18);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'syst7.jpg',18);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'wkz1.jpg',20);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'wkz2.jpg',20);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'wkz3.jpg',20);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'wkz4.jpg',20);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'wkz5.jpg',20);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'wkz6.jpg',20);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'wkz7.jpg',20);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'ybxl1.jpg',21);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'ybxl2.jpg',21);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'ybxl3.jpg',21);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'ybxl4.jpg',21);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'ybxl5.jpg',21);

INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'zl1.jpg',23);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'zl2.jpg',23);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'zl3.jpg',23);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'zl4.jpg',23);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'zl5.jpg',23);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'zl6.jpg',23);
INSERT INTO M_IMAGE(ID,IMGNAME,MOVIE_ID) VALUES(m_image_id.nextval,'zl7.jpg',23);

--主演
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'无',1);


INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'彼特·库伦',2);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'马克·沃尔伯格',2);

INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'金炜',3);

INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'无',4);


INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'大鹏',5);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'范伟',5);


INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'刘烨',6);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'朱亚文',6);


INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'张智霖',7);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'梅婷',7);

INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'卢正雨',8);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'郭采洁',8);

INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'陈圆',9);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'刘俐儿',9);



INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'古天乐',10);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'马丽',10);


INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'李果',11);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'闫鹿杨',11);

INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'景甜',12);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'欧豪',12);


INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'夏德俊',13);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'郑佩佩',13);


INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'徐璐',14);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'彭昱畅',14);


INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'刘亦菲',15);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'杨洋',15);


INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'吴启华',16);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'麦家琪',16);


INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'史蒂夫·卡瑞尔',17);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'克里斯汀·韦格',17);


INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'小林薰',18);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'安田成美',18);

INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'尹航',19);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'代旭',19);

INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'彭于晏',20);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'倪妮',20);


INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'刘青',21);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'李浩轩',21);


INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'张永达',22);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'庄小龙',22);


INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'吴京',23);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'弗兰克·格里罗',23);

INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'来喜',24);
INSERT INTO M_MAINACT(ID,NAME,MOVIE_ID) VALUES(m_maid.nextval,'廖蔚蔚',24);


--电影院地址
INSERT INTO M_ADDRESS(ADDRESSID,PROVENCE,CITY,COUNTY,DETAILADRESS,EMAILCODE) VALUES(m_address_id.nextval,'江苏省','苏州市','相城区','富元路1号','223344');

INSERT INTO M_ADDRESS(ADDRESSID,PROVENCE,CITY,COUNTY,DETAILADRESS,EMAILCODE) VALUES(m_address_id.nextval,'江苏省','常州市','武进区','海中路26号','245632');

INSERT INTO M_ADDRESS(ADDRESSID,PROVENCE,CITY,COUNTY,DETAILADRESS,EMAILCODE) VALUES(m_address_id.nextval,'江苏省','淮安市','涟水县','高沟高心路38号','223412');

INSERT INTO M_ADDRESS(ADDRESSID,PROVENCE,CITY,COUNTY,DETAILADRESS,EMAILCODE) VALUES(m_address_id.nextval,'江苏省','常熟市','枫林县','枫林路29号','256944');

INSERT INTO M_ADDRESS(ADDRESSID,PROVENCE,CITY,COUNTY,DETAILADRESS,EMAILCODE) VALUES(m_address_id.nextval,'江苏省','南通市','文通区','朱家路85号','321466');


--电影院
INSERT INTO M_CINEMA(ID,CMNAME,CMTYPE,ADDRESS_ID) VALUES(m_cinema_id.nextval,'万东影院','twoD',1);

INSERT INTO M_CINEMA(ID,CMNAME,CMTYPE,ADDRESS_ID) VALUES(m_cinema_id.nextval,'万南影院','threeD',2);

INSERT INTO M_CINEMA(ID,CMNAME,CMTYPE,ADDRESS_ID) VALUES(m_cinema_id.nextval,'万西影院','全景声',3);

INSERT INTO M_CINEMA(ID,CMNAME,CMTYPE,ADDRESS_ID) VALUES(m_cinema_id.nextval,'万北影院','IMAX',4);

INSERT INTO M_CINEMA(ID,CMNAME,CMTYPE,ADDRESS_ID) VALUES(m_cinema_id.nextval,'万中影院','IMAX',5);


--影厅
INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'twoD','梅',1);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'threeD','兰',1);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'全景声','竹',1);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'IMAX','菊',1);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'twoD','梅',2);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'threeD','兰',2);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'全景声','竹',2);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'IMAX','菊',2);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'twoD','梅',3);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'threeD','兰',3);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'全景声','竹',3);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'IMAX','菊',3);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'twoD','梅',4);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'threeD','兰',4);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'全景声','竹',4);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'IMAX','菊',4);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'twoD','梅',5);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'threeD','兰',5);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'全景声','竹',5);

INSERT INTO M_MOVIEHALL(ID,HALLTYPE,HALLNAME,CINEMA_ID) VALUES(m_moviehall_id.nextval,'IMAX','菊',5);



--排期
INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-21 08:00:00','yy-MM-dd hh24:mi:ss'),'1','1','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-21 12:00:00','yy-MM-dd hh24:mi:ss'),'1','1','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-21 18:00:00','yy-MM-dd hh24:mi:ss'),'1','1','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-22 08:00:00','yy-MM-dd hh24:mi:ss'),'1','1','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-22 12:00:00','yy-MM-dd hh24:mi:ss'),'1','1','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-22 18:00:00','yy-MM-dd hh24:mi:ss'),'1','1','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-23 08:00:00','yy-MM-dd hh24:mi:ss'),'1','1','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-23 12:00:00','yy-MM-dd hh24:mi:ss'),'1','1','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-23 18:00:00','yy-MM-dd hh24:mi:ss'),'1','1','1');



INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-23 08:00:00','yy-MM-dd hh24:mi:ss'),'1','2','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-23 12:00:00','yy-MM-dd hh24:mi:ss'),'1','2','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-23 18:00:00','yy-MM-dd hh24:mi:ss'),'1','2','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-24 08:00:00','yy-MM-dd hh24:mi:ss'),'1','2','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-24 12:00:00','yy-MM-dd hh24:mi:ss'),'1','2','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-24 18:00:00','yy-MM-dd hh24:mi:ss'),'1','2','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-25 08:00:00','yy-MM-dd hh24:mi:ss'),'1','2','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-25 12:00:00','yy-MM-dd hh24:mi:ss'),'1','2','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-25 18:00:00','yy-MM-dd hh24:mi:ss'),'1','2','1');



INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-28 08:00:00','yy-MM-dd hh24:mi:ss'),'1','3','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-28 12:00:00','yy-MM-dd hh24:mi:ss'),'1','3','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-28 18:00:00','yy-MM-dd hh24:mi:ss'),'1','3','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-29 08:00:00','yy-MM-dd hh24:mi:ss'),'1','3','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-29 12:00:00','yy-MM-dd hh24:mi:ss'),'1','3','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-29 18:00:00','yy-MM-dd hh24:mi:ss'),'1','3','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-30 08:00:00','yy-MM-dd hh24:mi:ss'),'1','3','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-30 12:00:00','yy-MM-dd hh24:mi:ss'),'1','3','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-30 18:00:00','yy-MM-dd hh24:mi:ss'),'1','3','1');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-13 08:00:00','yy-MM-dd hh24:mi:ss'),'1','4','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-13 12:00:00','yy-MM-dd hh24:mi:ss'),'1','4','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-13 18:00:00','yy-MM-dd hh24:mi:ss'),'1','4','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-14 08:00:00','yy-MM-dd hh24:mi:ss'),'1','4','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-14 12:00:00','yy-MM-dd hh24:mi:ss'),'1','4','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-14 18:00:00','yy-MM-dd hh24:mi:ss'),'1','4','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-15 08:00:00','yy-MM-dd hh24:mi:ss'),'1','4','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-15 12:00:00','yy-MM-dd hh24:mi:ss'),'1','4','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-15 18:00:00','yy-MM-dd hh24:mi:ss'),'1','4','1');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-21 08:00:00','yy-MM-dd hh24:mi:ss'),'1','5','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-21 12:00:00','yy-MM-dd hh24:mi:ss'),'1','5','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-21 18:00:00','yy-MM-dd hh24:mi:ss'),'1','5','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-22 08:00:00','yy-MM-dd hh24:mi:ss'),'1','5','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-22 12:00:00','yy-MM-dd hh24:mi:ss'),'1','5','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-22 18:00:00','yy-MM-dd hh24:mi:ss'),'1','5','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-23 08:00:00','yy-MM-dd hh24:mi:ss'),'1','5','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-23 12:00:00','yy-MM-dd hh24:mi:ss'),'1','5','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-23 18:00:00','yy-MM-dd hh24:mi:ss'),'1','5','2');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-28 08:00:00','yy-MM-dd hh24:mi:ss'),'1','6','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-28 12:00:00','yy-MM-dd hh24:mi:ss'),'1','6','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-28 18:00:00','yy-MM-dd hh24:mi:ss'),'1','6','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-29 08:00:00','yy-MM-dd hh24:mi:ss'),'1','6','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-29 12:00:00','yy-MM-dd hh24:mi:ss'),'1','6','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-29 18:00:00','yy-MM-dd hh24:mi:ss'),'1','6','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-30 08:00:00','yy-MM-dd hh24:mi:ss'),'1','6','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-30 12:00:00','yy-MM-dd hh24:mi:ss'),'1','6','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-30 18:00:00','yy-MM-dd hh24:mi:ss'),'1','1','2');



INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-07 08:00:00','yy-MM-dd hh24:mi:ss'),'1','7','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-07 12:00:00','yy-MM-dd hh24:mi:ss'),'1','7','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-07 18:00:00','yy-MM-dd hh24:mi:ss'),'1','7','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-08 08:00:00','yy-MM-dd hh24:mi:ss'),'1','7','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-08 12:00:00','yy-MM-dd hh24:mi:ss'),'1','7','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-08 18:00:00','yy-MM-dd hh24:mi:ss'),'1','7','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-09 08:00:00','yy-MM-dd hh24:mi:ss'),'1','7','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-09 12:00:00','yy-MM-dd hh24:mi:ss'),'1','7','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-09 18:00:00','yy-MM-dd hh24:mi:ss'),'1','7','1');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-18 08:00:00','yy-MM-dd hh24:mi:ss'),'1','8','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-18 12:00:00','yy-MM-dd hh24:mi:ss'),'1','8','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-18 18:00:00','yy-MM-dd hh24:mi:ss'),'1','8','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-19 08:00:00','yy-MM-dd hh24:mi:ss'),'1','8','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-19 12:00:00','yy-MM-dd hh24:mi:ss'),'1','8','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-19 18:00:00','yy-MM-dd hh24:mi:ss'),'1','8','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-20 08:00:00','yy-MM-dd hh24:mi:ss'),'1','8','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-20 12:00:00','yy-MM-dd hh24:mi:ss'),'1','8','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-20 18:00:00','yy-MM-dd hh24:mi:ss'),'1','8','1');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-14 08:00:00','yy-MM-dd hh24:mi:ss'),'1','9','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-14 12:00:00','yy-MM-dd hh24:mi:ss'),'1','9','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-14 18:00:00','yy-MM-dd hh24:mi:ss'),'1','9','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-15 08:00:00','yy-MM-dd hh24:mi:ss'),'1','9','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-15 12:00:00','yy-MM-dd hh24:mi:ss'),'1','9','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-15 18:00:00','yy-MM-dd hh24:mi:ss'),'1','9','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-16 08:00:00','yy-MM-dd hh24:mi:ss'),'1','9','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-16 12:00:00','yy-MM-dd hh24:mi:ss'),'1','9','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-16 18:00:00','yy-MM-dd hh24:mi:ss'),'1','9','2');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-21 08:00:00','yy-MM-dd hh24:mi:ss'),'1','10','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-21 12:00:00','yy-MM-dd hh24:mi:ss'),'1','10','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-21 18:00:00','yy-MM-dd hh24:mi:ss'),'1','10','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-22 08:00:00','yy-MM-dd hh24:mi:ss'),'1','10','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-22 12:00:00','yy-MM-dd hh24:mi:ss'),'1','10','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-22 18:00:00','yy-MM-dd hh24:mi:ss'),'1','10','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-23 08:00:00','yy-MM-dd hh24:mi:ss'),'1','10','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-23 12:00:00','yy-MM-dd hh24:mi:ss'),'1','10','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-23 18:00:00','yy-MM-dd hh24:mi:ss'),'1','10','3');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-13 08:00:00','yy-MM-dd hh24:mi:ss'),'1','11','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-13 12:00:00','yy-MM-dd hh24:mi:ss'),'1','11','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-13 18:00:00','yy-MM-dd hh24:mi:ss'),'1','11','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-14 08:00:00','yy-MM-dd hh24:mi:ss'),'1','11','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-14 12:00:00','yy-MM-dd hh24:mi:ss'),'1','11','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-14 18:00:00','yy-MM-dd hh24:mi:ss'),'1','11','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-15 08:00:00','yy-MM-dd hh24:mi:ss'),'1','11','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-15 12:00:00','yy-MM-dd hh24:mi:ss'),'1','11','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-15 18:00:00','yy-MM-dd hh24:mi:ss'),'1','11','3');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-25 08:00:00','yy-MM-dd hh24:mi:ss'),'1','12','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-25 12:00:00','yy-MM-dd hh24:mi:ss'),'1','12','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-25 18:00:00','yy-MM-dd hh24:mi:ss'),'1','12','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-26 08:00:00','yy-MM-dd hh24:mi:ss'),'1','12','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-26 12:00:00','yy-MM-dd hh24:mi:ss'),'1','12','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-26 18:00:00','yy-MM-dd hh24:mi:ss'),'1','12','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-27 08:00:00','yy-MM-dd hh24:mi:ss'),'1','12','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-27 12:00:00','yy-MM-dd hh24:mi:ss'),'1','12','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-27 18:00:00','yy-MM-dd hh24:mi:ss'),'1','12','1');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-21 08:00:00','yy-MM-dd hh24:mi:ss'),'1','13','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-21 12:00:00','yy-MM-dd hh24:mi:ss'),'1','13','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-21 18:00:00','yy-MM-dd hh24:mi:ss'),'1','13','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-22 08:00:00','yy-MM-dd hh24:mi:ss'),'1','13','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-22 12:00:00','yy-MM-dd hh24:mi:ss'),'1','13','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-22 18:00:00','yy-MM-dd hh24:mi:ss'),'1','13','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-23 08:00:00','yy-MM-dd hh24:mi:ss'),'1','13','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-23 12:00:00','yy-MM-dd hh24:mi:ss'),'1','13','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-23 18:00:00','yy-MM-dd hh24:mi:ss'),'1','13','4');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-03 08:00:00','yy-MM-dd hh24:mi:ss'),'1','14','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-03 12:00:00','yy-MM-dd hh24:mi:ss'),'1','14','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-03 18:00:00','yy-MM-dd hh24:mi:ss'),'1','14','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-04 08:00:00','yy-MM-dd hh24:mi:ss'),'1','14','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-04 12:00:00','yy-MM-dd hh24:mi:ss'),'1','14','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-04 18:00:00','yy-MM-dd hh24:mi:ss'),'1','14','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-05 08:00:00','yy-MM-dd hh24:mi:ss'),'1','14','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-05 12:00:00','yy-MM-dd hh24:mi:ss'),'1','14','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-05 18:00:00','yy-MM-dd hh24:mi:ss'),'1','14','1');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-20 08:00:00','yy-MM-dd hh24:mi:ss'),'1','15','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-20 12:00:00','yy-MM-dd hh24:mi:ss'),'1','15','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-20 18:00:00','yy-MM-dd hh24:mi:ss'),'1','15','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-21 08:00:00','yy-MM-dd hh24:mi:ss'),'1','15','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-21 12:00:00','yy-MM-dd hh24:mi:ss'),'1','15','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-21 18:00:00','yy-MM-dd hh24:mi:ss'),'1','15','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-22 08:00:00','yy-MM-dd hh24:mi:ss'),'1','15','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-22 12:00:00','yy-MM-dd hh24:mi:ss'),'1','15','2');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-22 18:00:00','yy-MM-dd hh24:mi:ss'),'1','15','2');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-07 08:00:00','yy-MM-dd hh24:mi:ss'),'1','16','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-07 12:00:00','yy-MM-dd hh24:mi:ss'),'1','16','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-07 18:00:00','yy-MM-dd hh24:mi:ss'),'1','16','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-08 08:00:00','yy-MM-dd hh24:mi:ss'),'1','16','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-08 12:00:00','yy-MM-dd hh24:mi:ss'),'1','16','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-08 18:00:00','yy-MM-dd hh24:mi:ss'),'1','16','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-09 08:00:00','yy-MM-dd hh24:mi:ss'),'1','16','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-09 12:00:00','yy-MM-dd hh24:mi:ss'),'1','16','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-09 18:00:00','yy-MM-dd hh24:mi:ss'),'1','16','3');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-18 08:00:00','yy-MM-dd hh24:mi:ss'),'1','17','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-18 12:00:00','yy-MM-dd hh24:mi:ss'),'1','17','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-18 18:00:00','yy-MM-dd hh24:mi:ss'),'1','17','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-19 08:00:00','yy-MM-dd hh24:mi:ss'),'1','17','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-19 12:00:00','yy-MM-dd hh24:mi:ss'),'1','17','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-19 18:00:00','yy-MM-dd hh24:mi:ss'),'1','17','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-20 08:00:00','yy-MM-dd hh24:mi:ss'),'1','17','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-20 12:00:00','yy-MM-dd hh24:mi:ss'),'1','17','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-20 18:00:00','yy-MM-dd hh24:mi:ss'),'1','17','4');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-03 08:00:00','yy-MM-dd hh24:mi:ss'),'1','18','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-03 12:00:00','yy-MM-dd hh24:mi:ss'),'1','18','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-03 18:00:00','yy-MM-dd hh24:mi:ss'),'1','18','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-04 08:00:00','yy-MM-dd hh24:mi:ss'),'1','18','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-04 12:00:00','yy-MM-dd hh24:mi:ss'),'1','18','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-04 18:00:00','yy-MM-dd hh24:mi:ss'),'1','18','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-05 08:00:00','yy-MM-dd hh24:mi:ss'),'1','18','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-05 12:00:00','yy-MM-dd hh24:mi:ss'),'1','18','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-08-05 18:00:00','yy-MM-dd hh24:mi:ss'),'1','18','3');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-13 08:00:00','yy-MM-dd hh24:mi:ss'),'1','19','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-13 12:00:00','yy-MM-dd hh24:mi:ss'),'1','19','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-13 18:00:00','yy-MM-dd hh24:mi:ss'),'1','19','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-14 08:00:00','yy-MM-dd hh24:mi:ss'),'1','19','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-14 12:00:00','yy-MM-dd hh24:mi:ss'),'1','19','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-14 18:00:00','yy-MM-dd hh24:mi:ss'),'1','19','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-15 08:00:00','yy-MM-dd hh24:mi:ss'),'1','19','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-15 12:00:00','yy-MM-dd hh24:mi:ss'),'1','19','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-15 18:00:00','yy-MM-dd hh24:mi:ss'),'1','19','4');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-28 08:00:00','yy-MM-dd hh24:mi:ss'),'1','20','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-28 12:00:00','yy-MM-dd hh24:mi:ss'),'1','20','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-28 18:00:00','yy-MM-dd hh24:mi:ss'),'1','20','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-29 08:00:00','yy-MM-dd hh24:mi:ss'),'1','20','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-29 12:00:00','yy-MM-dd hh24:mi:ss'),'1','20','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-29 18:00:00','yy-MM-dd hh24:mi:ss'),'1','20','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-30 08:00:00','yy-MM-dd hh24:mi:ss'),'1','20','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-30 12:00:00','yy-MM-dd hh24:mi:ss'),'1','20','3');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-30 18:00:00','yy-MM-dd hh24:mi:ss'),'1','20','3');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-05-21 08:00:00','yy-MM-dd hh24:mi:ss'),'1','21','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-05-21 12:00:00','yy-MM-dd hh24:mi:ss'),'1','21','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-05-21 18:00:00','yy-MM-dd hh24:mi:ss'),'1','21','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-05-22 08:00:00','yy-MM-dd hh24:mi:ss'),'1','21','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-05-22 12:00:00','yy-MM-dd hh24:mi:ss'),'1','21','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-05-22 18:00:00','yy-MM-dd hh24:mi:ss'),'1','21','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-05-23 08:00:00','yy-MM-dd hh24:mi:ss'),'1','21','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-05-23 12:00:00','yy-MM-dd hh24:mi:ss'),'1','21','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-05-23 18:00:00','yy-MM-dd hh24:mi:ss'),'1','21','1');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-28 08:00:00','yy-MM-dd hh24:mi:ss'),'1','22','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-28 12:00:00','yy-MM-dd hh24:mi:ss'),'1','22','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-28 18:00:00','yy-MM-dd hh24:mi:ss'),'1','22','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-29 08:00:00','yy-MM-dd hh24:mi:ss'),'1','22','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-29 12:00:00','yy-MM-dd hh24:mi:ss'),'1','22','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-29 18:00:00','yy-MM-dd hh24:mi:ss'),'1','22','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-30 08:00:00','yy-MM-dd hh24:mi:ss'),'1','22','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-30 12:00:00','yy-MM-dd hh24:mi:ss'),'1','22','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-30 18:00:00','yy-MM-dd hh24:mi:ss'),'1','22','4');



INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-30 08:00:00','yy-MM-dd hh24:mi:ss'),'1','23','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-30 12:00:00','yy-MM-dd hh24:mi:ss'),'1','23','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-06-30 18:00:00','yy-MM-dd hh24:mi:ss'),'1','23','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-01 08:00:00','yy-MM-dd hh24:mi:ss'),'1','23','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-01 12:00:00','yy-MM-dd hh24:mi:ss'),'1','23','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-01 18:00:00','yy-MM-dd hh24:mi:ss'),'1','23','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-02 08:00:00','yy-MM-dd hh24:mi:ss'),'1','23','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-02 12:00:00','yy-MM-dd hh24:mi:ss'),'1','23','1');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-02 18:00:00','yy-MM-dd hh24:mi:ss'),'1','23','1');


INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-06 08:00:00','yy-MM-dd hh24:mi:ss'),'1','24','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-06 12:00:00','yy-MM-dd hh24:mi:ss'),'1','24','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-06 18:00:00','yy-MM-dd hh24:mi:ss'),'1','24','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-07 08:00:00','yy-MM-dd hh24:mi:ss'),'1','24','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-07 12:00:00','yy-MM-dd hh24:mi:ss'),'1','24','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-07 18:00:00','yy-MM-dd hh24:mi:ss'),'1','24','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-08 08:00:00','yy-MM-dd hh24:mi:ss'),'1','24','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-08 12:00:00','yy-MM-dd hh24:mi:ss'),'1','24','4');

INSERT INTO M_SCHEDULE(SCHID,NEWPRICE,OLDPRICE,SCHDATE,CINEMA_ID,MOVIE_ID,MOVIEHALL_ID) VALUES(m_schedule_id.nextval,'19.9','35.5',"TO_DATE"('2017-07-08 18:00:00','yy-MM-dd hh24:mi:ss'),'1','24','4');


--评论
INSERT INTO M_COMMENT(ID,DOWN,UP,COMMENTDATE,USERNAME,INFO,MOVIE_ID) VALUES(m_comment_id.nextval,'','',"TO_DATE"('2017-07-22 12:00:00','yy-MM-dd hh24:mi:ss'),'123456','这电影真不错。。。',1);
INSERT INTO M_COMMENT(ID,DOWN,UP,COMMENTDATE,USERNAME,INFO,MOVIE_ID) VALUES(m_comment_id.nextval,'','',"TO_DATE"('2017-07-22 14:00:00','yy-MM-dd hh24:mi:ss'),'addmin','这电影真好看。。。',1);
INSERT INTO M_COMMENT(ID,DOWN,UP,COMMENTDATE,USERNAME,INFO,MOVIE_ID) VALUES(m_comment_id.nextval,'','',"TO_DATE"('2017-07-22 16:00:00','yy-MM-dd hh24:mi:ss'),'1asdasd','这电影真不错。。。',1);

INSERT INTO M_COMMENT(ID,DOWN,UP,COMMENTDATE,USERNAME,INFO,MOVIE_ID) VALUES(m_comment_id.nextval,'','',"TO_DATE"('2017-06-24 12:00:00','yy-MM-dd hh24:mi:ss'),'123456','这电影真不错。。。',2);
INSERT INTO M_COMMENT(ID,DOWN,UP,COMMENTDATE,USERNAME,INFO,MOVIE_ID) VALUES(m_comment_id.nextval,'','',"TO_DATE"('2017-06-24 14:00:00','yy-MM-dd hh24:mi:ss'),'addmin','这电影真好看。。。',2);
INSERT INTO M_COMMENT(ID,DOWN,UP,COMMENTDATE,USERNAME,INFO,MOVIE_ID) VALUES(m_comment_id.nextval,'','',"TO_DATE"('2017-06-24 16:00:00','yy-MM-dd hh24:mi:ss'),'1asdasd','这电影真不错。。。',2);

INSERT INTO M_COMMENT(ID,DOWN,UP,COMMENTDATE,USERNAME,INFO,MOVIE_ID) VALUES(m_comment_id.nextval,'','',"TO_DATE"('2017-07-14 12:00:00','yy-MM-dd hh24:mi:ss'),'123456','这电影真不错。。。',4);
INSERT INTO M_COMMENT(ID,DOWN,UP,COMMENTDATE,USERNAME,INFO,MOVIE_ID) VALUES(m_comment_id.nextval,'','',"TO_DATE"('2017-07-14 14:00:00','yy-MM-dd hh24:mi:ss'),'addmin','这电影真好看。。。',4);
INSERT INTO M_COMMENT(ID,DOWN,UP,COMMENTDATE,USERNAME,INFO,MOVIE_ID) VALUES(m_comment_id.nextval,'','',"TO_DATE"('2017-07-14 16:00:00','yy-MM-dd hh24:mi:ss'),'1asdasd','这电影真不错。。。',4);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','1',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','2',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','3',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','4',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','5',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','6',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','7',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','8',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','9',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','10',1);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','1',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','2',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','3',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','4',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','5',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','6',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','7',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','8',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','9',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','10',1);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','1',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','2',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','3',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','4',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','5',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','6',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','7',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','8',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','9',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','10',1);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','1',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','2',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','3',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','4',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','5',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','6',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','7',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','8',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','9',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','10',1);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','1',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','2',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','3',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','4',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','5',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','6',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','7',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','8',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','9',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','10',1);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','1',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','2',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','3',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','4',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','5',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','6',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','7',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','8',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','9',1);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','10',1);


INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','1',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','2',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','3',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','4',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','5',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','6',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','7',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','8',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','9',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','10',2);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','1',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','2',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','3',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','4',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','5',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','6',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','7',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','8',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','9',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','10',2);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','1',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','2',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','3',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','4',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','5',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','6',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','7',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','8',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','9',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','10',2);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','1',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','2',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','3',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','4',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','5',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','6',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','7',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','8',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','9',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','10',2);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','1',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','2',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','3',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','4',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','5',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','6',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','7',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','8',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','9',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','10',2);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','1',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','2',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','3',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','4',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','5',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','6',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','7',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','8',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','9',2);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','10',2);


INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','1',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','2',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','3',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','4',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','5',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','6',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','7',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','8',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','9',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','10',3);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','1',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','2',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','3',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','4',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','5',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','6',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','7',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','8',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','9',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','10',3);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','1',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','2',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','3',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','4',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','5',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','6',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','7',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','8',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','9',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','10',3);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','1',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','2',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','3',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','4',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','5',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','6',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','7',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','8',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','9',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','10',3);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','1',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','2',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','3',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','4',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','5',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','6',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','7',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','8',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','9',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','10',3);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','1',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','2',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','3',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','4',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','5',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','6',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','7',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','8',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','9',3);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','10',3);


INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','1',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','2',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','3',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','4',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','5',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','6',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','7',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','8',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','9',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','1','10',4);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','1',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','2',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','3',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','4',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','5',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','6',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','7',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','8',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','9',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','2','10',4);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','1',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','2',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','3',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','4',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','5',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','6',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','7',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','8',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','9',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','3','10',4);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','1',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','2',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','3',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','4',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','5',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','6',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','7',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','8',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','9',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','4','10',4);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','1',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','2',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','3',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','4',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','5',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','6',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','7',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','8',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','9',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','5','10',4);

INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','1',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','2',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','3',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','4',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','5',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','6',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','7',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','8',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','9',4);
INSERT INTO M_SEAT(SEATID,SEATTYPE,SEATX,SEATY,MOVIEHALL_ID) VALUES(m_seat_id.nextval,'可选座位','6','10',4);

commit
